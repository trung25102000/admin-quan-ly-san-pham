<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sinh viên - Trường Đại học Khoa học Huế - Th&#244;ng tin c&#225; nh&#226;n</title>

    <link rel="shortcut icon" type="image/x-icon" href="/favicon.ico" />
    <link href="Themes/sinhvien.css" rel="stylesheet" />
    <link href="Themes/sinhvien1.css" rel="stylesheet" />
    <link href="Themes/bootstrap.css" rel="stylesheet" />
    <link href="Themes/sinhvien_custo" rel="stylesheet" />
    <link href="Themes/sinhviensite.css" rel="stylesheet" />
    <link href="Themes/bootstrap-datepicker3.min.css" rel="stylesheet" />

    <!-- Scripts-->
    <script src="/Scripts/jquery-1.10.2.min.js"></script>
    <script src="/Scripts/bootstrap.min.js"></script>
    <script src="/Scripts/jquery.inputmask.bundle.min.js"></script>
    <script src="/Plugins/datepicker/bootstrap-datepicker.js"></script>
    <script src="/Plugins/datepicker/bootstrap-datepicker.vi.min.js"></script>
    <script src="/Scripts/site.js?t=638068372405032359"></script> 
    
</head>
<body>
    <!-- header-->
<!-- Header and Menu -->
<div class="container-fluid">
    <!-- Tiêu đề website -->
    <div class="row hitec-title">
        <div class="col-xs-8 nopadding">
            <h1>TRƯỜNG ĐẠI HỌC KHOA HỌC - ĐẠI HỌC HUẾ</h1>
        </div>
        <div class="col-xs-4 text-right" style="color:#cecece; line-height:28px">
            ${date}
        </div>
    </div>

    <!-- Thanh menu chính -->
    <div class="row">
        <div class="col-xs-12 nopadding">
            <nav class="navbar navbar-default hitec-navbar">
                <div class="navbar-header">
                    <button type="button" class="pull-left navbar-toggle" data-toggle="collapse" data-target="#menuMain">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand hitec-logo" href="KiemTraDangNhapSV">
                        <img src="Images/logoHue2.jpg" alt="logo">
                    </a>
                </div>

                <!-- menu main -->
                <div class="nav collapse navbar-collapse" id="menuMain">
                   <ul class="nav navbar-nav">
                        <li class="dropdown">
                            <a href="DangKiHocTapController" class="dropdown-toggle" data-toggle="dropdown">Đăng kí học tập <b class="caret"></b></a>
                        </li>

                        <li class="dropdown">
                            <a href="HocPhanDangKiController" class="dropdown-toggle" data-toggle="dropdown">Lớp Học Phần đã Đăng ký <b class="caret"></b></a>
                        </li>
                        
                         <li class="dropdown">
                            <a href="KetQuaQuaTrinhController" class="dropdown-toggle" data-toggle="dropdown">Kết Quả Học tập <b class="caret"></b></a>
                        </li>
                        
                        <li class="dropdown">
                            <a href="LichSuController" class="dropdown-toggle" data-toggle="dropdown">Lịch Sử quá trình học tập <b class="caret"></b></a>
                        </li>
                        
                        <li class="dropdown">
                            <a href="LopHocPhanDuocDuyetController" class="dropdown-toggle" data-toggle="dropdown">Lớp học phần đã được duyệt <b class="caret"></b></a>
                        </li>

                    </ul>
                </div>
            </nav>
        </div>
    </div>

    <!-- Thông tin về khóa/ngành, học kỳ tác nghiệp -->

    <div class="row" style="line-height:23px; font-size:13px;">
        <div class="col-xs-12 text-right">
            <a href="/Setting/Change" style="font-weight:bold; color:#ce2020" title="Nhấp vào đây để thay đổi ngành học và học kỳ tác nghiệp">
                <b class="glyphicon glyphicon-education"></b> Kh&#243;a ${lop.getKhoa()} (${lop.getNamnhaphoc()}-${lop.getNamnhaphoc()+4})
                <b class="glyphicon glyphicon-book" style="margin-left:20px;"></b> ${khoa.getTenkhoa()}
                <b class="glyphicon glyphicon-calendar" style="margin-left:20px;"></b> Học kỳ: 2, năm học: ${year}-${year+1}
            </a>
        </div>
    </div>
</div>

    <!-- Panel left and main content-->
    <div class="container-fluid" id="wrapper">
        <!-- Left panel -->
        <div class="panel-sidebar-left">
            <div class="hitec-sidebar">

    <div class="hitec-information">
        <h5>
            ${sessionScope.ten.getHodem()} ${sessionScope.ten.getTen()}
        </h5>        
        <p>
            <img src="Images/logoCanhan.png" alt="">
            <a id="link_userProfile" href="Thongtincanhan">Lý lịch cá nhân</a>
        </p>
        <!--
        <p>
            <img src="~/images/icons/curriculum_vitae16.png" alt="">
            <a id="link_userProfile" href="#">Sổ tay sinh viên</a>
        </p>
        -->
        <p>
            <img src="Images/logodoimatkhau.png" alt="">
            <a id="link_userPassword" href="DoiMatKhauController">Đổi mật khẩu</a>
        </p>

        <p>
            <img src="Images/logoDangXuat.png" alt="">
            <a id="link_userLogout" href="DangXuat" onclick="return confirm('Thoát khỏi hệ thống')">Đăng xuất</a>
        </p>
    </div>

<div class="hitec-general-function">
    <h5>CÁC CHỨC NĂNG CHUNG</h5>
    <p>
        <img src="Images/logothudientu.png" alt="">
        <a id="linkNews" href="/News">Tin tức - Thông báo <span class="notification-news"></span></a>
    </p>
    <p>
        <img src="Images/logothoikhoabieu.png" alt="">
        <a id="linkCalendar" href="/TimeTable/Week">Thời khóa biểu học tập<span class="notification-calendar"></span></a>
    </p>    
    <p>
        <img src="Images/logoemail.png" alt="">
        <a id="linkMessage" href="/Message/Inbox">Tin nhắn <span class="notification-message"></span></a>
    </p>   
	<p>
        <img src="Images/logothudientu.png" alt="">
        <a id="linkEmail" href="https://mail.google.com/a/husc.edu.vn" target="_blank">Thư điện tử</a> <img src="/images/new.gif" alt="">
    </p>	
</div>









<div id="hotNews" class="hitec-notification">
    <h5>TIN TỨC - THÔNG BÁO</h5>
    <ul>
            <li>
                <a href="/News/Content/so-tay-hoc-vu-va-thoi-khoa-bieu-du-kien-hoc-ky-2-nam-hoc-2022-2023/">Sổ tay học vụ v&#224; thời kh&#243;a biểu dự kiến học kỳ 2, năm học 2022-2023 <small class="text-muted">(06/12/2022)</small></a>
            </li>
            <li>
                <a href="/News/Content/quyet-dinh-ve-viec-cong-nhan-tot-nghiep-dai-hoc-he-chinh-quy-dot-3-nam-2022/">Quyết định về việc c&#244;ng nhận tốt nghiệp đại học hệ ch&#237;nh quy đợt 3 năm 2022 <small class="text-muted">(31/10/2022)</small></a>
            </li>
            <li>
                <a href="/News/Content/thong-bao-ket-qua-ra-soat-dieu-kien-xet-tot-nghiep-he-chinh-quy-dot-3-nam-2022/">Th&#244;ng b&#225;o Kết quả r&#224; so&#225;t điều kiện x&#233;t tốt nghiệp hệ ch&#237;nh quy, đợt 3 năm 2022 <small class="text-muted">(25/10/2022)</small></a>
            </li>
            <li>
                <a href="/News/Content/thong-bao-v-v-to-chuc-le-be-giang-va-trao-bang-tot-nghiep-dai-hoc-nam-2022/">Th&#244;ng b&#225;o v/v tổ chức Lễ bế giảng v&#224; trao bằng tốt nghiệp đại học năm 2022 <small class="text-muted">(14/06/2022)</small></a>
            </li>
            <li>
                <a href="/News/Content/ke-hoach-dao-tao-va-thoi-khoa-bieu-du-kien-hoc-ky-3-nam-hoc-2021-2022/">Kế hoạch đ&#224;o tạo v&#224; Thời kh&#243;a biểu dự kiến học kỳ 3, năm học 2021-2022 <small class="text-muted">(27/05/2022)</small></a>
            </li>
    </ul>
</div>


            </div>
        </div>
        <!-- Right panel -->
        
        <!-- Main content -->
        <div class="panel-main-content">
            <div class="hitec-content">
                <div class="row">
                    <div class="col-xs-12">
                        <img id="ajax-loader" src="/images/icons/ajax-loader.gif" alt="" />
                        


<h2>LÝ LỊCH SINH VIÊN</h2>
<div class="container-fluid form-horizontal" style="position:relative">
    <div id="userProfileContent">
    		<fieldset class="hitec-fieldset">
    <legend>
        <a href="laydulieu" class="pull-right">
            <span class="glyphicon glyphicon-edit"></span>sửa
        </a>
        Thông tin chung
    </legend>

    <div class="text-center" style="width:130px; height:145px; position:absolute; z-index:999; top:30px; right:0; padding-left:10px; background:#ffffff;">
        <label for="filePhoto" style="cursor:pointer" title="Click để thay đổi ảnh (kích thước ảnh tối đa 512KB)">
            <c:if test="${sv.getAnh()==null}">  <img id="studentPhoto" src="Images/Anhcanhan.jpg" class="img-thumbnail" style="width:125px; height:140px;" alt=""></c:if>
            <c:if test="${sv.getAnh()!=null}"><img id="studentPhoto" src="Images/${sv.getAnh()}" class="img-thumbnail" style="width:125px; height:140px;" alt=""></c:if>
        </label>
        <form action="tam123" method="post" id="formUploadPhoto" enctype= "multipart/form-data">
            <input name="__RequestVerificationToken" type="hidden" value="DLAF0OTYSp7JuaRXsTwkgduOhsnxXrHAlVRVlN83fLKZpOS-mkeZQwwYswVMQQ69hfDsX2ni_nZIOc0X9Qg9iTYI0-78yyOYHieCw9nawZ1ywhfqm-yKDSWxrlN6DYCFcP7ebf9DaRvkgS2__hmbe4vJQJD7Cgka9mTz9EacVVRw_tXuMEJxVpNNa_0-6wq2zugLGbNQX27BxAMa_Hn2jFvEzuc1">
            <input type="file" name="filePhoto" id="filePhoto" accept="image/x-png,image/gif,image/jpeg" style="position:absolute;left:0;top:0;opacity:0;">
            <input type="submit" value="Đồng ý">
        </form>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="form-group hitec-border-bottom-dotted">
		                <label class="control-label col-sm-3">Mã sinh viên:</label>
		                <div class="col-sm-9">
		                    <p class="form-control-static"> ${sv.getMasv()}</p>
		                </div>
		            </div>
		
		            <div class="form-group hitec-border-bottom-dotted">
		                <label class="control-label col-sm-3">Họ và tên:</label>
		                <div class="col-sm-9">
		                    <p class="form-control-static"> ${sv.getHodem()} ${sv.getTen()}</p>
		                </div>
		            </div>
		
		            <div class="form-group hitec-border-bottom-dotted">
		                <label class="control-label col-sm-3">Giới tính:</label>
		                <div class="col-sm-9">
		                	<c:if test="${sv.getGioitinh()==true}"><p class="form-control-static">Nam</p></c:if>
		                    <c:if test="${sv.getGioitinh()==false}"><p class="form-control-static">Nữ</p></c:if>
		                </div>
		            </div>
		
		            <div class="form-group hitec-border-bottom-dotted">
		                <label class="control-label col-sm-3">Ngày sinh:</label>
		                <div class="col-sm-9">
		                    <p class="form-control-static">${sv.getNgaysinh()}</p>
		                </div>
		            </div>
		
		            <div class="form-group hitec-border-bottom-dotted">
		                <label class="control-label col-sm-3">Nơi sinh:</label>
		                <div class="col-sm-9">
		                    <p class="form-control-static">${sv.getNoisinh()}</p>
		                </div>
		            </div>
		
		            <div class="form-group hitec-border-bottom-dotted">
		                <label class="control-label col-sm-3">Quốc tịch:</label>
		                <div class="col-sm-9">
		                    <p class="form-control-static">
		                        Việt Nam
		                    </p>
		                </div>
		            </div>
		        </div>
		    </div>
		</fieldset>
    </div>
</div>


                    </div>
                </div>
            </div>
        </div>
    </div>
<div class="container-fluid nopadding">
    <footer>
        <div class="row">
            <div class="col-xs-12 text-center nopadding">
                <p>Trường Đại học Khoa học – Đại học Huế</p>
                <p>Địa chỉ: 77 Nguyễn Huệ, Thành phố Huế, Thừa Thiên Huế, Việt Nam</p>
                <p>Điện thoại: (+84) 0234.3823290 – Fax: (+84) 0234.3824901</p>
                <p>Website: <a href="http://www.husc.edu.vn" target="_blank">http://www.husc.edu.vn</a></p>
				<p><small>&copy; Hoàng Quốc Trung, Khoa Công nghệ Thông tin</small></p>
            </div>
        </div>
    </footer>
</div>

    <!-- for main popup dialog -->
    <div id="dialogMain" class="modal fade" role="dialog"></div>
      
    <script language="javascript" type="text/javascript">
        applicationUrl = "https://student.husc.edu.vn/";
        $(document).ready(function () {
            get_notifications();
            window.setInterval(get_notifications, 60000);
            $(".hitec-datepicker").datepicker({
                format: "dd/mm/yyyy",
                language: "vi",
                autoclose: true,
                todayHighlight: true,
                weekStart: 1
            });
            $('#dialogMain').on('hidden.bs.modal', function () {
                $("#dialogMain").html("");
            })
            $(".checkbox-all").change(function () {
                var status = this.checked;
                $($(this).data("checkbox-all-range")).each(function () {
                    $(this).prop("checked", status);
                });
            });            
            triggerPopupLink();
        });
    </script>    
    
    
    <script language="javasript" type="text/javascript">
        $(document).ready(function () {
            getUserProfileContent();
            getUserHistoryContent();
            getUserRelationsContent();
        });
        function getUserProfileContent() {
            getPageContent("#userProfileContent", createBaseLink("Account/UserProfileContent"));
            triggerPopupLink();
        }
        function getUserHistoryContent() {
            getPageContent("#userHistoryContent", createBaseLink("Account/History/List"));
            triggerPopupLink();
        }
        function getUserRelationsContent() {
            getPageContent("#userRelationsContent", createBaseLink("Account/Relations/List"));
            triggerPopupLink();
        }
    </script>

</body>
</html>