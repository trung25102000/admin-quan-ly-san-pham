<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sinh viên - Trường Đại học Khoa học Huế - Th&#244;ng tin c&#225; nh&#226;n</title>

    <link rel="shortcut icon" type="image/x-icon" href="/favicon.ico" />
    <link href="Themes/sinhvien.css" rel="stylesheet" />
    <link href="Themes/sinhvien1.css" rel="stylesheet" />
    <link href="Themes/bootstrap.css" rel="stylesheet" />
    <link href="Themes/sinhvien_custo" rel="stylesheet" />
    <link href="Themes/sinhviensite.css" rel="stylesheet" />
    <link href="Themes/bootstrap-datepicker3.min.css" rel="stylesheet" />

    <!-- Scripts-->
    <script src="/Scripts/jquery-1.10.2.min.js"></script>
    <script src="/Scripts/bootstrap.min.js"></script>
    <script src="/Scripts/jquery.inputmask.bundle.min.js"></script>
    <script src="/Plugins/datepicker/bootstrap-datepicker.js"></script>
    <script src="/Plugins/datepicker/bootstrap-datepicker.vi.min.js"></script>
    <script src="/Scripts/site.js?t=638068372405032359"></script> 
    
</head>
<c:if test="${khongcapnhat!=null}">
<script>
	alert('Cập nhật Thất bại!')
</script>
</c:if>
<body>
    <!-- header-->
<!-- Header and Menu -->
<div class="container-fluid">
    <!-- Tiêu đề website -->
    <div class="row hitec-title">
        <div class="col-xs-8 nopadding">
            <h1>TRƯỜNG ĐẠI HỌC KHOA HỌC - ĐẠI HỌC HUẾ</h1>
        </div>
        <div class="col-xs-4 text-right" style="color:#cecece; line-height:28px">
            ${date}
        </div>
    </div>

    <!-- Thanh menu chính -->
    <div class="row">
        <div class="col-xs-12 nopadding">
            <nav class="navbar navbar-default hitec-navbar">
                <div class="navbar-header">
                    <button type="button" class="pull-left navbar-toggle" data-toggle="collapse" data-target="#menuMain">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand hitec-logo" href="KiemTraDangNhapSV">
                        <img src="Images/logoHue2.jpg" alt="logo">
                    </a>
                </div>

                <!-- menu main -->
                <div class="nav collapse navbar-collapse" id="menuMain">
                    <ul class="nav navbar-nav">
                        <li class="dropdown">
                            <a href="DangKiHocTapController" class="dropdown-toggle" data-toggle="dropdown">Đăng kí học tập <b class="caret"></b></a>
                        </li>

                        <li class="dropdown">
                            <a href="HocPhanDangKiController" class="dropdown-toggle" data-toggle="dropdown">Lớp Học Phần đã Đăng ký <b class="caret"></b></a>
                        </li>
                        
                         <li class="dropdown">
                            <a href="KetQuaQuaTrinhController" class="dropdown-toggle" data-toggle="dropdown">Kết Quả Học tập <b class="caret"></b></a>
                        </li>
                        
                        <li class="dropdown">
                            <a href="LichSuController" class="dropdown-toggle" data-toggle="dropdown">Lịch Sử quá trình học tập <b class="caret"></b></a>
                        </li>
                        
                        <li class="dropdown">
                            <a href="LopHocPhanDuocDuyetController" class="dropdown-toggle" data-toggle="dropdown">Lớp học phần đã được duyệt <b class="caret"></b></a>
                        </li>

                    </ul>
                </div>
            </nav>
        </div>
    </div>

    <!-- Thông tin về khóa/ngành, học kỳ tác nghiệp -->

    <div class="row" style="line-height:23px; font-size:13px;">
        <div class="col-xs-12 text-right">
            <a href="/Setting/Change" style="font-weight:bold; color:#ce2020" title="Nhấp vào đây để thay đổi ngành học và học kỳ tác nghiệp">
                <b class="glyphicon glyphicon-education"></b> Kh&#243;a ${lop.getKhoa()} (${lop.getNamnhaphoc()}-${lop.getNamnhaphoc()+4})
                <b class="glyphicon glyphicon-book" style="margin-left:20px;"></b> ${khoa.getTenkhoa()}
                <b class="glyphicon glyphicon-calendar" style="margin-left:20px;"></b> Học kỳ: 2, năm học: ${year}-${year+1}
            </a>
        </div>
    </div>
</div>

    <!-- Panel left and main content-->
    <div class="container-fluid" id="wrapper">
        <!-- Left panel -->
        <div class="panel-sidebar-left">
            <div class="hitec-sidebar">

    <div class="hitec-information">
        <h5>
            ${sessionScope.ten.getHodem()} ${sessionScope.ten.getTen()}
        </h5>        
        <p>
            <img src="Images/logoCanhan.png" alt="">
            <a id="link_userProfile" href="Thongtincanhan">Lý lịch cá nhân</a>
        </p>
        <!--
        <p>
            <img src="~/images/icons/curriculum_vitae16.png" alt="">
            <a id="link_userProfile" href="#">Sổ tay sinh viên</a>
        </p>
        -->
        <p>
            <img src="Images/logodoimatkhau.png" alt="">
            <a id="link_userPassword" href="DoiMatKhauController">Đổi mật khẩu</a>
        </p>

        <p>
            <img src="Images/logoDangXuat.png" alt="">
            <a id="link_userLogout" href="DangXuat" onclick="return confirm('Thoát khỏi hệ thống')">Đăng xuất</a>
        </p>
    </div>

<div class="hitec-general-function">
    <h5>CÁC CHỨC NĂNG CHUNG</h5>
    <p>
        <img src="Images/logothudientu.png" alt="">
        <a id="linkNews" href="/News">Tin tức - Thông báo <span class="notification-news"></span></a>
    </p>
    <p>
        <img src="Images/logothoikhoabieu.png" alt="">
        <a id="linkCalendar" href="/TimeTable/Week">Thời khóa biểu học tập<span class="notification-calendar"></span></a>
    </p>    
    <p>
        <img src="Images/logoemail.png" alt="">
        <a id="linkMessage" href="/Message/Inbox">Tin nhắn <span class="notification-message"></span></a>
    </p>   
	<p>
        <img src="Images/logothudientu.png" alt="">
        <a id="linkEmail" href="https://mail.google.com/a/husc.edu.vn" target="_blank">Thư điện tử</a> <img src="/images/new.gif" alt="">
    </p>	
</div>









<div id="hotNews" class="hitec-notification">
    <h5>TIN TỨC - THÔNG BÁO</h5>
    <ul>
            <li>
                <a href="/News/Content/so-tay-hoc-vu-va-thoi-khoa-bieu-du-kien-hoc-ky-2-nam-hoc-2022-2023/">Sổ tay học vụ v&#224; thời kh&#243;a biểu dự kiến học kỳ 2, năm học 2022-2023 <small class="text-muted">(06/12/2022)</small></a>
            </li>
            <li>
                <a href="/News/Content/quyet-dinh-ve-viec-cong-nhan-tot-nghiep-dai-hoc-he-chinh-quy-dot-3-nam-2022/">Quyết định về việc c&#244;ng nhận tốt nghiệp đại học hệ ch&#237;nh quy đợt 3 năm 2022 <small class="text-muted">(31/10/2022)</small></a>
            </li>
            <li>
                <a href="/News/Content/thong-bao-ket-qua-ra-soat-dieu-kien-xet-tot-nghiep-he-chinh-quy-dot-3-nam-2022/">Th&#244;ng b&#225;o Kết quả r&#224; so&#225;t điều kiện x&#233;t tốt nghiệp hệ ch&#237;nh quy, đợt 3 năm 2022 <small class="text-muted">(25/10/2022)</small></a>
            </li>
            <li>
                <a href="/News/Content/thong-bao-v-v-to-chuc-le-be-giang-va-trao-bang-tot-nghiep-dai-hoc-nam-2022/">Th&#244;ng b&#225;o v/v tổ chức Lễ bế giảng v&#224; trao bằng tốt nghiệp đại học năm 2022 <small class="text-muted">(14/06/2022)</small></a>
            </li>
            <li>
                <a href="/News/Content/ke-hoach-dao-tao-va-thoi-khoa-bieu-du-kien-hoc-ky-3-nam-hoc-2021-2022/">Kế hoạch đ&#224;o tạo v&#224; Thời kh&#243;a biểu dự kiến học kỳ 3, năm học 2021-2022 <small class="text-muted">(27/05/2022)</small></a>
            </li>
    </ul>
</div>


            </div>
        </div>
        <!-- Right panel -->
        
        <!-- Main content -->
        <div class="panel-main-content">
            <div class="hitec-content">
                <div class="row">
                    <div class="col-xs-12">
                        <img id="ajax-loader" src="/images/icons/ajax-loader.gif" alt="" />
                 
						<form  class="form-horizontal" action="laydulieu" method="post">
						    <input name="__RequestVerificationToken" type="hidden" value="AhNRPVuKXk2bJAMsK-07cGGgIQ_A8uwsZCtsD-mJL7gwz7nr1TOUNzucA83ihiDnK0DB8ZTosQDOzOZ5769TZuqXmH_UwYXGdgq8OLhsLLNihL1vlWl3Yo0pgRTu3CNos133QBxrJFcRpojIdSIk5QX1-4FHOfjBqq4c5nGbvE8AYlpEhkoP6b26OdqiHGoZaM2YLUajopojAMTS3iMXwYtadAU1" />
						    <div class="alert alert-info alert-dismissable">
						        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
						        <p><b>Nhắc nhở:</b></p>
						        <p>
						            - Sinh viên khai báo đúng, đầy đủ thông tin và chịu trách nhiệm với các thông tin mà bản thân đã khai báo
						        </p>
						        <p>
						            - Những mục được đánh dấu * là bắt buộc phải khai báo
						        </p>
						    </div>
						
						    <!-- Thông tin chung -->
						    <fieldset class="hitec-fieldset" style="padding-bottom:20px; margin-bottom:20px;">
						        <legend style="margin-bottom:15px">Thông tin chung</legend>
						
						        <div class="row">
						            <div class="col-md-12">
						
						                <div class="form-group">
						                    <label class="control-label col-md-2">Mã sinh viên:</label>
						                    <div class="col-md-2">
						                        <input type="text" class="form-control" disabled value="${sessionScope.ten.getMasv()}" />
						                    </div>
						
						                    <label class="control-label col-md-2">Họ và tên:</label>
						                    <div class="col-md-6">
						                        <input type="text" name="ten" class="form-control"  value="${sessionScope.ten.getHodem()} ${sessionScope.ten.getTen()}" />
						                    </div>
						                </div>
						
						                <div class="form-group">
						                    <label class="control-label col-md-2">*Ngày sinh:</label>
						                    <div class="col-md-2">
						                        <input class="form-control inputmask-date" id="LyLichSinhVien_NgaySinh" name="LyLichSinhVien.NgaySinh" type="text" value="${sessionScope.ten.getNgaysinh()}" />
						                    </div>
						
						                    <label class="control-label col-md-2">*Nơi sinh:</label>
						                    <div class="col-md-2">
						                        <select class="form-control tudien-quocgia" data-related-control="#noisinh_tinhthanh" id="LyLichSinhVien_NoiSinh_QuocGia" name="LyLichSinhVien.NoiSinh_QuocGia"><option value="">------</option>
												<option selected="selected" value="Việt Nam">Việt Nam</option>
												<option value="Lào">L&#224;o</option>
												<option value="Trung Quốc">Trung Quốc</option>
												<option value="Campuchia">Campuchia</option>
												<option value="Nhật Bản">Nhật Bản</option>
												<option value="Hàn Quốc">H&#224;n Quốc</option>
												<option value="Hoa Kì">Hoa Kỳ</option>
												<option value="Pháp">Ph&#225;p</option>
												</select>
						                    </div>
						                    <div class="col-md-4">
						                        <input class="form-control inputmask-date" id="LyLichSinhVien_NgaySinh" name="LyLichSinhVien.NoiSinh" type="text" value="${sessionScope.ten.getNoisinh()}" />
						                    </div>
						                </div>
						
						                <div class="form-group">
						                    <label class="control-label col-md-2">*Giới tính:</label>
						                    <div class="col-md-2">
						                        <select class="form-control" id="LyLichSinhVien_QuocTich" name="LyLichSinhVien.QuocTich"><option value="">------</option>
												<c:if test="${sessionScope.ten.getGioitinh()==true}">
													<option selected="selected" value="001">Nam</option>
													<option value="002">Nữ</option>
												</c:if>
												<c:if test="${sessionScope.ten.getGioitinh()!=true}">
													<option  value="001">Nam</option>
													<option selected="selected" value="002">Nữ</option>
												</c:if>
												</select>
						                    </div>
						
						                    <label class="control-label col-md-2">*Dân tộc:</label>
						                    <div class="col-md-2">
						                        <select class="form-control" id="LyLichSinhVien_DanToc" name="LyLichSinhVien.DanToc"><option selected="selected" value="01">Kinh</option>
												<option value="02">T&#224;y</option>
												<option value="03">N&#249;ng</option>
												<option value="04">M&#244;ng</option>
												<option value="05">Mường</option>
												<option value="06">Dao</option>
												<option value="07">Khơ-me</option>
												<option value="08">&#202;-đ&#234;</option>
												<option value="09">Cao Lan</option>
												<option value="10">Th&#225;i</option>
												<option value="11">Gia-rai</option>
												<option value="12">La Chi</option>
												<option value="13">H&#224; Nh&#236;</option>
												<option value="14">Gi&#225;y</option>
												<option value="15">Mn&#244;ng</option>
												<option value="16">Cơ-tu</option>
												<option value="17">Xơ-đăng</option>
												<option value="18">Xti&#234;ng</option>
												<option value="19">Ba-na</option>
												<option value="20">Hr&#234;</option>
												<option value="21">Gi&#233;-Tri&#234;ng</option>
												<option value="22">Chăm</option>
												<option value="23">Cơ-ho</option>
												<option value="24">Mạ</option>
												<option value="25">S&#225;n D&#236;u</option>
												<option value="26">Thổ</option>
												<option value="27">Khơ-m&#250;</option>
												<option value="28">Bru-V&#226;n Kiều</option>
												<option value="29">Ta-&#244;i</option>
												<option value="30">Co</option>
												<option value="31">Chơ-ro</option>
												<option value="32">Xinh-mun</option>
												<option value="33">Chu-ru</option>
												<option value="35">Ph&#249; L&#225;</option>
												<option value="36">La Hủ</option>
												<option value="37">Kh&#225;ng</option>
												<option value="38">Lự</option>
												<option value="39">P&#224; Thẻn</option>
												<option value="40">L&#244; L&#244;</option>
												<option value="41">Chứt</option>
												<option value="42">Mảng</option>
												<option value="43">Cơ Lao</option>
												<option value="44">Bố Y</option>
												<option value="45">La Ha</option>
												<option value="46">Cống</option>
												<option value="47">Ng&#225;i</option>
												<option value="48">Si La</option>
												<option value="49">Pu P&#233;o</option>
												<option value="50">Br&#226;u</option>
												<option value="51">Rơ-măm</option>
												<option value="52">Ơ Đu</option>
												<option value="53">Hoa</option>
												<option value="54">Ra-glai</option>
												<option value="55">Hm&#244;ng</option>
												<option value="56">Pac&#244;</option>
												<option value="57">Pahy</option>
												<option value="58">L&#224;o</option>
												<option value="59">Bhoong</option>
												<option value="60">Người nước ngo&#224;i</option>
												<option value="61">S&#225;n Chay</option>
												</select>
						                    </div>
						
						                    <label class="control-label col-md-1">*Tôn giáo:</label>
						                    <div class="col-md-3">
						                        <select class="form-control" id="LyLichSinhVien_TonGiao" name="LyLichSinhVien.TonGiao"><option selected="selected" value="01">Kh&#244;ng</option>
												<option value="02">Phật gi&#225;o</option>
												<option value="03">C&#244;ng gi&#225;o</option>
												<option value="04">Cao Đ&#224;i</option>
												<option value="05">Phật gi&#225;o Ho&#224; Hảo</option>
												<option value="06">Tin L&#224;nh</option>
												<option value="07">Minh L&#253; đạo</option>
												<option value="09">Tịnh độ cư sĩ Phật hội Việt Nam</option>
												<option value="10">Đạo Tứ ấn hiếu nghĩa</option>
												<option value="11">Bửu sơn Kỳ hương</option>
												<option value="12">Ba Ha&#39;i</option>
												<option value="13">T&#244;n gi&#225;o kh&#225;c</option>
												<option value="15">Hồi gi&#225;o</option>
												<option value="16">Minh sư đạo</option>
												</select>
				 							</div>
										</div>
						
						    
						    <div class="row">
						        <div class="col-lg-offset-3 col-md-9" id="editProfileAlert" style="margin-top:5px;margin-bottom:5px;">
						        </div>
						        <div class="col-lg-offset-3 col-md-9" style="margin-top:5px;margin-bottom:15px;">
						            <button style="float:right;margin-left: 15px"  name="capnhat" value="1" type="submit" class="btn btn-primary"><i class="glyphicon glyphicon-floppy-disk"></i> Cập nhật lý lịch</button>
						            <a style="float:right;" href="Thongtincanhan" class="btn btn-default">Quay lại</a>
						        </div>
						    </div>
						    </div>
						    </fieldset>
						</form>

                    </div>
                </div>
            </div>
        </div>
    </div>
<div class="container-fluid nopadding">
    <footer>
        <div class="row">
            <div class="col-xs-12 text-center nopadding">
                <p>Trường Đại học Khoa học – Đại học Huế</p>
                <p>Địa chỉ: 77 Nguyễn Huệ, Thành phố Huế, Thừa Thiên Huế, Việt Nam</p>
                <p>Điện thoại: (+84) 0234.3823290 – Fax: (+84) 0234.3824901</p>
                <p>Website: <a href="http://www.husc.edu.vn" target="_blank">http://www.husc.edu.vn</a></p>
				<p><small>&copy; Hoàng Quốc Trung, Khoa Công nghệ Thông tin</small></p>
            </div>
        </div>
    </footer>
</div>

    <!-- for main popup dialog -->
    <div id="dialogMain" class="modal fade" role="dialog"></div>
      
    <script language="javascript" type="text/javascript">
        applicationUrl = "https://student.husc.edu.vn/";
        $(document).ready(function () {
            get_notifications();
            window.setInterval(get_notifications, 60000);
            $(".hitec-datepicker").datepicker({
                format: "dd/mm/yyyy",
                language: "vi",
                autoclose: true,
                todayHighlight: true,
                weekStart: 1
            });
            $('#dialogMain').on('hidden.bs.modal', function () {
                $("#dialogMain").html("");
            })
            $(".checkbox-all").change(function () {
                var status = this.checked;
                $($(this).data("checkbox-all-range")).each(function () {
                    $(this).prop("checked", status);
                });
            });            
            triggerPopupLink();
        });
    </script>    
    
    
    <script language="javasript" type="text/javascript">
        $(document).ready(function () {
            getUserProfileContent();
            getUserHistoryContent();
            getUserRelationsContent();
        });
        function getUserProfileContent() {
            getPageContent("#userProfileContent", createBaseLink("Account/UserProfileContent"));
            triggerPopupLink();
        }
        function getUserHistoryContent() {
            getPageContent("#userHistoryContent", createBaseLink("Account/History/List"));
            triggerPopupLink();
        }
        function getUserRelationsContent() {
            getPageContent("#userRelationsContent", createBaseLink("Account/Relations/List"));
            triggerPopupLink();
        }
    </script>

</body>
</html>