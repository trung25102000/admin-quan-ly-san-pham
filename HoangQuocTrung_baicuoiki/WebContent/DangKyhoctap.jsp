<%@page import="dao.ThamGiaHoc_dao"%>
<%@page import="bo.ThamGiaHoc_bo"%>
<%@page import="bean.SinhVien_bean"%>
<%@page import="bean.MonHoc_bean"%>
<%@page import="bo.Monhoc_bo"%>
<%@page import="bo.LopHocPhan_bo"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sinh viên - Trường Đại học Khoa học Huế - Tin tức - th&#244;ng b&#225;o</title>
   

    <link rel="shortcut icon" type="image/x-icon" href="/favicon.ico" />
    <link href="Themes/sinhvien.css" rel="stylesheet" />
    <link href="Themes/sinhvien1.css" rel="stylesheet" />
    <link href="Themes/sinhviensite.css" rel="stylesheet" />
    <link href="Themes/bootstrap-datepicker3.min.css" rel="stylesheet" />
    <script src="Script/sinhvien.js"></script> 
 <%LopHocPhan_bo l_bo=new LopHocPhan_bo();  %> 
</head>
<c:if test="${doithanhcong!=null}">
<script>
	alert('Đổi Mật Khẩu Thành Công!')
</script>
</c:if>
<body>
    <!-- header-->
<!-- Header and Menu -->
<div class="container-fluid">
    <!-- Tiêu đề website -->
    <div class="row hitec-title">
        <div class="col-xs-8 nopadding">
            <h1>TRƯỜNG ĐẠI HỌC KHOA HỌC - ĐẠI HỌC HUẾ</h1>
        </div>
        <div class="col-xs-4 text-right" style="color:#cecece; line-height:28px">
            ${date}
        </div>
    </div>

    <!-- Thanh menu chính -->
    <div class="row">
        <div class="col-xs-12 nopadding">
            <nav class="navbar navbar-default hitec-navbar">
                <div class="navbar-header">
                    <button type="button" class="pull-left navbar-toggle" data-toggle="collapse" data-target="#menuMain">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand hitec-logo" href="KiemTraDangNhapSV">
                        <img src="Images/logoHue2.jpg" alt="logo">
                    </a>
                </div>

                <!-- menu main -->
                <div class="nav collapse navbar-collapse" id="menuMain">
                    <ul class="nav navbar-nav">
                        <li class="dropdown">
                            <a href="DangKiHocTapController" class="dropdown-toggle" data-toggle="dropdown">Đăng kí học tập <b class="caret"></b></a>
                        </li>

                        <li class="dropdown">
                            <a href="HocPhanDangKiController" class="dropdown-toggle" data-toggle="dropdown">Lớp Học Phần đã Đăng ký <b class="caret"></b></a>
                        </li>
                        
                         <li class="dropdown">
                            <a href="KetQuaQuaTrinhController" class="dropdown-toggle" data-toggle="dropdown">Kết Quả Học tập <b class="caret"></b></a>
                        </li>
                        
                        <li class="dropdown">
                            <a href="LichSuController" class="dropdown-toggle" data-toggle="dropdown">Lịch Sử quá trình học tập <b class="caret"></b></a>
                        </li>
                        
                        <li class="dropdown">
                            <a href="LopHocPhanDuocDuyetController" class="dropdown-toggle" data-toggle="dropdown">Lớp học phần đã được duyệt <b class="caret"></b></a>
                        </li>


                    </ul>
                </div>
            </nav>
        </div>
    </div>

    <!-- Thông tin về khóa/ngành, học kỳ tác nghiệp -->

    <div class="row" style="line-height:23px; font-size:13px;">
        <div class="col-xs-12 text-right">
            <a href="/Setting/Change" style="font-weight:bold; color:#ce2020" title="Nhấp vào đây để thay đổi ngành học và học kỳ tác nghiệp">
                <b class="glyphicon glyphicon-education"></b> Kh&#243;a ${lop.getKhoa()} (${lop.getNamnhaphoc()}-${lop.getNamnhaphoc()+4})
                <b class="glyphicon glyphicon-book" style="margin-left:20px;"></b> ${khoa.getTenkhoa()}
                <b class="glyphicon glyphicon-calendar" style="margin-left:20px;"></b> Học kỳ: 2, năm học: ${year}-${year+1}
            </a>
        </div>
    </div>
</div>

    <!-- Panel left and main content-->
    <div class="container-fluid" id="wrapper">
        <!-- Left panel -->
        <div class="panel-sidebar-left">
            <div class="hitec-sidebar">

    <div class="hitec-information">
        <h5>
            ${sessionScope.ten.getHodem()} ${sessionScope.ten.getTen()}
        </h5>        
        <p>
            <img src="Images/logoCanhan.png" alt="">
            <a id="link_userProfile" href="Thongtincanhan">Lý lịch cá nhân</a>
        </p>
        <!--
        <p>
            <img src="~/images/icons/curriculum_vitae16.png" alt="">
            <a id="link_userProfile" href="#">Sổ tay sinh viên</a>
        </p>
        -->
        <p>
            <img src="Images/logodoimatkhau.png" alt="">
            <a id="link_userPassword" href="DoiMatKhauController">Đổi mật khẩu</a>
        </p>

        <p>
            <img src="Images/logoDangXuat.png" alt="">
            <a id="link_userLogout" href="DangXuat" onclick="return confirm('Thoát khỏi hệ thống')">Đăng xuất</a>
        </p>
    </div>

<div class="hitec-general-function">
    <h5>CÁC CHỨC NĂNG CHUNG</h5>
    <p>
        <img src="Images/logothudientu.png" alt="">
        <a id="linkNews" href="/News">Tin tức - Thông báo <span class="notification-news"></span></a>
    </p>
    <p>
        <img src="Images/logothoikhoabieu.png" alt="">
        <a id="linkCalendar" href="/TimeTable/Week">Thời khóa biểu học tập<span class="notification-calendar"></span></a>
    </p>    
    <p>
        <img src="Images/logoemail.png" alt="">
        <a id="linkMessage" href="/Message/Inbox">Tin nhắn <span class="notification-message"></span></a>
    </p>   
	<p>
        <img src="Images/logothudientu.png" alt="">
        <a id="linkEmail" href="https://mail.google.com/a/husc.edu.vn" target="_blank">Thư điện tử</a> <img src="/images/new.gif" alt="">
    </p>	
</div>





<div id="hotNews" class="hitec-notification">
    <h5>TIN TỨC - THÔNG BÁO</h5>
    <ul>
            <li>
                <a href="/News/Content/so-tay-hoc-vu-va-thoi-khoa-bieu-du-kien-hoc-ky-2-nam-hoc-2022-2023/">Sổ tay học vụ v&#224; thời kh&#243;a biểu dự kiến học kỳ 2, năm học 2022-2023 <small class="text-muted">(06/12/2022)</small></a>
            </li>
            <li>
                <a href="/News/Content/quyet-dinh-ve-viec-cong-nhan-tot-nghiep-dai-hoc-he-chinh-quy-dot-3-nam-2022/">Quyết định về việc c&#244;ng nhận tốt nghiệp đại học hệ ch&#237;nh quy đợt 3 năm 2022 <small class="text-muted">(31/10/2022)</small></a>
            </li>
            <li>
                <a href="/News/Content/thong-bao-ket-qua-ra-soat-dieu-kien-xet-tot-nghiep-he-chinh-quy-dot-3-nam-2022/">Th&#244;ng b&#225;o Kết quả r&#224; so&#225;t điều kiện x&#233;t tốt nghiệp hệ ch&#237;nh quy, đợt 3 năm 2022 <small class="text-muted">(25/10/2022)</small></a>
            </li>
            <li>
                <a href="/News/Content/thong-bao-v-v-to-chuc-le-be-giang-va-trao-bang-tot-nghiep-dai-hoc-nam-2022/">Th&#244;ng b&#225;o v/v tổ chức Lễ bế giảng v&#224; trao bằng tốt nghiệp đại học năm 2022 <small class="text-muted">(14/06/2022)</small></a>
            </li>
            <li>
                <a href="/News/Content/ke-hoach-dao-tao-va-thoi-khoa-bieu-du-kien-hoc-ky-3-nam-hoc-2021-2022/">Kế hoạch đ&#224;o tạo v&#224; Thời kh&#243;a biểu dự kiến học kỳ 3, năm học 2021-2022 <small class="text-muted">(27/05/2022)</small></a>
            </li>
    </ul>
</div>


            </div>
        </div>
        <!-- Right panel -->
        
        <!-- Main content -->
        <div class="panel-main-content">
            <div class="hitec-content">
                <div class="row">
                    <div class="col-xs-12">
                        <img id="ajax-loader" src="/images/icons/ajax-loader.gif" alt="" />
                        

<h2>DANH SÁCH HỌC PHẦN ĐƯỢC GIẢNG DẠY TRONG KẾ HOẠCH ĐÀO TẠO</h2>

<div class="container-fluid text-right" style="margin-bottom:5px;">
    <div class="btn-group">
        <a href="DangKiHoctapController" class="btn btn-default">Danh sách lớp đã đăng ký</a>
    </div>
</div>

<div class="container-fluid" style="margin-top:10px;">
    <div class="alert alert-info alert-dismissable">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <b>Sinh viên lưu ý: </b>Ưu tiên chọn và đăng ký học các học phần theo kế hoạch đào tạo của khóa/ngành, đăng ký học đầy đủ các học phần bắt buộc trong kế hoạch.
    </div>
</div>

<div class="container-fluid">
    <ul class="nav nav-tabs">
        <li class="active"><a data-toggle="tab" href="#modulesIn">Các học phần theo Kế hoạch đào tạo</a></li>
    </ul>

    <div class="tab-content">

        <!-- Tab các học phần trong kế hoạch -->
        <div id="modulesIn" class="tab-pane fade in active table-bordered">
            <table class="table table-hover table-striped">
                <thead>
                    <tr>
                        <th style="width:35px" class="text-center">STT</th>
                        <th style="width:92px">Mã học phần</th>
                        <th style="width:auto">Tên học phần</th>
                        <th style="width:75px" class="text-center">Số<br />tín chỉ</th>
                        <th style="width:75px" class="text-center">Số lớp<br />đã mở ĐK</th>
                        <th style="width:70px" class="text-center">HP đã<br />đăng ký</th>
                        <th style="width:20px"></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td colspan="7" class="table-cell-title">
                            <b>Các học phần bắt buộc</b>
                        </td>
                    </tr>
                    	<c:set var="i" value="${1}"></c:set>
                    	<%
                    	Monhoc_bo m_bo=new Monhoc_bo();
                    	ThamGiaHoc_bo tg_bo=new ThamGiaHoc_bo();
                    	SinhVien_bean sv=(SinhVien_bean) session.getAttribute("ten");
                    	String maSv=sv.getMasv();
                    	for(MonHoc_bean m:m_bo.Getmonhocs())
                    	{
                    	%>
	                    		<tr>
                                <td class="text-center">${i}</td>
                                <td class="text-left">
                                    <a href="KetQuaHocTapController?MaMon=<%=m.getMamonhoc()%>"><%=m.getMamonhoc()%></a>
                                </td>
                                <td class="text-left">
                                    <a href="KetQuaHocTapController?MaMon=<%=m.getMamonhoc()%>"><%=m.getTenmonhoc()%></a>
                                </td>
                                <td class="text-center"><%=m.getSODVHT()%></td>
                                <td class="text-center"><%=l_bo.Soluonglop(m.getMamonhoc())%></td>
                                <%if(tg_bo.Dadangky(maSv, m.getMamonhoc())>0) {%>
                                <td class="text-center">
                                    &#10003;
                                </td>
                                <% }else  {%>
                                <td class="text-center">
                                
                                </td>
                                <%} %>
                                <td class="text-center">
                                    <a href="KetQuaHocTapController?MaMon=<%=m.getMamonhoc()%>" title="Xem danh sách các nhóm lớp của học phần: Java n&#226;ng cao">
                                        *
                                    </a>
                                </td>
                                <c:set var="i" value="${i+1}"></c:set>
                            </tr>
	                    <%}%>
                            
                           </tbody>
                           </table>
					 </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>