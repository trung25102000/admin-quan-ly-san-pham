<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sinh viên - Trường Đại học Khoa học Huế - Tin tức - th&#244;ng b&#225;o</title>
   

    <link rel="shortcut icon" type="image/x-icon" href="/favicon.ico" />
    <link href="Themes/sinhvien.css" rel="stylesheet" />
    <link href="Themes/sinhvien1.css" rel="stylesheet" />
    <link href="Themes/sinhviensite.css" rel="stylesheet" />
    <link href="Themes/bootstrap-datepicker3.min.css" rel="stylesheet" />
    <script src="Script/sinhvien.js"></script> 
    
</head>
<c:if test="${daxoa!=null}">
<script>
	alert('Hủy học phần thành công!')
</script>
</c:if>
<body>
    <!-- header-->
<!-- Header and Menu -->
<div class="container-fluid">
    <!-- Tiêu đề website -->
    <div class="row hitec-title">
        <div class="col-xs-8 nopadding">
            <h1>TRƯỜNG ĐẠI HỌC KHOA HỌC - ĐẠI HỌC HUẾ</h1>
        </div>
        <div class="col-xs-4 text-right" style="color:#cecece; line-height:28px">
            ${date}
        </div>
    </div>

    <!-- Thanh menu chính -->
    <div class="row">
        <div class="col-xs-12 nopadding">
            <nav class="navbar navbar-default hitec-navbar">
                <div class="navbar-header">
                    <button type="button" class="pull-left navbar-toggle" data-toggle="collapse" data-target="#menuMain">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand hitec-logo" href="KiemTraDangNhapSV">
                        <img src="Images/logoHue2.jpg" alt="logo">
                    </a>
                </div>

                <!-- menu main -->
                <div class="nav collapse navbar-collapse" id="menuMain">
                   <ul class="nav navbar-nav">
                        <li class="dropdown">
                            <a href="DangKiHocTapController" class="dropdown-toggle" data-toggle="dropdown">Đăng kí học tập <b class="caret"></b></a>
                        </li>

                        <li class="dropdown">
                            <a href="HocPhanDangKiController" class="dropdown-toggle" data-toggle="dropdown">Lớp Học Phần đã Đăng ký <b class="caret"></b></a>
                        </li>
						
						 <li class="dropdown">
                            <a href="KetQuaQuaTrinhController" class="dropdown-toggle" data-toggle="dropdown">Kết Quả Học tập <b class="caret"></b></a>
                        </li>
                        
                        <li class="dropdown">
                            <a href="LichSuController" class="dropdown-toggle" data-toggle="dropdown">Lịch Sử quá trình học tập <b class="caret"></b></a>
                        </li>
                        
                        <li class="dropdown">
                            <a href="LopHocPhanDuocDuyetController" class="dropdown-toggle" data-toggle="dropdown">Lớp học phần đã được duyệt <b class="caret"></b></a>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
    </div>

    <!-- Thông tin về khóa/ngành, học kỳ tác nghiệp -->

    <div class="row" style="line-height:23px; font-size:13px;">
        <div class="col-xs-12 text-right">
            <a href="/Setting/Change" style="font-weight:bold; color:#ce2020" title="Nhấp vào đây để thay đổi ngành học và học kỳ tác nghiệp">
                <b class="glyphicon glyphicon-education"></b> Kh&#243;a ${lop.getKhoa()} (${lop.getNamnhaphoc()}-${lop.getNamnhaphoc()+4})
                <b class="glyphicon glyphicon-book" style="margin-left:20px;"></b> ${khoa.getTenkhoa()}
                <b class="glyphicon glyphicon-calendar" style="margin-left:20px;"></b>  năm học: ${year}-${year+1}
            </a>
        </div>
    </div>
</div>

    <!-- Panel left and main content-->
    <div class="container-fluid" id="wrapper">
        <!-- Left panel -->
        <div class="panel-sidebar-left">
            <div class="hitec-sidebar">

    <div class="hitec-information">
        <h5>
            ${sessionScope.ten.getHodem()} ${sessionScope.ten.getTen()}
        </h5>        
        <p>
            <img src="Images/logoCanhan.png" alt="">
            <a id="link_userProfile" href="Thongtincanhan">Lý lịch cá nhân</a>
        </p>
        <!--
        <p>
            <img src="~/images/icons/curriculum_vitae16.png" alt="">
            <a id="link_userProfile" href="#">Sổ tay sinh viên</a>
        </p>
        -->
        <p>
            <img src="Images/logodoimatkhau.png" alt="">
            <a id="link_userPassword" href="DoiMatKhauController">Đổi mật khẩu</a>
        </p>

        <p>
            <img src="Images/logoDangXuat.png" alt="">
            <a id="link_userLogout" href="DangXuat" onclick="return confirm('Thoát khỏi hệ thống')">Đăng xuất</a>
        </p>
    </div>

<div class="hitec-general-function">
    <h5>CÁC CHỨC NĂNG CHUNG</h5>
    <p>
        <img src="Images/logothudientu.png" alt="">
        <a id="linkNews" href="/News">Tin tức - Thông báo <span class="notification-news"></span></a>
    </p>
    <p>
        <img src="Images/logothoikhoabieu.png" alt="">
        <a id="linkCalendar" href="/TimeTable/Week">Thời khóa biểu học tập<span class="notification-calendar"></span></a>
    </p>    
    <p>
        <img src="Images/logoemail.png" alt="">
        <a id="linkMessage" href="/Message/Inbox">Tin nhắn <span class="notification-message"></span></a>
    </p>   
	<p>
        <img src="Images/logothudientu.png" alt="">
        <a id="linkEmail" href="https://mail.google.com/a/husc.edu.vn" target="_blank">Thư điện tử</a> <img src="/images/new.gif" alt="">
    </p>	
</div>





<div id="hotNews" class="hitec-notification">
    <h5>TIN TỨC - THÔNG BÁO</h5>
    <ul>
            <li>
                <a href="/News/Content/so-tay-hoc-vu-va-thoi-khoa-bieu-du-kien-hoc-ky-2-nam-hoc-2022-2023/">Sổ tay học vụ v&#224; thời kh&#243;a biểu dự kiến học kỳ 2, năm học 2022-2023 <small class="text-muted">(06/12/2022)</small></a>
            </li>
            <li>
                <a href="/News/Content/quyet-dinh-ve-viec-cong-nhan-tot-nghiep-dai-hoc-he-chinh-quy-dot-3-nam-2022/">Quyết định về việc c&#244;ng nhận tốt nghiệp đại học hệ ch&#237;nh quy đợt 3 năm 2022 <small class="text-muted">(31/10/2022)</small></a>
            </li>
            <li>
                <a href="/News/Content/thong-bao-ket-qua-ra-soat-dieu-kien-xet-tot-nghiep-he-chinh-quy-dot-3-nam-2022/">Th&#244;ng b&#225;o Kết quả r&#224; so&#225;t điều kiện x&#233;t tốt nghiệp hệ ch&#237;nh quy, đợt 3 năm 2022 <small class="text-muted">(25/10/2022)</small></a>
            </li>
            <li>
                <a href="/News/Content/thong-bao-v-v-to-chuc-le-be-giang-va-trao-bang-tot-nghiep-dai-hoc-nam-2022/">Th&#244;ng b&#225;o v/v tổ chức Lễ bế giảng v&#224; trao bằng tốt nghiệp đại học năm 2022 <small class="text-muted">(14/06/2022)</small></a>
            </li>
            <li>
                <a href="/News/Content/ke-hoach-dao-tao-va-thoi-khoa-bieu-du-kien-hoc-ky-3-nam-hoc-2021-2022/">Kế hoạch đ&#224;o tạo v&#224; Thời kh&#243;a biểu dự kiến học kỳ 3, năm học 2021-2022 <small class="text-muted">(27/05/2022)</small></a>
            </li>
    </ul>
</div>


            </div>
        </div>
        <!-- Right panel -->
        
        <!-- Main content -->
        <div class="panel-main-content">
            <div class="hitec-content">
                <div class="row">
                    <div class="col-xs-12">
                        <img id="ajax-loader" src="/images/icons/ajax-loader.gif" alt="" />
                        

<h2>KẾT QUẢ HỌC TẬP</h2>

<div class="container-fluid text-right" style="margin-bottom:5px;">
    <div class="btn-group">   
        <a href="LichSuController" class="btn btn-default">Lịch sử quá trình học</a>   
    </div>
</div>

<div class="container-fluid form-horizontal">
    <div class="row form-group hitec-border-bottom-dotted">
        <label class="control-label col-xs-3">Khóa nhập học:</label>
        <div class="col-xs-9">
            <p class="form-control-static">${lop.getKhoa()} (${lop.getNamnhaphoc()}-${lop.getNamnhaphoc()+4})</p>
        </div>
    </div>

    <div class="row form-group hitec-border-bottom-dotted">
        <label class="control-label col-xs-3">Ngành học:</label>
        <div class="col-xs-9">
            <p class="form-control-static">${khoa.getTenkhoa()}</p>
        </div>
    </div>

    <div class="row form-group hitec-border-bottom-dotted">
        <label class="control-label col-xs-3">Tính chất ngành học:</label>
        <div class="col-xs-9">
            <p class="form-control-static">Ng&#224;nh học ch&#237;nh</p>
        </div>
    </div>

    <div class="row form-group hitec-border-bottom-dotted">
        <label class="control-label col-xs-3">Số tín chỉ đã tích lũy:</label>
        <div class="col-xs-2">
            <p class="form-control-static">${SoTinChi}</p>
        </div>
        <label class="control-label col-xs-4">Điểm trung bình tích lũy:</label>
        <div class="col-xs-3">
            <p class="form-control-static">${DiemTichLuy}</p>
        </div>
    </div>
</div>

<table class="table table-bordered table-hover" style="margin-top:20px">
    <thead>
        <tr>
            <th class="text-center" rowspan="2" style="width:80px">Mã<br />học phần</th>
            <th class="text-center" rowspan="2">Tên học phần</th>
            <th class="text-center" rowspan="2" style="width:40px">Số TC</th>
            <th class="text-center" rowspan="2" style="width:40px">Lần học</th>
            <th class="text-center" rowspan="2" style="width:40px">Lần thi</th>
            <th class="text-center" colspan="3" style="width:180px">Điểm đánh giá học phần</th>
        </tr>
        <tr>
            <th class="text-center" style="width:60px">Hệ 10</th>
            <th class="text-center" style="width:60px">Đ.Chữ</th>
            <th class="text-center" style="width:60px">Hệ 4</th>
        </tr>
    </thead>
    <tbody>
                <tr>
                    <td colspan="6" class="table-cell-title">
                        Tổng số TC đăng ký: <b>${SoTinChi}</b><br />Tổng số TC tích lũy: <b>${SoTinChi}</b>
                    </td>
                </tr>
                <c:forEach var="kq" items="${DsKetQua }">
		            <tr>
		                <td class="text-center">
		                    <a href="/Subject/Details/KNM5012/" title="Thông tin về học phần: Kỹ năng mềm">${kq.getMaMonhoc()}</a>
		                </td>
		                <td>
		                    <a href="/Subject/Details/KNM5012/" title="Thông tin về học phần: Kỹ năng mềm">${kq.getTenMonHoc()}</a>
		                </td>
		                <td class="text-center">${kq.getSoDVHT()}</td>
		                <td class="text-center">1</td>
		                <td class="text-center">1</td>
		                <td class="text-center">${kq.getDiemTB()}</td>
		                <td class="text-center">${kq.getKetQua()}</td>
		                <td class="text-center">${kq.getDiemheso()}</td>
		            </tr>
		           </c:forEach>
            </tbody>
            </table>

                    </div>
                </div>
            </div>
        </div>
    </div>

</body>
</html>