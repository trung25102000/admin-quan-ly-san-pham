<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sinh viên - Trường Đại học Khoa học Huế - Tin tức - th&#244;ng b&#225;o</title>
   

    <link rel="shortcut icon" type="image/x-icon" href="/favicon.ico" />
    <link href="Themes/sinhvien.css" rel="stylesheet" />
    <link href="Themes/sinhvien1.css" rel="stylesheet" />
    <link href="Themes/sinhviensite.css" rel="stylesheet" />
    <link href="Themes/bootstrap-datepicker3.min.css" rel="stylesheet" />
    <script src="Script/sinhvien.js"></script> 
    
</head>
<c:if test="${doithanhcong!=null}">
<script>
	alert('Đổi Mật Khẩu Thành Công!')
</script>
</c:if>
<body>
    <!-- header-->
<!-- Header and Menu -->
<div class="container-fluid">
    <!-- Tiêu đề website -->
    <div class="row hitec-title">
        <div class="col-xs-8 nopadding">
            <h1>TRƯỜNG ĐẠI HỌC KHOA HỌC - ĐẠI HỌC HUẾ</h1>
        </div>
        <div class="col-xs-4 text-right" style="color:#cecece; line-height:28px">
            ${date}
        </div>
    </div>

    <!-- Thanh menu chính -->
    <div class="row">
        <div class="col-xs-12 nopadding">
            <nav class="navbar navbar-default hitec-navbar">
                <div class="navbar-header">
                    <button type="button" class="pull-left navbar-toggle" data-toggle="collapse" data-target="#menuMain">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand hitec-logo" href="KiemTraDangNhapSV">
                        <img src="Images/logoHue2.jpg" alt="logo">
                    </a>
                </div>

                <!-- menu main -->
                <div class="nav collapse navbar-collapse" id="menuMain">
                   <ul class="nav navbar-nav">
                        <li class="dropdown">
                            <a href="DangKiHocTapController" class="dropdown-toggle" data-toggle="dropdown">Đăng kí học tập <b class="caret"></b></a>
                        </li>

                        <li class="dropdown">
                            <a href="HocPhanDangKiController" class="dropdown-toggle" data-toggle="dropdown">Lớp Học Phần đã Đăng ký <b class="caret"></b></a>
                        </li>
                        
                         <li class="dropdown">
                            <a href="KetQuaQuaTrinhController" class="dropdown-toggle" data-toggle="dropdown">Kết Quả Học tập <b class="caret"></b></a>
                        </li>
                        
                        <li class="dropdown">
                            <a href="LichSuController" class="dropdown-toggle" data-toggle="dropdown">Lịch Sử quá trình học tập <b class="caret"></b></a>
                        </li>
                        
                         <li class="dropdown">
                            <a href="LopHocPhanDuocDuyetController" class="dropdown-toggle" data-toggle="dropdown">Lớp học phần đã được duyệt <b class="caret"></b></a>
                        </li>

                    </ul>
                </div>
            </nav>
        </div>
    </div>

    <!-- Thông tin về khóa/ngành, học kỳ tác nghiệp -->

    <div class="row" style="line-height:23px; font-size:13px;">
        <div class="col-xs-12 text-right">
            <a href="/Setting/Change" style="font-weight:bold; color:#ce2020" title="Nhấp vào đây để thay đổi ngành học và học kỳ tác nghiệp">
                <b class="glyphicon glyphicon-education"></b> Kh&#243;a ${lop.getKhoa()} (${lop.getNamnhaphoc()}-${lop.getNamnhaphoc()+4})
                <b class="glyphicon glyphicon-book" style="margin-left:20px;"></b> ${khoa.getTenkhoa()}
                <b class="glyphicon glyphicon-calendar" style="margin-left:20px;"></b>  năm học: ${year}-${year+1}
            </a>
        </div>
    </div>
</div>

    <!-- Panel left and main content-->
    <div class="container-fluid" id="wrapper">
        <!-- Left panel -->
        <div class="panel-sidebar-left">
            <div class="hitec-sidebar">

    <div class="hitec-information">
        <h5>
            ${sessionScope.ten.getHodem()} ${sessionScope.ten.getTen()}
        </h5>        
        <p>
            <img src="Images/logoCanhan.png" alt="">
            <a id="link_userProfile" href="Thongtincanhan">Lý lịch cá nhân</a>
        </p>
        <!--
        <p>
            <img src="~/images/icons/curriculum_vitae16.png" alt="">
            <a id="link_userProfile" href="#">Sổ tay sinh viên</a>
        </p>
        -->
        <p>
            <img src="Images/logodoimatkhau.png" alt="">
            <a id="link_userPassword" href="DoiMatKhauController">Đổi mật khẩu</a>
        </p>

        <p>
            <img src="Images/logoDangXuat.png" alt="">
            <a id="link_userLogout" href="DangXuat" onclick="return confirm('Thoát khỏi hệ thống')">Đăng xuất</a>
        </p>
    </div>

<div class="hitec-general-function">
    <h5>CÁC CHỨC NĂNG CHUNG</h5>
    <p>
        <img src="Images/logothudientu.png" alt="">
        <a id="linkNews" href="/News">Tin tức - Thông báo <span class="notification-news"></span></a>
    </p>
    <p>
        <img src="Images/logothoikhoabieu.png" alt="">
        <a id="linkCalendar" href="/TimeTable/Week">Thời khóa biểu học tập<span class="notification-calendar"></span></a>
    </p>    
    <p>
        <img src="Images/logoemail.png" alt="">
        <a id="linkMessage" href="/Message/Inbox">Tin nhắn <span class="notification-message"></span></a>
    </p>   
	<p>
        <img src="Images/logothudientu.png" alt="">
        <a id="linkEmail" href="https://mail.google.com/a/husc.edu.vn" target="_blank">Thư điện tử</a> <img src="/images/new.gif" alt="">
    </p>	
</div>





<div id="hotNews" class="hitec-notification">
    <h5>TIN TỨC - THÔNG BÁO</h5>
    <ul>
            <li>
                <a href="/News/Content/so-tay-hoc-vu-va-thoi-khoa-bieu-du-kien-hoc-ky-2-nam-hoc-2022-2023/">Sổ tay học vụ v&#224; thời kh&#243;a biểu dự kiến học kỳ 2, năm học 2022-2023 <small class="text-muted">(06/12/2022)</small></a>
            </li>
            <li>
                <a href="/News/Content/quyet-dinh-ve-viec-cong-nhan-tot-nghiep-dai-hoc-he-chinh-quy-dot-3-nam-2022/">Quyết định về việc c&#244;ng nhận tốt nghiệp đại học hệ ch&#237;nh quy đợt 3 năm 2022 <small class="text-muted">(31/10/2022)</small></a>
            </li>
            <li>
                <a href="/News/Content/thong-bao-ket-qua-ra-soat-dieu-kien-xet-tot-nghiep-he-chinh-quy-dot-3-nam-2022/">Th&#244;ng b&#225;o Kết quả r&#224; so&#225;t điều kiện x&#233;t tốt nghiệp hệ ch&#237;nh quy, đợt 3 năm 2022 <small class="text-muted">(25/10/2022)</small></a>
            </li>
            <li>
                <a href="/News/Content/thong-bao-v-v-to-chuc-le-be-giang-va-trao-bang-tot-nghiep-dai-hoc-nam-2022/">Th&#244;ng b&#225;o v/v tổ chức Lễ bế giảng v&#224; trao bằng tốt nghiệp đại học năm 2022 <small class="text-muted">(14/06/2022)</small></a>
            </li>
            <li>
                <a href="/News/Content/ke-hoach-dao-tao-va-thoi-khoa-bieu-du-kien-hoc-ky-3-nam-hoc-2021-2022/">Kế hoạch đ&#224;o tạo v&#224; Thời kh&#243;a biểu dự kiến học kỳ 3, năm học 2021-2022 <small class="text-muted">(27/05/2022)</small></a>
            </li>
    </ul>
</div>


            </div>
        </div>
        <!-- Right panel -->
        
        <!-- Main content -->
        <div class="panel-main-content">
            <div class="hitec-content">
                <div class="row">
                    <div class="col-xs-12">
                        <img id="ajax-loader" src="/images/icons/ajax-loader.gif" alt="" />
                        

<h2>THÔNG BÁO</h2>
<div class="container-fluid text-justify" style="margin-bottom:20px">
	<a href="https://drive.google.com/file/d/186u0UJCfr2NcliBsQ6Fqb6N5PAfAiw3l/" target="_blank">
		<img src="/Images/hackathon_2022.png" alt="Hackathon 2022" style="width:100%" />
	</a>
</div>
<!--
<div class="container-fluid">
		<div class="alert alert-success">    
			<strong>Lưu ý:</strong> Để chuẩn bị cho việc triển khai học trực tuyến, Nhà trường lưu ý sinh viên về việc sử dụng hộp thư điện tử như sau:
			<ul>
				<li>Mỗi sinh viên được cấp một hộp thư (email) có dạng: <b>mã_sinh_viên@husc.edu.vn</b></li>
				<li>Sinh viên đăng nhập vào hộp thư với tên đăng nhập là <b>mã_sinh_ viên@husc.edu.vn</b>, mật khẩu truy cập ban đầu là <b>mật khẩu mà sinh viên hiện đang sử dụng để truy cập trang tín chỉ</b>.</li>				
				<li>Sinh viên chú ý đảm bảo an toàn cho tài khoản thư cá nhân.</li>
			</ul>
			<br />
			<p>
			Xem tài liệu hướng dẫn sử dụng Google Classroom và Google Meet <a href="http://ums.husc.edu.vn/uploads/Guide_to_Google_ClassRoom_and_Meet.pdf" target="_blank"><b>tại đây</b></a>
			</p>
			<p style="text-align:left">
				<a class="btn btn-info" href="https://mail.google.com/a/husc.edu.vn" target="_blank"><span class="glyphicon glyphicon-envelope"></span> Nhấp vào đây để truy cập hộp thư</a>
			</p>
		</div>
</div>
-->
<div id="reminders" class="container-fluid"></div>
<div id="newsList" class="container-fluid">
        <div style="margin-bottom:15px">
            <p style="font-weight:bold">
                <a href="/News/Content/thong-bao-ve-viec-dieu-chinh-ke-hoach-dao-tao-nganh-xa-hoi-hoc-khoa-44-va-45-hoc-ky-2-nam-hoc-2022-2023/">Th&#244;ng b&#225;o về việc điều chỉnh kế hoạch đ&#224;o tạo Ng&#224;nh X&#227; hội học Kh&#243;a 44 v&#224; 45 học kỳ 2, năm học 2022-2023</a>
                <br />
                <small class="text-muted">[16/12/2022 08:22]</small>
            </p>
            <p>
                Căn cứ Tờ trình số 18/TTr-XHH&CTXH của Phụ trách Khoa Xã hội học và Công tác xã hội ngày 12/12/2022 về việc điều chỉnh Kế hoạch đào tạo học kỳ 2, năm học 2022-2023 của các lớp Xã hội học Khóa 44 và 45, Nhà trường thông báo đến các đơn vị, giảng viên và sinh viên được biết kế hoạch đào tạo học kỳ 2, năm học 2022-2023 có sự thay đổi như sau:
            </p>
        </div>
        <div style="margin-bottom:15px">
            <p style="font-weight:bold">
                <a href="/News/Content/thong-bao-ve-viec-mo-them-cac-nhom-lop-hoc-phan-thiet-ke-co-so-du-lieu-ma-hoc-phan-tin4012-nhom-6-7-8-9-10/">Th&#244;ng b&#225;o về việc mở th&#234;m c&#225;c nh&#243;m lớp học phần Thiết kế cơ sở dữ liệu (m&#227; học phần TIN4012) - Nh&#243;m 6,7,8,9,10</a>
                <br />
                <small class="text-muted">[14/12/2022 10:31]</small>
            </p>
            <p>
                Căn cứ nhu cầu thực tế sinh viên đăng ký học phần Thiết kế cơ sở dữ liệu (mã học phần TIN4012) , Nhà trường đã mở thêm một số nhóm lớp học phần này trên Website Thông tin đào tạo đại học, cụ thể các nhóm học phần Thiết kế cơ sở dữ liệu (mã học phần TIN4012) - Nhóm 6,7,8,9,10. 
Vậy, Nhà trường thông báo đến sinh viên được biết để đăng ký đúng thời gian quy định.


            </p>
        </div>
        <div style="margin-bottom:15px">
            <p style="font-weight:bold">
                <a href="/News/Content/dieu-chinh-lich-thi-ket-thuc-hoc-phan-hoc-ky-1-nam-hoc-2022-2023-lan-1/">Điều chỉnh lịch thi kết th&#250;c học phần học kỳ 1, năm học 2022-2023 (Lần 1)</a>
                <br />
                <small class="text-muted">[13/12/2022 16:22]</small>
            </p>
            <p>
                Trường Đại học Khoa học thông báo đến giảng viên và sinh viên về việc điều chỉnh ịch thi kết thúc học phần học kỳ 1, năm học 2022-2023 (Lần 1). Để biết thêm thông tin chi tiết, đề nghị giảng viên và sinh viên theo dõi thông qua Website Thông tin đào tạo đại học hoặc truy cập vào nội dung được đính kèm sau đây:
            </p>
        </div>
        <div style="margin-bottom:15px">
            <p style="font-weight:bold">
                <a href="/News/Content/thong-bao-ve-viec-cung-cap-so-lieu-kiem-tra-gio-giang-dai-hoc-hoc-ky-1-nam-hoc-2022-2023/">Th&#244;ng b&#225;o về việc cung cấp số liệu, kiểm tra giờ giảng đại học, học kỳ 1, năm học 2022-2023</a>
                <br />
                <small class="text-muted">[13/12/2022 09:01]</small>
            </p>
            <p>
                Để phục vụ cho việc cập nhật số liệu về giảng dạy đại học, kiểm tra và phản hồi kết quả thống kê giờ quy chuẩn của học kỳ 1, năm học 2022-2023, Hiệu trưởng yêu cầu Trưởng các đơn vị và giảng viên thực hiện một số nhiệm vụ sau (xem chi tiết tại Nội dung đính kèm)
            </p>
        </div>
        <div style="margin-bottom:15px">
            <p style="font-weight:bold">
                <a href="/News/Content/thong-bao-ve-viec-nop-hoc-phi-giao-duc-the-chat-hoc-ky-1-nam-hoc-2022-2023/">Th&#244;ng b&#225;o về việc nộp học ph&#237; Gi&#225;o dục thể chất học kỳ 1 năm học 2022-2023 </a>
                <br />
                <small class="text-muted">[09/12/2022 17:04]</small>
            </p>
            <p>
                Khoa Giáo dục thể chất, Đại học Huế thông báo đến sinh viên được biết về việc nộp học phí Giáo dục thể chất học kỳ 1 năm học 2022-2023 dành cho sinh viên khối Không chuyên theo link đính kèm bên dưới.
            </p>
        </div>
        <div style="margin-bottom:15px">
            <p style="font-weight:bold">
                <a href="/News/Content/thong-bao-ve-viec-nhan-chi-thuong-cho-doi-tuong-sinh-vien-cua-truong-dat-dieu-kien-chi-thuong-chuong-trinh-di-hoc-vui-tui-day-uu-dai-tang-50-000-vao-tai-khoan-sinh-vien/">Th&#244;ng b&#225;o về việc nhận chi thưởng cho đối tượng sinh vi&#234;n của trường đạt điều kiện chi thưởng chương tr&#236;nh - &quot;Đi học vui - T&#250;i đầy ưu đ&#227;i&quot;, tặng 50,000 v&#224;o t&#224;i khoản Sinh vi&#234;n</a>
                <br />
                <small class="text-muted">[09/12/2022 10:26]</small>
            </p>
            <p>
                Nhà Trường thông báo đến sinh viên K46 về việc nhận chi thưởng cho đối tượng sinh viên của trường đạt điều kiện chi thưởng chương trình - "Đi học vui - Túi đầy ưu đãi", tặng 50,000 vào tài khoản Sinh viên.

            </p>
        </div>
        <div style="margin-bottom:15px">
            <p style="font-weight:bold">
                <a href="/News/Content/thong-bao-ve-viec-cap-nhat-thong-tin-can-cuoc-cong-dan-doi-voi-sinh-vien/">Th&#244;ng b&#225;o về việc cập nhật th&#244;ng tin Căn cước c&#244;ng d&#226;n đối với sinh vi&#234;n</a>
                <br />
                <small class="text-muted">[09/12/2022 09:59]</small>
            </p>
            <p>
                Hiện nay, dữ liệu Lý lịch cá nhân của sinh viên đã có thông tin Chứng minh nhân dân hoặc Căn cước công dân của người học cung cấp. Tuy nhiên, hệ thống quản lý người học của Bộ Giáo dục và Đào tạo hiện nay chỉ quản lý thông tin sinh viên thông qua Căn cước công dân, vì vậy, Nhà trường đề nghị sinh viên đã cung cấp dữ liệu CMND cập nhật lại bằng thông tin CCCD trước 17h00 ngày 11/12/2022.
            </p>
        </div>
        <div style="margin-bottom:15px">
            <p style="font-weight:bold">
                <a href="/News/Content/truong-dai-hoc-ngoai-ngu-thong-bao-to-chuc-ky-thi-khao-sat-dau-vao-tieng-anh-ngoai-ngu-khong-chuyen-cho-sinh-vien-khoa-tuyen-sinh-2022-va-cac-khoa-ve-truoc-dot-2/">TRƯỜNG ĐẠI HỌC NGOẠI NGỮ TH&#212;NG B&#193;O TỔ CHỨC KỲ THI KHẢO S&#193;T ĐẦU V&#192;O TIẾNG ANH, NGOẠI NGỮ KH&#212;NG CHUY&#202;N CHO SINH VI&#202;N KH&#211;A TUYỂN SINH 2022 V&#192; C&#193;C KH&#211;A VỀ TRƯỚC (ĐỢT 2)</a>
                <br />
                <small class="text-muted">[07/12/2022 16:24]</small>
            </p>
            <p>
                Trường Đại học Ngoại ngữ, Đại học Huế đã tổ chức khảo sát kiểm tra đầu vào, ngoại ngữ tiếng Anh cho sinh viên các trường thành viên Đại học Huế vào tháng 10/2022. Tuy nhiên, có nhiều sinh viên chưa đăng ký dự thi và chưa học NNKC theo đúng tiến độ. Căn cứ vào kết quả kiểm tra đầu vào, sinh viên có thể được miễn học các cấp độ thấp A1, A2, thậm chí có thể được miễn học cả 3 cấp độ. Nếu không dự thi kiểm tra đầu vào, sinh viên không được xếp vào cấp độ để học và không thể đăng ký lớp để học được. Vì vậy, Trường sẽ tổ chức 1 kỳ thi kiểm tra đầu vào bổ sung (đợt 3) theo kế hoạch sau: 
            </p>
        </div>
        <div style="margin-bottom:15px">
            <p style="font-weight:bold">
                <a href="/News/Content/thong-bao-nhan-giay-chung-nhan-trung-tam-do-luong-thu-nghiem-va-thong-tin-khoa-hoc-hue-cmtsi/">Th&#244;ng b&#225;o nhận giấy chứng nhận Trung t&#226;m đo lường, thử nghiệm v&#224; th&#244;ng tin khoa học - HUE CMTSI</a>
                <br />
                <small class="text-muted">[07/12/2022 13:43]</small>
            </p>
            <p>
                Phòng Đào tạo Đại học và Công tác sinh viên thông báo đến các bạn sinh viên các khóa có tham gia học lớp Đào tạo kiến thức về hệ thống quản lý công cụ cải tiến năng suất chất lượng, đến phòng ĐTĐH&CTSV gặp cô Bạch Liên để nhận chứng chỉ.
            </p>
        </div>
        <div style="margin-bottom:15px">
            <p style="font-weight:bold">
                <a href="/News/Content/so-tay-hoc-vu-va-thoi-khoa-bieu-du-kien-hoc-ky-2-nam-hoc-2022-2023/">Sổ tay học vụ v&#224; thời kh&#243;a biểu dự kiến học kỳ 2, năm học 2022-2023</a>
                <br />
                <small class="text-muted">[06/12/2022 18:45]</small>
            </p>
            <p>
                Nhà trường thông báo đến giảng viên và sinh viên toàn trường được biết Sổ tay học vụ và thời khóa biểu dự kiến học kỳ 2, năm học 2022-2023. Chi tiết được đính kèm theo các link bên dưới.

            </p>
        </div>
        <div style="margin-bottom:15px">
            <p style="font-weight:bold">
                <a href="/News/Content/lich-thi-ket-thuc-hoc-phan-hoc-ky-1-nam-hoc-2022-2023/">Lịch thi kết th&#250;c học phần học kỳ 1, năm học 2022-2023</a>
                <br />
                <small class="text-muted">[05/12/2022 07:47]</small>
            </p>
            <p>
                Trường Đại học Khoa học thông báo đến giảng viên và sinh viên Lịch thi kết thúc học phần học kỳ 1, năm học 2022-2023. Để biết thêm thông tin chi tiết, đề nghị giảng viên và sinh viên theo dõi thông qua Website Thông tin đào tạo đại học hoặc truy cập vào nội dung được đính kèm sau đây:
            </p>
        </div>
        <div style="margin-bottom:15px">
            <p style="font-weight:bold">
                <a href="/News/Content/thong-bao-ve-viec-cap-nhat-ly-lich-ca-nhan-cua-sinh-vien/">Th&#244;ng b&#225;o về việc cập nhật L&#253; lịch c&#225; nh&#226;n của sinh vi&#234;n</a>
                <br />
                <small class="text-muted">[03/12/2022 11:21]</small>
            </p>
            <p>
                Nhằm thực hiện tốt công tác hỗ trợ, quản lý người học tại Nhà trường, yêu cầu tất cả sinh viên phải khai báo đầy đủ, chính xác các thông tin cá nhân. Trường hợp sinh viên chưa khai báo đầy đủ sẽ không thể thực hiện các thao tác nghiệp vụ online liên quan đến học tập tại trang tín chỉ.
            </p>
        </div>
        <div style="margin-bottom:15px">
            <p style="font-weight:bold">
                <a href="/News/Content/thong-bao-ve-viec-dang-ky-lam-the-ngan-hang-danh-cho-sinh-vien-nam-nhat-k46/">Th&#244;ng b&#225;o về việc đăng k&#253; l&#224;m thẻ ng&#226;n h&#224;ng d&#224;nh cho sinh vi&#234;n năm Nhất - K46 chưa c&#243; thẻ</a>
                <br />
                <small class="text-muted">[25/11/2022 14:50]</small>
            </p>
            <p>
                Sinh viên có tên trong danh sách liên hệ ngân hàng Vietinbank - Phòng giao dịch số 07 Nguyễn Huệ, Phường Phú Nhuận, Tp. Huế để đăng ký.
            </p>
        </div>
        <div style="margin-bottom:15px">
            <p style="font-weight:bold">
                <a href="/News/Content/nhac-nho-sinh-vien-ve-viec-khai-bao-thong-tin-tiem-chung-vaccine-covid-19/">[NHẮC NHỞ SINH VI&#202;N] Về việc khai b&#225;o th&#244;ng tin ti&#234;m chủng Vaccine Covid-19</a>
                <br />
                <small class="text-muted">[24/11/2022 11:29]</small>
            </p>
            <p>
                Nhà trường sẽ tiến hành khóa tài khoản sinh viên và thi hành quyết định kỷ luật cảnh cáo đối với những sinh viên không thực hiện khai báo thông tin tiêm chủng Covid - 19 sau 15h00 ngày 24/11/2022 (hôm nay).
            </p>
        </div>
        <div style="margin-bottom:15px">
            <p style="font-weight:bold">
                <a href="/News/Content/thong-bao-ve-viec-cung-cap-thong-tin-tiem-chung-vaccine-covid-19/">Th&#244;ng b&#225;o về việc cung cấp th&#244;ng tin ti&#234;m chủng vaccine Covid-19</a>
                <br />
                <small class="text-muted">[22/11/2022 11:25]</small>
            </p>
            <p>
                Hiện nay, tình hình dịch bệnh Covid-19 vẫn diễn ra phức tạp, nhằm tăng cường công tác phòng, chống dịch và có căn cứ đề nghị tiêm chủng bổ sung vaccine, Nhà trường đề nghị tất cả sinh viên khai báo theo form đính kèm
            </p>
        </div>
        <div style="margin-bottom:15px">
            <p style="font-weight:bold">
                <a href="/News/Content/nop-hoc-phi-qua-trang-tin-chi-bi-loi/">NỘP HỌC PH&#205; QUA TRANG T&#205;N CHỈ BỊ LỖI</a>
                <br />
                <small class="text-muted">[22/11/2022 08:06]</small>
            </p>
            <p>
                Hiện có một số bạn sinh viên phản ánh với Nhà trường về việc nộp học phí qua trang tín chỉ bị lỗi, không nộp được học phí. Nhà trường đã liên hệ với Ngân hàng viettinbank check lại lỗi trên và phía ngân hàng yêu cầu Nhà trường cung cấp số điện thoại các bạn bị lỗi trên để họ hướng dẫn trực tiếp. Nhà trường sẽ để file ở dưới. Các bạn nộp học phí không được cung cấp thông tin vào file giúp mình nhé.
            </p>
        </div>
        <div style="margin-bottom:15px">
            <p style="font-weight:bold">
                <a href="/News/Content/thong-bao-ve-viec-nhan-bang-tot-nghiep-dai-hoc-he-chinh-quy-bo-sung-dot-3-nam-2022/">Th&#244;ng b&#225;o về việc nhận bằng tốt nghiệp đại học hệ ch&#237;nh quy bổ sung đợt 3 năm 2022</a>
                <br />
                <small class="text-muted">[21/11/2022 13:54]</small>
            </p>
            <p>
                Nhà trường thông báo đến sinh viên được xét tốt nghiệp bổ sung đợt 3 năm 2022 theo quyết định số 965/QĐ-ĐHKh ngày 31/10/2022 đến nhận bằng.
            </p>
        </div>
        <div style="margin-bottom:15px">
            <p style="font-weight:bold">
                <a href="/News/Content/thong-bao-khao-sat-ve-nhu-cau-dang-ky-hoc-phan-tu-chon-hoc-ky-2-nam-hoc-2022-2023-doi-voi-sinh-vien-khoa-45-nganh-cong-nghe-thong-tin/">Th&#244;ng b&#225;o khảo s&#225;t về nhu cầu đăng k&#253; học phần tự chọn học kỳ 2, năm học 2022-2023 đối với sinh vi&#234;n Kh&#243;a 45, ng&#224;nh C&#244;ng nghệ th&#244;ng tin</a>
                <br />
                <small class="text-muted">[21/11/2022 10:37]</small>
            </p>
            <p>
                Dựa trên thực tế số lượng sinh viên Khóa 45, ngành Công nghệ thông tin, Nhà trường tiến hành thực hiện khảo sát về việc sinh viên lựa chọn các học phần tự chọn cho học kỳ 2, năm học 2022-2023 để chuẩn bị lập kế hoạch đúng theo nhu cầu của sinh viên theo đường link bên dưới. Sinh viên thực hiện khảo sát từ ngày có thông báo đến hết ngày 24/11/2022. 


            </p>
        </div>
        <div style="margin-bottom:15px">
            <p style="font-weight:bold">
                <a href="/News/Content/thong-bao-ve-viec-thu-hoc-phi-ngoai-ngu-khong-chuyen-dot-2-hoc-ky-1-nam-hoc-2022-2023/">Th&#244;ng b&#225;o về việc THU HỌC PH&#205; NGOẠI NGỮ KH&#212;NG CHUY&#202;N ĐỢT 2 HỌC KỲ 1 NĂM HỌC 2022-2023</a>
                <br />
                <small class="text-muted">[18/11/2022 08:07]</small>
            </p>
            <p>
                Trường Đại học ngoại ngữ thông báo đến sinh viên từ ngày 21 đến ngày 25/11/2022, Trường Đại học Ngoại ngữ sẽ thu học phí học Ngoại ngữ không chuyên đợt 2 học kỳ 1 năm học 2022-2023.

Những sinh viên năm thứ hai chưa nộp học phí trong đợt 1 và những sinh viên năm thứ nhất vừa đăng ký học NNKC đợt tháng 10/2022, xem lại Thông báo số 3118 tại đường link nói trên và hoàn thành việc nộp học phí trong đợt 2 này để đủ điều kiện thi hết cấp độ cũng như chuyển tiếp lên cấp độ cao hơn. Chi tiết xem ở file đính kèm.
            </p>
        </div>
        <div style="margin-bottom:15px">
            <p style="font-weight:bold">
                <a href="/News/Content/sinh-vien-nhan-tien-khen-thuong-sinh-vien-gioi-sinh-vien-xuat-sac-nam-hoc-2021-2022/">SINH VI&#202;N NHẬN TIỀN KHEN THƯỞNG SINH VI&#202;N GIỎI, SINH VI&#202;N XUẤT SẮC NĂM HỌC 2021-2022</a>
                <br />
                <small class="text-muted">[17/11/2022 14:21]</small>
            </p>
            <p>
                Theo Quyết định số 938/QĐ-ĐHKH ngày 25/10/2022 về việc khen thưởng sinh viên chính quy năm học 2021-2022 có một số bạn chưa có số tài khoản sinh viên. Hiện tại các bạn bổ sung sai số tài khoản hoặc lấy số tài khoản của người khác để điền vào trang thông tin trường gửi cho các bạn gây khó khăn trong việc chuyển tiền và làm chậm trễ tiền tới các bạn khác. Vì vậy, Nhà trường sẽ chuyển tiền Khen thưởng cho các bạn có số tài khoản Vietinbank đã đăng ký cùng với thẻ sinh viên. Các số tài khoản khác và tài khoản không hợp lệ sẽ chờ các bạn bổ sung đúng số tài khoản và chuyển tiền sau. Phòng KHTC-CSVC sẽ để file đến 20/11/2022 và trong tuần kế tiếp sẽ chuyển tiền. Các bạn chưa có thẻ vui lòng ra Vietinbank 17 Nguyễn Huệ để làm thủ tục lấy thẻ và cung cấp mã thẻ để Nhà trường chuyển tiền trong tuần kế tiếp.
Lưu ý là không chỉ năm nay mà các năm sau các bạn còn nhận tiền và còn nhận các đợt hỗ trợ khác của nhà trường nên các bạn bổ sung sớm nhé.
            </p>
        </div>
</div>

                    </div>
                </div>
            </div>
        </div>
    </div>
<div class="container-fluid nopadding">
    <footer>
        <div class="row">
            <div class="col-xs-12 text-center nopadding">
                <p>Trường Đại học Khoa học – Đại học Huế</p>
                <p>Địa chỉ: 77 Nguyễn Huệ, Thành phố Huế, Thừa Thiên Huế, Việt Nam</p>
                <p>Điện thoại: (+84) 0234.3823290 – Fax: (+84) 0234.3824901</p>
                <p>Website: <a href="http://www.husc.edu.vn" target="_blank">http://www.husc.edu.vn</a></p>
				<p><small>&copy; Hoàng Quốc Trung-Công Nghệ Thông Tin</small></p>
            </div>
        </div>
    </footer>
</div>

    <!-- for main popup dialog -->
    <div id="dialogMain" class="modal fade" role="dialog"></div>
      
    <script language="javascript" type="text/javascript">
        applicationUrl = "https://student.husc.edu.vn/";
        $(document).ready(function () {
            get_notifications();
            window.setInterval(get_notifications, 60000);
            $(".hitec-datepicker").datepicker({
                format: "dd/mm/yyyy",
                language: "vi",
                autoclose: true,
                todayHighlight: true,
                weekStart: 1
            });
            $('#dialogMain').on('hidden.bs.modal', function () {
                $("#dialogMain").html("");
            })
            $(".checkbox-all").change(function () {
                var status = this.checked;
                $($(this).data("checkbox-all-range")).each(function () {
                    $(this).prop("checked", status);
                });
            });            
            triggerPopupLink();
        });
    </script>    
    
    
    <script>
        $(document).ready(function () {
            $.ajax({
                url: createBaseLink("Notification/Reminders"),
                type: "GET",                
                async: true,                
                success: function (data) { $("#reminders").html(data);}
            });
        })
    </script>

</body>
</html>