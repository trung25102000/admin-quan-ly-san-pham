<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>ĐĂNG NHẬP HỆ THỐNG</title>
    <link href="Themes/Login.css" rel="stylesheet"/>
    <link href="Themes/Login1.css" rel="stylesheet"/>
    <link href="Themes/bootstrap.min.css" rel="stylesheet"/>
</head>
<c:if test="${DangNhapSai!=null}">
	<script >
		alert('Tài khoản không tồn tại!')
	</script>
</c:if>
<body style="background-color: #006EB7">
    <div class="hitec-signin">                
            <div class="hitec-col-image text-center">
                <div class="hitec-signin-logo">
                    <a href="https://ums.husc.edu.vn"><img src="Images/Logohue.jpg" alt=""></a>
                </div>
                <div class="hitec-signin-image">
                    <img src="Images/nen.jpg" alt="">
                </div>
            </div>
            <div class="hitec-col-signin">
                <form action="KiemTraDangNhapSV" method="post" class="hitec-form-signin">
                    <input name="__RequestVerificationToken" type="hidden" value="mHaHFPj0YU9XOHm8I4MW_fA8mETo11dFhzTeQZK2YqeS5lU0TqpxP7cVTLjyCBXD80hKLbNR_KE5NLNfTbx8a5UKGYo1" />
                    <img src="Images/logo.jpg" alt="">
                    <h2 class="form-signin-heading">SINH VIÊN</h2>
                    <div class="form-group">
                        <label for="loginID">MÃ SINH VIÊN:</label>
                        <input type="text" id="loginID" name="loginID" class="form-control input-lg" placeholder="Mã sinh viên" required autofocus value="" />
                    </div>
                    <div class="form-group">
                        <label for="password">MẬT KHẨU</label>
                        <input type="password" id="password" name="password" class="form-control input-lg" placeholder="Mật khẩu" required value="" />
                    </div>
                    <div class="form-group">
                        <span class="text-danger"></span>
                    </div>
                    <button class="btn btn-lg btn-primary btn-block btn-custom" type="submit">ĐĂNG NHẬP</button>
                </form>
            </div>        
    </div>    
   
    <script src="/Scripts/jquery-1.10.2.min.js"></script>
    <script src="/Scripts/bootstrap.min.js"></script>
</body>