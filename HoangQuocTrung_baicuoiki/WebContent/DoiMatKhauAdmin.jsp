<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    <%@ page import="net.tanesha.recaptcha.ReCaptcha" %>
    <%@ page import="net.tanesha.recaptcha.ReCaptchaFactory" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sinh viên - Trường Đại học Khoa học Huế - Th&#244;ng tin c&#225; nh&#226;n</title>

    <link rel="shortcut icon" type="image/x-icon" href="/favicon.ico" />
    <link href="Themes/sinhvien.css" rel="stylesheet" />
    <link href="Themes/sinhvien1.css" rel="stylesheet" />
    <link href="Themes/bootstrap.css" rel="stylesheet" />
    <link href="Themes/sinhvien_custo" rel="stylesheet" />
    <link href="Themes/sinhviensite.css" rel="stylesheet" />
    <link href="Themes/bootstrap-datepicker3.min.css" rel="stylesheet" />

    <!-- Scripts-->
    <script src="/Scripts/jquery-1.10.2.min.js"></script>
    <script src="/Scripts/bootstrap.min.js"></script>
    <script src="/Scripts/jquery.inputmask.bundle.min.js"></script>
    <script src="/Plugins/datepicker/bootstrap-datepicker.js"></script>
    <script src="/Plugins/datepicker/bootstrap-datepicker.vi.min.js"></script>
    <script src="/Scripts/site.js?t=638068372405032359"></script> 
    
</head>
<c:if test="${chuanhaphet!=null}">
<script>
	alert('Vui lòng nhập đầy đủ thông tin!')
</script>
</c:if>
<c:if test="${xacnhansai!=null}">
<script>
	alert('Mật Khẩu xác nhận không hợp lê!')
</script>
</c:if>
<c:if test="${Matkhausai!=null}">
<script>
	alert('Mật Khẩu cũ không chính xác!')
</script>
</c:if>
<c:if test="${thatbai!=null}">
<script>
	alert('Đổi thất bại!')
</script>
</c:if>
<body>
    <!-- header-->
<!-- Header and Menu -->
<div class="container-fluid">
    <!-- Tiêu đề website -->
    <div class="row hitec-title">
        <div class="col-xs-8 nopadding">
            <h1>TRƯỜNG ĐẠI HỌC KHOA HỌC - ĐẠI HỌC HUẾ</h1>
        </div>
        <div class="col-xs-4 text-right" style="color:#cecece; line-height:28px">
            ${date}
        </div>
    </div>

    <!-- Thanh menu chính -->
    <div class="row">
        <div class="col-xs-12 nopadding">
            <nav class="navbar navbar-default hitec-navbar">
                <div class="navbar-header">
                    <button type="button" class="pull-left navbar-toggle" data-toggle="collapse" data-target="#menuMain">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand hitec-logo" href="KiemTraDangNhapAdmin">
                        <img src="Images/Logoadmin2.jpg" alt="logo">
                    </a>
                </div>

                <!-- menu main -->
                <div class="nav collapse navbar-collapse" id="menuMain">
                   <ul class="nav navbar-nav">
                        <li class="dropdown">
                            <a href="QuanLyGiaoVienController" class="dropdown-toggle" data-toggle="dropdown">Quản Lý Giáo Viên<b class="caret"></b></a>
                        </li>

                        <li class="dropdown">
                            <a href="QuanLySinhVienController" class="dropdown-toggle" data-toggle="dropdown">Quản Lý Sinh Viên<b class="caret"></b></a>
                        </li>
                        
                         <li class="dropdown">
                            <a href="XacNhanDangKyController" class="dropdown-toggle" data-toggle="dropdown">Xác Nhận Đăng ký học phần<b class="caret"></b></a>
                        </li>
                        
                        <li class="dropdown">
                            <a href="DanhSachDiemTichLuySV" class="dropdown-toggle" data-toggle="dropdown">Kết quả học tập của sinh viên<b class="caret"></b></a>
                        </li>

                    </ul>
                </div>
            </nav>
        </div>
    </div>

    <!-- Thông tin về khóa/ngành, học kỳ tác nghiệp -->

    <div class="row" style="line-height:23px; font-size:13px;">
        <div class="col-xs-12 text-right">
            <a href="/Setting/Change" style="font-weight:bold; color:#ce2020" title="Nhấp vào đây để thay đổi ngành học và học kỳ tác nghiệp">
                <b class="glyphicon glyphicon-calendar" style="margin-left:20px;"></b> Năm học: ${year}-${year+1}
            </a>
        </div>
    </div>
</div>

    <!-- Panel left and main content-->
    <div class="container-fluid" id="wrapper">
        <!-- Left panel -->
        <div class="panel-sidebar-left">
            <div class="hitec-sidebar">

    <div class="hitec-information">
        <h5>
            Quản trị Admin
        </h5>        
        <!--
        <p>
            <img src="~/images/icons/curriculum_vitae16.png" alt="">
            <a id="link_userProfile" href="#">Sổ tay sinh viên</a>
        </p>
        -->
        <p>
            <img src="Images/logodoimatkhau.png" alt="">
            <a id="link_userPassword" href="DoiMatKhauAdminController">Đổi mật khẩu</a>
        </p>

        <p>
            <img src="Images/logoDangXuat.png" alt="">
            <a id="link_userLogout" href="ThoatAdmin" onclick="return confirm('Thoát khỏi hệ thống')">Đăng xuất</a>
        </p>
    </div>

<div class="hitec-general-function">
    <h5>CÁC CHỨC NĂNG CHUNG</h5>
    <p>
        <img src="Images/logothudientu.png" alt="">
        <a id="linkNews" href="/News">Tin tức - Thông báo <span class="notification-news"></span></a>
    </p>    
    <p>
        <img src="Images/logoemail.png" alt="">
        <a id="linkMessage" href="/Message/Inbox">Tin nhắn <span class="notification-message"></span></a>
    </p>   
	<p>
        <img src="Images/logothudientu.png" alt="">
        <a id="linkEmail" href="https://mail.google.com/a/husc.edu.vn" target="_blank">Thư điện tử</a> <img src="/images/new.gif" alt="">
    </p>	
</div>





<div id="hotNews" class="hitec-notification">
    <h5>TIN TỨC - THÔNG BÁO</h5>
    <ul>
            <li>
                <a href="/News/Content/so-tay-hoc-vu-va-thoi-khoa-bieu-du-kien-hoc-ky-2-nam-hoc-2022-2023/">Sổ tay học vụ v&#224; thời kh&#243;a biểu dự kiến học kỳ 2, năm học 2022-2023 <small class="text-muted">(06/12/2022)</small></a>
            </li>
            <li>
                <a href="/News/Content/quyet-dinh-ve-viec-cong-nhan-tot-nghiep-dai-hoc-he-chinh-quy-dot-3-nam-2022/">Quyết định về việc c&#244;ng nhận tốt nghiệp đại học hệ ch&#237;nh quy đợt 3 năm 2022 <small class="text-muted">(31/10/2022)</small></a>
            </li>
            <li>
                <a href="/News/Content/thong-bao-ket-qua-ra-soat-dieu-kien-xet-tot-nghiep-he-chinh-quy-dot-3-nam-2022/">Th&#244;ng b&#225;o Kết quả r&#224; so&#225;t điều kiện x&#233;t tốt nghiệp hệ ch&#237;nh quy, đợt 3 năm 2022 <small class="text-muted">(25/10/2022)</small></a>
            </li>
            <li>
                <a href="/News/Content/thong-bao-v-v-to-chuc-le-be-giang-va-trao-bang-tot-nghiep-dai-hoc-nam-2022/">Th&#244;ng b&#225;o v/v tổ chức Lễ bế giảng v&#224; trao bằng tốt nghiệp đại học năm 2022 <small class="text-muted">(14/06/2022)</small></a>
            </li>
            <li>
                <a href="/News/Content/ke-hoach-dao-tao-va-thoi-khoa-bieu-du-kien-hoc-ky-3-nam-hoc-2021-2022/">Kế hoạch đ&#224;o tạo v&#224; Thời kh&#243;a biểu dự kiến học kỳ 3, năm học 2021-2022 <small class="text-muted">(27/05/2022)</small></a>
            </li>
    </ul>
</div>


            </div>
        </div>
        <!-- Right panel -->
        
        <!-- Main content -->
        <div class="panel-main-content">
            <div class="hitec-content">
                <div class="row">
                    <div class="col-xs-12">
                        <img id="ajax-loader" src="/images/icons/ajax-loader.gif" alt="" />
                 
				<h2>THAY ĐỔI MẬT KHẨU</h2>

						<form action="DoiMatKhauAdminController" class="form-horizontal" method="post" role="form"><input name="__RequestVerificationToken" type="hidden" value="r-DmTAK4mxoxw3gkjXkHp0iKTHzP5jC5uEc8noE7wVyVeaeXVaWAYqSgGlGoSIY11omExbxVklTlS05Wc84XkTFkrNeEcPxhVwkdkuHdqhUGEN5fy0KgC7Mzuq6T3kggnza_R-9GnLc06mbVcg5DItyyPdfHqkQwJcZpPrfifIxge-VlPOWN9csDc7hxo4akfSf3ZRf01acoSqK-Ntg7fLJgFj41" />    <div class="row">
						        <div class="col-xs-2 text-center ">
						            <img src="Images/logochiakhoa.jpg" />
						        </div>
						        <div class="col-xs-10">
						            <div class="row">
						                <div class="col-xs-12">
						                    <h5>Lưu ý:</h5>
						                    <ul>
						                        <li>
						                            Mật khẩu được sử dụng để đăng nhập vào hệ thống và có vai trò rất quan trọng. Hãy
						                            giữ gìn mật khẩu cẩn thận và tuyệt đối không được giao mật khẩu cho người khác.
						                        </li>
						                        <li>
						                            Sinh viên phải chịu hoàn toàn trách nhiệm nếu để lộ mật khẩu dẫn đến ảnh hưởng đến thông
						                            tin và dữ liệu của cá nhân cũng như công việc của bản thân.
						                        </li>
						                        <li>
						                            Nên đặt mật khẩu đủ dài và khó đoán. Không nên sử dụng ngày sinh, số
						                            điện thoại, cách viết tắt của họ tên,.... để làm mật khẩu.
						                        </li>
						                        <li>Để tránh các sai sót khi gõ mật khẩu, nên tắt chế độ gõ tiếng Việt trước khi thay đổi mật khẩu.</li>
						                    </ul>
						                </div>
						            </div>
						
						            <div class="row">
						                <div class="col-xs-8">
						                    <div class="form-group">
						                        <label class="control-label col-xs-5" for="OldPassword">Mật khẩu cũ:</label>
						                        <div class="col-xs-7">
						                            <input class="form-control" id="OldPassword" name="OldPassword" type="password" value="${MatKhauCu}" />
						                            <span class="field-validation-valid text-danger" data-valmsg-for="OldPassword" data-valmsg-replace="true"></span>
						                        </div>
						                    </div>
						
						                    <div class="form-group">
						                        <label class="control-label col-xs-5" for="NewPassword">Mật khẩu mới:</label>
						                        <div class="col-xs-7">
						                            <input class="form-control" id="NewPassword" name="NewPassword" type="password" value="${MatKhauMoi}"/>
						                            <span class="field-validation-valid text-danger" data-valmsg-for="NewPassword" data-valmsg-replace="true"></span>
						                        </div>
						                    </div>
						
						                    <div class="form-group">
						                        <label class="control-label col-xs-5" for="ConfirmPassword">X&#225;c nhận lại mật khẩu:</label>
						                        <div class="col-xs-7">
						                            <input class="form-control" id="ConfirmPassword" name="ConfirmPassword" type="password" />
						                            <span class="field-validation-valid text-danger" data-valmsg-for="ConfirmPassword" data-valmsg-replace="true"></span>
						                        </div>
						                    </div>
						                    <div class="form-group">
						                    		<div  class="g-recaptcha"
           					 						data-sitekey="6Lf4rogjAAAAAJJA-M8H8ur5b9wHjcZCCc7qIASx"></div>
						                    </div>
						                
													<script src="https://www.google.com/recaptcha/enterprise.js?render=6Lf4rogjAAAAAJJA-M8H8ur5b9wHjcZCCc7qIASx"></script>
						                    <div class="form-group">
						                        <div class="col-xs-offset-5 col-xs-7">
						                            <button type="submit" name="method" value="ok" class="btn btn-primary">Đổi mật khẩu</button>
						                            <a href="DoiMatKhauAdminController?ketthuc=1" class="btn btn-default">Kết thúc</a>
						                        </div>
						                    </div>
						                </div>
						            </div>
						        </div>
						    </div>
						</form>
						
						
						                    </div>
						                </div>
						            </div>
						        </div>
						    </div>
						<div class="container-fluid nopadding">
						    <footer>
						        <div class="row">
						            <div class="col-xs-12 text-center nopadding">
						                <p>Trường Đại học Khoa học – Đại học Huế</p>
						                <p>Địa chỉ: 77 Nguyễn Huệ, Thành phố Huế, Thừa Thiên Huế, Việt Nam</p>
						                <p>Điện thoại: (+84) 0234.3823290 – Fax: (+84) 0234.3824901</p>
						                <p>Website: <a href="http://www.husc.edu.vn" target="_blank">http://www.husc.edu.vn</a></p>
										<p><small>&copy; Trần Nguyên Phong, Khoa Công nghệ Thông tin</small></p>
						            </div>
						        </div>
						    </footer>
						</div>
						
						    <!-- for main popup dialog -->
						    <div id="dialogMain" class="modal fade" role="dialog"></div>
						    

</body>
</html>