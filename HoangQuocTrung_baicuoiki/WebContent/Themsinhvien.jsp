<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sinh viên - Trường Đại học Khoa học Huế - Tin tức - th&#244;ng b&#225;o</title>
   

    <link rel="shortcut icon" type="image/x-icon" href="/favicon.ico" />
    <link href="Themes/sinhvien.css" rel="stylesheet" />
    <link href="Themes/sinhvien1.css" rel="stylesheet" />
    <link href="Themes/sinhviensite.css" rel="stylesheet" />
    <link href="Themes/bootstrap-datepicker3.min.css" rel="stylesheet" />
    <script src="Script/sinhvien.js"></script> 
    
</head>
<c:if test="${daxapxep!=null}">
<script>
	alert('Xắp xếp thành công!')
</script>
</c:if>
<c:if test="${xacnhanthatbai!=null}">
<script>
	alert('Xác nhận thất bại!')
</script>
</c:if>
<body>
    <!-- header-->
<!-- Header and Menu -->
<div class="container-fluid">
    <!-- Tiêu đề website -->
    <div class="row hitec-title">
        <div class="col-xs-8 nopadding">
            <h1>TRƯỜNG ĐẠI HỌC KHOA HỌC - ĐẠI HỌC HUẾ</h1>
        </div>
        <div class="col-xs-4 text-right" style="color:#cecece; line-height:28px">
            ${date}
        </div>
    </div>

    <!-- Thanh menu chính -->
    <div class="row">
        <div class="col-xs-12 nopadding">
            <nav class="navbar navbar-default hitec-navbar">
                <div class="navbar-header">
                    <button type="button" class="pull-left navbar-toggle" data-toggle="collapse" data-target="#menuMain">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand hitec-logo" href="KiemTraDangNhapAdmin">
                        <img src="Images/Logoadmin2.jpg" alt="logo">
                    </a>
                </div>

                <!-- menu main -->
                <div class="nav collapse navbar-collapse" id="menuMain">
                   <ul class="nav navbar-nav">
                        <li class="dropdown">
                            <a href="QuanLyGiaoVienController" class="dropdown-toggle" data-toggle="dropdown">Quản Lý Giáo Viên<b class="caret"></b></a>
                        </li>

                        <li class="dropdown">
                            <a href="QuanLySinhVienController" class="dropdown-toggle" data-toggle="dropdown">Quản Lý Sinh Viên<b class="caret"></b></a>
                        </li>
                        
                         <li class="dropdown">
                            <a href="XacNhanDangKyController" class="dropdown-toggle" data-toggle="dropdown">Xác Nhận Đăng ký học phần<b class="caret"></b></a>
                        </li>
                        
                        <li class="dropdown">
                            <a href="DanhSachDiemTichLuySV" class="dropdown-toggle" data-toggle="dropdown">Kết quả học tập của sinh viên<b class="caret"></b></a>
                        </li>

                    </ul>
                </div>
            </nav>
        </div>
    </div>

    <!-- Thông tin về khóa/ngành, học kỳ tác nghiệp -->

    <div class="row" style="line-height:23px; font-size:13px;">
        <div class="col-xs-12 text-right">
            <a href="/Setting/Change" style="font-weight:bold; color:#ce2020" title="Nhấp vào đây để thay đổi ngành học và học kỳ tác nghiệp">
                <b class="glyphicon glyphicon-calendar" style="margin-left:20px;"></b> Năm học: ${year}-${year+1}
            </a>
        </div>
    </div>
</div>

    <!-- Panel left and main content-->
    <div class="container-fluid" id="wrapper">
        <!-- Left panel -->
        <div class="panel-sidebar-left">
            <div class="hitec-sidebar">

    <div class="hitec-information">
        <h5>
            Quản trị Admin
        </h5>        
        <!--
        <p>
            <img src="~/images/icons/curriculum_vitae16.png" alt="">
            <a id="link_userProfile" href="#">Sổ tay sinh viên</a>
        </p>
        -->
        <p>
            <img src="Images/logodoimatkhau.png" alt="">
            <a id="link_userPassword" href="DoiMatKhauAdminController">Đổi mật khẩu</a>
        </p>

        <p>
            <img src="Images/logoDangXuat.png" alt="">
            <a id="link_userLogout" href="ThoatAdmin" onclick="return confirm('Thoát khỏi hệ thống')">Đăng xuất</a>
        </p>
    </div>

<div class="hitec-general-function">
    <h5>CÁC CHỨC NĂNG CHUNG</h5>
    <p>
        <img src="Images/logothudientu.png" alt="">
        <a id="linkNews" href="/News">Tin tức - Thông báo <span class="notification-news"></span></a>
    </p>    
    <p>
        <img src="Images/logoemail.png" alt="">
        <a id="linkMessage" href="/Message/Inbox">Tin nhắn <span class="notification-message"></span></a>
    </p>   
	<p>
        <img src="Images/logothudientu.png" alt="">
        <a id="linkEmail" href="https://mail.google.com/a/husc.edu.vn" target="_blank">Thư điện tử</a> <img src="/images/new.gif" alt="">
    </p>	
</div>




<div id="hotNews" class="hitec-notification">
    <h5>TIN TỨC - THÔNG BÁO</h5>
    <ul>
            <li>
                <a href="/News/Content/so-tay-hoc-vu-va-thoi-khoa-bieu-du-kien-hoc-ky-2-nam-hoc-2022-2023/">Sổ tay học vụ v&#224; thời kh&#243;a biểu dự kiến học kỳ 2, năm học 2022-2023 <small class="text-muted">(06/12/2022)</small></a>
            </li>
            <li>
                <a href="/News/Content/quyet-dinh-ve-viec-cong-nhan-tot-nghiep-dai-hoc-he-chinh-quy-dot-3-nam-2022/">Quyết định về việc c&#244;ng nhận tốt nghiệp đại học hệ ch&#237;nh quy đợt 3 năm 2022 <small class="text-muted">(31/10/2022)</small></a>
            </li>
            <li>
                <a href="/News/Content/thong-bao-ket-qua-ra-soat-dieu-kien-xet-tot-nghiep-he-chinh-quy-dot-3-nam-2022/">Th&#244;ng b&#225;o Kết quả r&#224; so&#225;t điều kiện x&#233;t tốt nghiệp hệ ch&#237;nh quy, đợt 3 năm 2022 <small class="text-muted">(25/10/2022)</small></a>
            </li>
            <li>
                <a href="/News/Content/thong-bao-v-v-to-chuc-le-be-giang-va-trao-bang-tot-nghiep-dai-hoc-nam-2022/">Th&#244;ng b&#225;o v/v tổ chức Lễ bế giảng v&#224; trao bằng tốt nghiệp đại học năm 2022 <small class="text-muted">(14/06/2022)</small></a>
            </li>
            <li>
                <a href="/News/Content/ke-hoach-dao-tao-va-thoi-khoa-bieu-du-kien-hoc-ky-3-nam-hoc-2021-2022/">Kế hoạch đ&#224;o tạo v&#224; Thời kh&#243;a biểu dự kiến học kỳ 3, năm học 2021-2022 <small class="text-muted">(27/05/2022)</small></a>
            </li>
    </ul>
</div>
</div>
</div>

        <!-- Right panel -->
        
        <!-- Main content -->
        <div class="panel-main-content">
            <div class="hitec-content">
                <div class="row">
                    <div class="col-xs-12">
                        <img id="ajax-loader" src="/images/icons/ajax-loader.gif" alt="" />
                 
						<form  class="form-horizontal" action="QuanLySinhVienController" method="post">
						    <input name="__RequestVerificationToken" type="hidden" value="AhNRPVuKXk2bJAMsK-07cGGgIQ_A8uwsZCtsD-mJL7gwz7nr1TOUNzucA83ihiDnK0DB8ZTosQDOzOZ5769TZuqXmH_UwYXGdgq8OLhsLLNihL1vlWl3Yo0pgRTu3CNos133QBxrJFcRpojIdSIk5QX1-4FHOfjBqq4c5nGbvE8AYlpEhkoP6b26OdqiHGoZaM2YLUajopojAMTS3iMXwYtadAU1" />
						    <div class="alert alert-info alert-dismissable">
						        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
						        <p><b>Nhắc nhở:</b></p>
						        <p>
						            - Sinh viên khai báo đúng, đầy đủ thông tin và chịu trách nhiệm với các thông tin mà bản thân đã khai báo
						        </p>
						        <p>
						            - Những mục được đánh dấu * là bắt buộc phải khai báo
						        </p>
						    </div>
						
						    <!-- Thông tin chung -->
						    <fieldset class="hitec-fieldset" style="padding-bottom:20px; margin-bottom:20px;">
						        <legend style="margin-bottom:15px">Thông tin chung</legend>
						
						        <div class="row">
						            <div class="col-md-12">
						
						                <div class="form-group">
						                    <label class="control-label col-md-2">Mã sinh viên:</label>
						                    <div class="col-md-2">
						                        <input type="text" class="form-control"  value="${sessionScope.ten.getMasv()}" />
						                    </div>
						
						                    <label class="control-label col-md-2">Họ và tên:</label>
						                    <div class="col-md-6">
						                        <input type="text" name="ten" class="form-control"  value="${sessionScope.ten.getHodem()} ${sessionScope.ten.getTen()}" />
						                    </div>
						                </div>
						
						                <div class="form-group">
						                    <label class="control-label col-md-2">*Ngày sinh:</label>
						                    <div class="col-md-2">
						                        <input class="form-control inputmask-date" id="LyLichSinhVien_NgaySinh" name="LyLichSinhVien.NgaySinh" type="text" value="${sessionScope.ten.getNgaysinh()}" />
						                    </div>
						
						                    <label class="control-label col-md-2">*Nơi sinh:</label>
						                    <div class="col-md-2">
						                        <select class="form-control tudien-quocgia" data-related-control="#noisinh_tinhthanh" id="LyLichSinhVien_NoiSinh_QuocGia" name="LyLichSinhVien.NoiSinh_QuocGia"><option value="">------</option>
												<option selected="selected" value="Việt Nam">Việt Nam</option>
												<option value="Lào">L&#224;o</option>
												<option value="Trung Quốc">Trung Quốc</option>
												<option value="Campuchia">Campuchia</option>
												<option value="Nhật Bản">Nhật Bản</option>
												<option value="Hàn Quốc">H&#224;n Quốc</option>
												<option value="Hoa Kì">Hoa Kỳ</option>
												<option value="Pháp">Ph&#225;p</option>
												</select>
						                    </div>
						                    <div class="col-md-4">
						                        <input class="form-control inputmask-date" id="LyLichSinhVien_NgaySinh" name="LyLichSinhVien.NoiSinh" type="text" value="${sessionScope.ten.getNoisinh()}" />
						                    </div>
						                </div>
						
						                <div class="form-group">
						                    <label class="control-label col-md-2">*Giới tính:</label>
						                    <div class="col-md-2">
						                        <select class="form-control" id="LyLichSinhVien_QuocTich" name="LyLichSinhVien.QuocTich"><option value="">------</option>
												<c:if test="${sessionScope.ten.getGioitinh()==true}">
													<option selected="selected" value="001">Nam</option>
													<option value="002">Nữ</option>
												</c:if>
												<c:if test="${sessionScope.ten.getGioitinh()!=true}">
													<option  value="001">Nam</option>
													<option selected="selected" value="002">Nữ</option>
												</c:if>
												</select>
						                    </div>
												<label class="control-label col-md-2">*Mã lớp:</label>
													 <input style="margin-left: 2px"  name="LyLichSinhVien.NoiSinh" type="text" value="${sessionScope.ten.getNoisinh()}" />
							                    <div class="col-md-2">
						                    </div>
						
						                   </div>
						                   </div>
						                  
						
						    
						    <div class="row">
						        <div class="col-lg-offset-3 col-md-9" id="editProfileAlert" style="margin-top:5px;margin-bottom:5px;">
						        </div>
						        <div class="col-lg-offset-3 col-md-9" style="margin-top:5px;margin-bottom:15px;">
						            <button style="float:right;margin-left: 15px"  name="capnhat" value="1" type="submit" class="btn btn-primary"><i class="glyphicon glyphicon-floppy-disk"></i> Thêm sinh viên</button>
						            <a style="float:right;" href="QuanLySinhVienController" class="btn btn-default">Quay lại</a>
						        </div>
						    </div>
						    </div>
						    </fieldset>
						</form>

                    </div>
                </div>
            </div>
        </div>
    </div>
<div class="container-fluid nopadding">
    <footer>
        <div class="row">
            <div class="col-xs-12 text-center nopadding">
                <p>Trường Đại học Khoa học – Đại học Huế</p>
                <p>Địa chỉ: 77 Nguyễn Huệ, Thành phố Huế, Thừa Thiên Huế, Việt Nam</p>
                <p>Điện thoại: (+84) 0234.3823290 – Fax: (+84) 0234.3824901</p>
                <p>Website: <a href="http://www.husc.edu.vn" target="_blank">http://www.husc.edu.vn</a></p>
				<p><small>&copy; Hoàng Quốc Trung, Khoa Công nghệ Thông tin</small></p>
            </div>
        </div>
    </footer>
</div>

    <!-- for main popup dialog -->
    <div id="dialogMain" class="modal fade" role="dialog"></div>
      
    <script language="javascript" type="text/javascript">
        applicationUrl = "https://student.husc.edu.vn/";
        $(document).ready(function () {
            get_notifications();
            window.setInterval(get_notifications, 60000);
            $(".hitec-datepicker").datepicker({
                format: "dd/mm/yyyy",
                language: "vi",
                autoclose: true,
                todayHighlight: true,
                weekStart: 1
            });
            $('#dialogMain').on('hidden.bs.modal', function () {
                $("#dialogMain").html("");
            })
            $(".checkbox-all").change(function () {
                var status = this.checked;
                $($(this).data("checkbox-all-range")).each(function () {
                    $(this).prop("checked", status);
                });
            });            
            triggerPopupLink();
        });
    </script>    
    
    
    <script language="javasript" type="text/javascript">
        $(document).ready(function () {
            getUserProfileContent();
            getUserHistoryContent();
            getUserRelationsContent();
        });
        function getUserProfileContent() {
            getPageContent("#userProfileContent", createBaseLink("Account/UserProfileContent"));
            triggerPopupLink();
        }
        function getUserHistoryContent() {
            getPageContent("#userHistoryContent", createBaseLink("Account/History/List"));
            triggerPopupLink();
        }
        function getUserRelationsContent() {
            getPageContent("#userRelationsContent", createBaseLink("Account/Relations/List"));
            triggerPopupLink();
        }
    </script>

</body>
</html>