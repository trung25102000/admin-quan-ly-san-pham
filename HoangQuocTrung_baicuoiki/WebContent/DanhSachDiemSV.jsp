<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sinh viên - Trường Đại học Khoa học Huế - Tin tức - th&#244;ng b&#225;o</title>
   

    <link rel="shortcut icon" type="image/x-icon" href="/favicon.ico" />
    <link href="Themes/sinhvien.css" rel="stylesheet" />
    <link href="Themes/sinhvien1.css" rel="stylesheet" />
    <link href="Themes/sinhviensite.css" rel="stylesheet" />
    <link href="Themes/bootstrap-datepicker3.min.css" rel="stylesheet" />
    <script src="Script/sinhvien.js"></script> 
    
</head>
<c:if test="${daxapxep!=null}">
<script>
	alert('Xắp xếp thành công!')
</script>
</c:if>
<c:if test="${xacnhanthatbai!=null}">
<script>
	alert('Xác nhận thất bại!')
</script>
</c:if>
<body>
    <!-- header-->
<!-- Header and Menu -->
<div class="container-fluid">
    <!-- Tiêu đề website -->
    <div class="row hitec-title">
        <div class="col-xs-8 nopadding">
            <h1>TRƯỜNG ĐẠI HỌC KHOA HỌC - ĐẠI HỌC HUẾ</h1>
        </div>
        <div class="col-xs-4 text-right" style="color:#cecece; line-height:28px">
            ${date}
        </div>
    </div>

    <!-- Thanh menu chính -->
    <div class="row">
        <div class="col-xs-12 nopadding">
            <nav class="navbar navbar-default hitec-navbar">
                <div class="navbar-header">
                    <button type="button" class="pull-left navbar-toggle" data-toggle="collapse" data-target="#menuMain">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand hitec-logo" href="KiemTraDangNhapAdmin">
                        <img src="Images/Logoadmin2.jpg" alt="logo">
                    </a>
                </div>

                <!-- menu main -->
                <div class="nav collapse navbar-collapse" id="menuMain">
                   <ul class="nav navbar-nav">
                        <li class="dropdown">
                            <a href="QuanLyGiaoVienController" class="dropdown-toggle" data-toggle="dropdown">Quản Lý Giáo Viên<b class="caret"></b></a>
                        </li>

                        <li class="dropdown">
                            <a href="QuanLySinhVienController" class="dropdown-toggle" data-toggle="dropdown">Quản Lý Sinh Viên<b class="caret"></b></a>
                        </li>
                        
                         <li class="dropdown">
                            <a href="XacNhanDangKyController" class="dropdown-toggle" data-toggle="dropdown">Xác Nhận Đăng ký học phần<b class="caret"></b></a>
                        </li>
                        
                        <li class="dropdown">
                            <a href="DanhSachDiemTichLuySV" class="dropdown-toggle" data-toggle="dropdown">Kết quả học tập của sinh viên<b class="caret"></b></a>
                        </li>

                    </ul>
                </div>
            </nav>
        </div>
    </div>

    <!-- Thông tin về khóa/ngành, học kỳ tác nghiệp -->

    <div class="row" style="line-height:23px; font-size:13px;">
        <div class="col-xs-12 text-right">
            <a href="/Setting/Change" style="font-weight:bold; color:#ce2020" title="Nhấp vào đây để thay đổi ngành học và học kỳ tác nghiệp">
                <b class="glyphicon glyphicon-calendar" style="margin-left:20px;"></b> Năm học: ${year}-${year+1}
            </a>
        </div>
    </div>
</div>

    <!-- Panel left and main content-->
    <div class="container-fluid" id="wrapper">
        <!-- Left panel -->
        <div class="panel-sidebar-left">
            <div class="hitec-sidebar">

    <div class="hitec-information">
        <h5>
            Quản trị Admin
        </h5>        
        <!--
        <p>
            <img src="~/images/icons/curriculum_vitae16.png" alt="">
            <a id="link_userProfile" href="#">Sổ tay sinh viên</a>
        </p>
        -->
        <p>
            <img src="Images/logodoimatkhau.png" alt="">
            <a id="link_userPassword" href="DoiMatKhauAdminController">Đổi mật khẩu</a>
        </p>

        <p>
            <img src="Images/logoDangXuat.png" alt="">
            <a id="link_userLogout" href="ThoatAdmin" onclick="return confirm('Thoát khỏi hệ thống')">Đăng xuất</a>
        </p>
    </div>

<div class="hitec-general-function">
    <h5>CÁC CHỨC NĂNG CHUNG</h5>
    <p>
        <img src="Images/logothudientu.png" alt="">
        <a id="linkNews" href="/News">Tin tức - Thông báo <span class="notification-news"></span></a>
    </p>    
    <p>
        <img src="Images/logoemail.png" alt="">
        <a id="linkMessage" href="/Message/Inbox">Tin nhắn <span class="notification-message"></span></a>
    </p>   
	<p>
        <img src="Images/logothudientu.png" alt="">
        <a id="linkEmail" href="https://mail.google.com/a/husc.edu.vn" target="_blank">Thư điện tử</a> <img src="/images/new.gif" alt="">
    </p>	
</div>





<div id="hotNews" class="hitec-notification">
    <h5>TIN TỨC - THÔNG BÁO</h5>
    <ul>
            <li>
                <a href="/News/Content/so-tay-hoc-vu-va-thoi-khoa-bieu-du-kien-hoc-ky-2-nam-hoc-2022-2023/">Sổ tay học vụ v&#224; thời kh&#243;a biểu dự kiến học kỳ 2, năm học 2022-2023 <small class="text-muted">(06/12/2022)</small></a>
            </li>
            <li>
                <a href="/News/Content/quyet-dinh-ve-viec-cong-nhan-tot-nghiep-dai-hoc-he-chinh-quy-dot-3-nam-2022/">Quyết định về việc c&#244;ng nhận tốt nghiệp đại học hệ ch&#237;nh quy đợt 3 năm 2022 <small class="text-muted">(31/10/2022)</small></a>
            </li>
            <li>
                <a href="/News/Content/thong-bao-ket-qua-ra-soat-dieu-kien-xet-tot-nghiep-he-chinh-quy-dot-3-nam-2022/">Th&#244;ng b&#225;o Kết quả r&#224; so&#225;t điều kiện x&#233;t tốt nghiệp hệ ch&#237;nh quy, đợt 3 năm 2022 <small class="text-muted">(25/10/2022)</small></a>
            </li>
            <li>
                <a href="/News/Content/thong-bao-v-v-to-chuc-le-be-giang-va-trao-bang-tot-nghiep-dai-hoc-nam-2022/">Th&#244;ng b&#225;o v/v tổ chức Lễ bế giảng v&#224; trao bằng tốt nghiệp đại học năm 2022 <small class="text-muted">(14/06/2022)</small></a>
            </li>
            <li>
                <a href="/News/Content/ke-hoach-dao-tao-va-thoi-khoa-bieu-du-kien-hoc-ky-3-nam-hoc-2021-2022/">Kế hoạch đ&#224;o tạo v&#224; Thời kh&#243;a biểu dự kiến học kỳ 3, năm học 2021-2022 <small class="text-muted">(27/05/2022)</small></a>
            </li>
    </ul>
</div>


            </div>
        </div>
        <!-- Right panel -->
        
        <!-- Main content -->
        <div class="panel-main-content">
            <div class="hitec-content">
                <div class="row">
                    <div class="col-xs-12">
                        <img id="ajax-loader" src="/images/icons/ajax-loader.gif" alt="" />
                        

<h2>DANH SÁCH LỚP HỌC PHẦN ĐÃ ĐĂNG KÝ HỌC CHỜ XÉT DUYỆT</h2>

<div class="container-fluid text-right" style="margin-bottom:5px;">
    <div class="btn-group">        
        <form action="DanhSachDiemTichLuySV">
        	<input type="text" placeholder="Nhập Mã sinh viên cần tìm..." name="Masv">
        	<input type="submit" class="btn btn-primary"> 
        </form>
    </div>
</div>
<table class="table table-hover table-striped">
    <thead>
        <tr>
            <th style="width:30px" class="text-center">STT</th>
            <th style="width:30px" class="text-center">Mã Sinh Viên</th>
            <th style="width:60px" class="text-center">Tên Sinh Viên</th>
            <th style="width:80px" class="text-center">Ngày Sinh</th>
            <th style="width:150px" class="text-center">Giới tính</th>
            <th style="width:80px">Lớp Học</th>            
            <th style="width:100px" class="text-center">Khóa Học</th>            
            <th style="width:80px" class="text-center">Điểm GPA</th>
            <th style="width:25px">Kết Quả</th>
        </tr>
    </thead>
    <tbody>
            <tr>
                <td colspan="9" class="table-cell-title">
                    <strong>Danh sách điểm trung bình tích lũy của sinh viên</strong>
                </td>
            </tr>
            <c:set var="i" value="${1}"></c:set>
            <c:forEach var="cho" items="${dscho}">
			                <tr>
			                	<c:if test="${i<=7}">
			                		 <td class="text-center">${i}</td>
				                  	<c:set var="i" value="${i+1}"></c:set>
				                  	 </c:if>
				                  	 <c:if test="${i>7}">
				                  	<c:set var="i" value="${1}"></c:set>
				                  	<td class="text-center">${i}</td>
				                  	</c:if>
			                    <td>
			                        <a href="/Course/Details/2022-2023.1.TIN4013.005/">
			                            ${cho.getMasv()}<br />
			                        </a>
			                     </td>
			                     <td>
			              			 ${cho.getHoTenSV()}
			                     </td>
			                   <td class="text-center">
			                        ${cho.getNgaysinh()}
			                    </td>
			                    <c:if test="${cho.getGioitinh()==true}">
				                    <td class="text-center">
				                        Nam
				                    </td>
			                    </c:if>
			                    <c:if test="${cho.getGioitinh()==false}">
				                    <td class="text-center">
				                        Nữ
				                    </td>
			                    </c:if>
			                    <td>
			                        ${cho.getTenlop()}
			                    </td>
			                    <td  class="text-center">
			                        Khóa ${cho.getKhoa()}
			                    </td>                    
			                    <td class="text-center">
			                        ${cho.getDiemGPA()}
			                    </td>
			                      <td class="text-center" style="color: red">
			                    	 ${cho.getHocLuc()}
			                    </td>                   
			                </tr>
			                </c:forEach>
    </tbody>
</table>
<div class="container-fluid" style="border-top:1px solid #ccc; padding-top:10px;">    
    <div class="row">
        <div class="col-xs-6">- Số sinh viên: <b>${dscho.size()}</b></div>
        <form action="DanhSachDiemTichLuySV">
        	<div style="float: right" ><button class="btn btn-primary" name="xacnhantatca" value="1">Xắp xếp theo Điểm GPA</button></div>
        </form>
    </div>    
    <div class="row">
        <div class="col-xs-12">
            <br /><b><i>Lưu ý:</i></b> 
            <ul style="font-style:italic">
                <li>Số tiền học phí tạm tính ở thời điểm hiện tại, có thể thay đổi khi có qui định mức thu học phí chính thức của Nhà trường.</li>
                <li>Sinh viên chỉ được phép hủy đăng ký những lớp học phần chưa được duyệt và do sinh viên tự đăng ký</li>
            </ul>
            
        </div>
    </div>
</div>

                    </div>
                </div>
            </div>
        </div>
    </div>

</body>
</html>