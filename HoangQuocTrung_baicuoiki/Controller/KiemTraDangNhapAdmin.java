

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import bean.DangNhap_bean;
import bean.SinhVien_bean;
import bo.DangNhap_bo;

/**
 * Servlet implementation class KiemTraDangNhapAdmin
 */
@WebServlet("/KiemTraDangNhapAdmin")
public class KiemTraDangNhapAdmin extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public KiemTraDangNhapAdmin() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String TenDangNhap=request.getParameter("loginID");
		String MatKhau=request.getParameter("password");
		DangNhap_bo Dn_bo=new DangNhap_bo();
		HttpSession sesion=request.getSession();
		DangNhap_bean dn=(DangNhap_bean)sesion.getAttribute("admin");
		if(dn==null)
		{
			if(TenDangNhap!=null && MatKhau!=null)
			{
				DangNhap_bean Dn_bean=null;
				// kIem tra
				try {
					Dn_bean=Dn_bo.KiemTraDangNhap(TenDangNhap, MatKhau);
				} catch (Exception e) {
					// TODO: handle exception
				}
				
				if(Dn_bean!=null)
				{
					HttpSession session=request.getSession();
					session.setAttribute("admin",Dn_bean );
					Date date=java.util.Calendar.getInstance().getTime(); 
					Calendar instance = Calendar.getInstance();
			        int year = instance.get(Calendar.YEAR);
			        request.setAttribute("year", year);
					request.setAttribute("date", date);
					request.getRequestDispatcher("Admin.jsp").forward(request, response);
				}
				else
				{
					request.setAttribute("DangNhapSai", 1);
					request.getRequestDispatcher("DangNhapAdmin.jsp").forward(request, response);
				}
			}
			else {
				//kiểm tra lần đầu
				request.getRequestDispatcher("DangNhapAdmin.jsp").forward(request, response);
			}
		}
		else
		{
			Calendar instance = Calendar.getInstance();
			int year = instance.get(Calendar.YEAR);
			request.setAttribute("year", year);
			Date date=java.util.Calendar.getInstance().getTime(); 
			request.setAttribute("date", date);
			request.getRequestDispatcher("Admin.jsp").forward(request, response);
		}
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
