

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import bean.SinhVien_bean;
import bo.Khoa_bo;
import bo.Lop_bo;
import bo.MP5;
import bo.SinhVien_bo;
import bo.VerifyRecaptcha;

/**
 * Servlet implementation class DoiMatKhauController
 */
@WebServlet("/DoiMatKhauController")
public class DoiMatKhauController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DoiMatKhauController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		response.setCharacterEncoding("utf-8");
		HttpSession session=request.getSession();
		MP5 mp5=new MP5();
		String MatKhauCust=request.getParameter("OldPassword");
		String MatKhauCu=null;
		try {
			MatKhauCu=mp5.ecrypt(MatKhauCust);
		} catch (Exception e) {
			// TODO: handle exception
		}
		String MatKhauMoi=request.getParameter("NewPassword");
		String XacNhanMatKhau=request.getParameter("ConfirmPassword");
		String DoiMatKhau=request.getParameter("method");
		
		SinhVien_bo sv_bo=new SinhVien_bo();
		SinhVien_bean sv=(SinhVien_bean) session.getAttribute("ten");
		Lop_bo l_bo=new Lop_bo();
		Khoa_bo k_bo=new Khoa_bo();
			Date date=java.util.Calendar.getInstance().getTime(); 
			Calendar instance = Calendar.getInstance();
	        int year = instance.get(Calendar.YEAR);
	        request.setAttribute("year", year);
			request.setAttribute("lop",l_bo.Getlopbymalop(sv.getMalop()));
			request.setAttribute("date", date);
			request.setAttribute("khoa", k_bo.GetKhoabyMaKhoa(l_bo.Getlopbymalop(sv.getMalop()).getMakhoa()));
			String gRecaptchaResponse = request.getParameter("g-recaptcha-response");
			boolean verify = VerifyRecaptcha.verify(gRecaptchaResponse);
		String ketthuc=request.getParameter("ketthuc");
		if(ketthuc!=null)
		{
			request.getRequestDispatcher("SinhVien.jsp").forward(request, response);
			return;
		}
		if(DoiMatKhau!=null)
		{
			if(MatKhauCu!=null && MatKhauMoi!=null && XacNhanMatKhau!=null)
			{
				if(MatKhauCu.equals(sv.getMatkhau()))
				{
					if(MatKhauMoi.equals(XacNhanMatKhau))
					{
						int a=sv_bo.DoiMatKhau(sv.getMasv(), MatKhauMoi);
						if(a>0)
						{
							request.setAttribute("doithanhcong", 1);
							request.getRequestDispatcher("SinhVien.jsp").forward(request, response);
							return;
						}
						else request.setAttribute("thatbai", 1);
					}
					else
					{
						request.setAttribute("xacnhansai", 1);
						request.setAttribute("MatKhauCu", MatKhauCu);
						request.setAttribute("MatKhauMoi", MatKhauMoi);
					}
				}
				else
					{
						request.setAttribute("Matkhausai", 1);
					}
			}
			else request.setAttribute("chuanhaphet", 1);
		}
		
			request.getRequestDispatcher("DoiMatKhau.jsp").forward(request, response);
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
