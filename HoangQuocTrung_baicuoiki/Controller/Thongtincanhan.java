

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import bean.SinhVien_bean;
import bo.Khoa_bo;
import bo.Lop_bo;
import bo.SinhVien_bo;

/**
 * Servlet implementation class Thongtincanhan
 */
@WebServlet("/Thongtincanhan")
public class Thongtincanhan extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Thongtincanhan() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session=request.getSession();
		SinhVien_bean sv_bean=(SinhVien_bean) session.getAttribute("ten");
		SinhVien_bo sv_bo=new SinhVien_bo();
		Lop_bo l_bo=new Lop_bo();
		Khoa_bo k_bo=new Khoa_bo();

		if(sv_bean!=null)
		{
			Date date=java.util.Calendar.getInstance().getTime(); 
			Calendar instance = Calendar.getInstance();
	        int year = instance.get(Calendar.YEAR);
	        request.setAttribute("year", year);
			request.setAttribute("lop",l_bo.Getlopbymalop(sv_bean.getMalop()));
			request.setAttribute("date", date);
			request.setAttribute("sv", sv_bo.GetSinhvienbyID(sv_bean.getMasv()));
			System.out.println(sv_bo.GetSinhvienbyID(sv_bean.getMasv()).getAnh());
			request.setAttribute("khoa", k_bo.GetKhoabyMaKhoa(l_bo.Getlopbymalop(sv_bean.getMalop()).getMakhoa()));
			request.getRequestDispatcher("LyLichSinhVien.jsp").forward(request, response);
		}
		
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
