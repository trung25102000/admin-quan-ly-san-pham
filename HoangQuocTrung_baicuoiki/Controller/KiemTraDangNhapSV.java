

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.websocket.Session;

import bean.SinhVien_bean;
import bo.Khoa_bo;
import bo.Lop_bo;
import bo.SinhVien_bo;

/**
 * Servlet implementation class KiemTraDangNhapSV
 */
@WebServlet("/KiemTraDangNhapSV")
public class KiemTraDangNhapSV extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public KiemTraDangNhapSV() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String TenDangNhap=request.getParameter("loginID");
		String MatKhau=request.getParameter("password");
		SinhVien_bo sv_bo=new SinhVien_bo();
		Lop_bo l_bo=new Lop_bo();
		Khoa_bo k_bo=new Khoa_bo();
		HttpSession sesion=request.getSession();
		SinhVien_bean sv_bean=(SinhVien_bean)sesion.getAttribute("ten");
		if(sv_bean==null)
		{
			if(TenDangNhap!=null && MatKhau!=null)
			{
				SinhVien_bean sv=null;
				// kIem tra
				try {
					sv=sv_bo.KiemTraDangNhap(TenDangNhap, MatKhau);
				} catch (Exception e) {
					// TODO: handle exception
				}
				
				if(sv!=null)
				{
					HttpSession session=request.getSession();
					session.setAttribute("ten",sv );
					Date date=java.util.Calendar.getInstance().getTime(); 
					Calendar instance = Calendar.getInstance();
			        int year = instance.get(Calendar.YEAR);
			        request.setAttribute("year", year);
					request.setAttribute("lop",l_bo.Getlopbymalop(sv.getMalop()));
					request.setAttribute("date", date);
					request.setAttribute("khoa", k_bo.GetKhoabyMaKhoa(l_bo.Getlopbymalop(sv.getMalop()).getMakhoa()));
					request.getRequestDispatcher("SinhVien.jsp").forward(request, response);
				}
				else
				{
					request.setAttribute("DangNhapSai", 1);
					request.getRequestDispatcher("DangNhapSV.jsp").forward(request, response);
				}
			}
			else {
				//kiểm tra lần đầu
				request.getRequestDispatcher("DangNhapSV.jsp").forward(request, response);
			}
		}
		else
		{
			Calendar instance = Calendar.getInstance();
			int year = instance.get(Calendar.YEAR);
			request.setAttribute("year", year);
			request.setAttribute("lop",l_bo.Getlopbymalop(sv_bean.getMalop()));
			request.setAttribute("khoa", k_bo.GetKhoabyMaKhoa(l_bo.Getlopbymalop(sv_bean.getMalop()).getMakhoa()));
			Date date=java.util.Calendar.getInstance().getTime(); 
			request.setAttribute("date", date);
			request.getRequestDispatcher("SinhVien.jsp").forward(request, response);
		}
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
