

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import bean.LopHocPhan_bean;
import bean.SinhVien_bean;
import bean.ThamGiaHoc_bean;
import bo.Khoa_bo;
import bo.LopHocPhan_bo;
import bo.Lop_bo;
import bo.Monhoc_bo;
import bo.SinhVien_bo;
import bo.ThamGiaHoc_bo;

/**
 * Servlet implementation class KetQuaHocTapController
 */
@WebServlet("/KetQuaHocTapController")
public class KetQuaHocTapController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public KetQuaHocTapController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		response.setCharacterEncoding("utf-8");
		HttpSession session=request.getSession();
		SinhVien_bo sv_bo=new SinhVien_bo();
		SinhVien_bean sv=(SinhVien_bean) session.getAttribute("ten");
		Lop_bo l_bo=new Lop_bo();
		Khoa_bo k_bo=new Khoa_bo();
		Monhoc_bo Mhoc_bo=new Monhoc_bo();
		LopHocPhan_bo l=new LopHocPhan_bo();
		ThamGiaHoc_bo TGhoc_bo=new ThamGiaHoc_bo();
		String Mamon=request.getParameter("MaMon");
		request.setAttribute("DsMonHoc", Mhoc_bo.Getmonhocs());
			Date date=java.util.Calendar.getInstance().getTime(); 
			Calendar instance = Calendar.getInstance();
	        int year = instance.get(Calendar.YEAR);
	        request.setAttribute("year", year);
			request.setAttribute("lop",l_bo.Getlopbymalop(sv.getMalop()));
			request.setAttribute("date", date);
			request.setAttribute("khoa", k_bo.GetKhoabyMaKhoa(l_bo.Getlopbymalop(sv.getMalop()).getMakhoa()));
			ThamGiaHoc_bean TGhoc_bean=TGhoc_bo.KetQua(sv.getMasv(), Mamon);
			if(TGhoc_bean!=null)
			{
				request.setAttribute("thamgiahoc", TGhoc_bean);
				request.setAttribute("TenHocPhan", l.TenHocPhan(TGhoc_bean.getMaLophocphan()));
				request.setAttribute("DiemTB", (TGhoc_bean.getDiemChuyenCan()+TGhoc_bean.getDiemKiemTra()+TGhoc_bean.getDiemThi())/3);
				request.setAttribute("KetQua",TGhoc_bo.Ketqua((TGhoc_bean.getDiemChuyenCan()+TGhoc_bean.getDiemKiemTra()+TGhoc_bean.getDiemThi())/3));
				request.setAttribute("sotinchi", Mhoc_bo.Sotinchi(l.MaMon(TGhoc_bean.getMaLophocphan())));
				request.setAttribute("SoluongSV", l.SoLuongSVToiDa(TGhoc_bean.getMaLophocphan()));
			}
			else {
				response.sendRedirect("DangKiController?MaMon="+Mamon);
				return;
			}
			request.getRequestDispatcher("KetQuaHoctap.jsp").forward(request, response);
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
