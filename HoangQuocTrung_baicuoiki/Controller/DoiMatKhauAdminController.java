

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import bean.DangNhap_bean;
import bo.DangNhap_bo;
import bo.Khoa_bo;
import bo.Lop_bo;
import bo.MP5;
import bo.VerifyRecaptcha;

/**
 * Servlet implementation class DoiMatKhauAdminController
 */
@WebServlet("/DoiMatKhauAdminController")
public class DoiMatKhauAdminController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DoiMatKhauAdminController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		response.setCharacterEncoding("utf-8");
		HttpSession session=request.getSession();
		MP5 mp5=new MP5();
		String MatKhauCu=request.getParameter("OldPassword");
		String MatKhauMoi=request.getParameter("NewPassword");
		String XacNhanMatKhau=request.getParameter("ConfirmPassword");
		System.out.println(MatKhauCu+","+MatKhauMoi+","+XacNhanMatKhau);
		String DoiMatKhau=request.getParameter("method");
		DangNhap_bo Dn_bo=new DangNhap_bo();
		HttpSession sesion=request.getSession();
		DangNhap_bean dn=(DangNhap_bean)sesion.getAttribute("admin");
		Lop_bo l_bo=new Lop_bo();
		Khoa_bo k_bo=new Khoa_bo();
			Date date=java.util.Calendar.getInstance().getTime(); 
			Calendar instance = Calendar.getInstance();
	        int year = instance.get(Calendar.YEAR);
	        request.setAttribute("year", year);
			request.setAttribute("date", date);
			String gRecaptchaResponse = request.getParameter("g-recaptcha-response");
			boolean verify = VerifyRecaptcha.verify(gRecaptchaResponse);
		String ketthuc=request.getParameter("ketthuc");
		if(ketthuc!=null)
		{
			request.getRequestDispatcher("Admin.jsp").forward(request, response);
			return;
		}
		if(DoiMatKhau!=null)
		{
			System.out.println("alo");
			if(MatKhauCu!=null && MatKhauMoi!=null && XacNhanMatKhau!=null)
			{
				if(MatKhauCu.equals(dn.getMatKhau()))
				{
					System.out.println("5alo");
					if(MatKhauMoi.equals(XacNhanMatKhau))
					{
						System.out.println("4alo");
						int a=Dn_bo.DoiMatKhau(dn.getTenDangNhap(), MatKhauMoi);
						if(a>0)
						{
							System.out.println("3alo");
							request.setAttribute("doithanhcong", 1);
							request.getRequestDispatcher("Admin.jsp").forward(request, response);
							return;
						}
						else request.setAttribute("thatbai", 1);
					}
					else
					{
						System.out.println("2alo");
						request.setAttribute("xacnhansai", 1);
						request.setAttribute("MatKhauCu", MatKhauCu);
						request.setAttribute("MatKhauMoi", MatKhauMoi);
					}
				}
				else
					{
						System.out.println("1alo");
						request.setAttribute("Matkhausai", 1);
					}
			}
			else
				{
				System.out.println("0alo");
					request.setAttribute("chuanhaphet",1);
				}
		}
			request.getRequestDispatcher("DoiMatKhauAdmin.jsp").forward(request, response);
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
