

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import bean.MonHoc_bean;
import bean.SinhVien_bean;
import bo.GiangVien_bo;
import bo.Khoa_bo;
import bo.LopHocPhan_bo;
import bo.Lop_bo;
import bo.Monhoc_bo;
import bo.SinhVien_bo;
import bo.ThamGiaHoc_bo;

/**
 * Servlet implementation class DangKiController
 */
@WebServlet("/DangKiController")
public class DangKiController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DangKiController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		response.setCharacterEncoding("utf-8");
		HttpSession session=request.getSession();
		SinhVien_bo sv_bo=new SinhVien_bo();
		SinhVien_bean sv=(SinhVien_bean) session.getAttribute("ten");
		GiangVien_bo g=new GiangVien_bo();
		Lop_bo l_bo=new Lop_bo();
		Khoa_bo k_bo=new Khoa_bo();
		Monhoc_bo Mhoc_bo=new Monhoc_bo();
		LopHocPhan_bo l=new LopHocPhan_bo();
		ThamGiaHoc_bo TGhoc_bo=new ThamGiaHoc_bo();
		String Mamon=request.getParameter("MaMon");
		String Malop=request.getParameter("MaLop");
		MonHoc_bean m=Mhoc_bo.Getmon(Mamon);
		request.setAttribute("mon", m);
		request.setAttribute("DsMonHoc", Mhoc_bo.Getmonhocs());
			Date date=java.util.Calendar.getInstance().getTime(); 
			Calendar instance = Calendar.getInstance();
	        int year = instance.get(Calendar.YEAR);
	        request.setAttribute("year", year);
			request.setAttribute("lop",l_bo.Getlopbymalop(sv.getMalop()));
			request.setAttribute("date", date);
			request.setAttribute("khoa", k_bo.GetKhoabyMaKhoa(l_bo.Getlopbymalop(sv.getMalop()).getMakhoa()));
			request.setAttribute("dslop", l.GetLophocphanbyMamon(Mamon));
			request.setAttribute("SoluongSV",l.SoLuongSVToiDaTheoMon(Mamon));
			request.setAttribute("MaMon", Mamon);
			request.setAttribute("gv_bo",g);
			if(Malop!=null)
			{
				int a=TGhoc_bo.ThemThamGiaHoc(Malop, sv.getMasv());
				if(a>0) response.sendRedirect("DangKiHocTapController");
				return;
			}
			request.getRequestDispatcher("DangKi.jsp").forward(request, response);
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
