

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import bean.MonHoc_bean;
import bean.SinhVien_bean;
import bo.ChoDangKi_bo;
import bo.GiangVien_bo;
import bo.KetQua_bo;
import bo.Khoa_bo;
import bo.LopHocPhan_bo;
import bo.Lop_bo;
import bo.Monhoc_bo;
import bo.SinhVien_bo;
import bo.ThamGiaHoc_bo;

/**
 * Servlet implementation class XacNhanDangKyController
 */
@WebServlet("/XacNhanDangKyController")
public class XacNhanDangKyController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public XacNhanDangKyController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		response.setCharacterEncoding("utf-8");
		HttpSession session=request.getSession();
		SinhVien_bo sv_bo=new SinhVien_bo();
		GiangVien_bo g=new GiangVien_bo();
		Lop_bo l_bo=new Lop_bo();
		Khoa_bo k_bo=new Khoa_bo();
		Monhoc_bo Mhoc_bo=new Monhoc_bo();
		LopHocPhan_bo l=new LopHocPhan_bo();
		ThamGiaHoc_bo TGhoc_bo=new ThamGiaHoc_bo();
		ChoDangKi_bo ChoDk_bo=new ChoDangKi_bo();
		KetQua_bo Kq_bo=new KetQua_bo();
		String Malop=request.getParameter("Malop");
		String xacnhan=request.getParameter("xacnhan");
		String xacnhantatca=request.getParameter("xacnhantatca");
		String Masv=request.getParameter("Masv");
		if(xacnhan!=null)
		{
			int a=TGhoc_bo.XacNhanDangKi(Malop, Masv);
			if(a>0) request.setAttribute("xacnhanxong", 1);
			else request.setAttribute("xacnhanthatbai", 1);
		}
		if(xacnhantatca!=null)
		{
			int a=TGhoc_bo.XacNhanDangKi();
			if(a==0 ) request.setAttribute("xacnhanthatbai", 1);
			else
			{
				request.setAttribute("xacnhanxong", 1);
				response.sendRedirect("KiemTraDangNhapAdmin");
				return;
			}
		}
		request.setAttribute("dscho", Kq_bo.Getketquadaduyet());
		String Mamon=request.getParameter("MaMon");
		MonHoc_bean m=Mhoc_bo.Getmon(Mamon);
		request.setAttribute("mon", m);
		request.setAttribute("DsMonHoc", Mhoc_bo.Getmonhocs());
			Date date=java.util.Calendar.getInstance().getTime(); 
			Calendar instance = Calendar.getInstance();
	        int year = instance.get(Calendar.YEAR);
	        request.setAttribute("year", year);
			request.setAttribute("date", date);
			request.setAttribute("solop", ChoDk_bo.Solop());
			request.setAttribute("tongtin", ChoDk_bo.Tongtinchi());
			request.getRequestDispatcher("XacNhanDangKiLop.jsp").forward(request, response);
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
