

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import bean.MonHoc_bean;
import bean.SinhVien_bean;
import bo.ChoDangKi_bo;
import bo.GiangVien_bo;
import bo.KetQua_bo;
import bo.Khoa_bo;
import bo.LopHocPhan_bo;
import bo.Lop_bo;
import bo.Monhoc_bo;
import bo.SinhVien_bo;
import bo.ThamGiaHoc_bo;

/**
 * Servlet implementation class LopHocPhanDuocDuyetController
 */
@WebServlet("/LopHocPhanDuocDuyetController")
public class LopHocPhanDuocDuyetController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LopHocPhanDuocDuyetController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		response.setCharacterEncoding("utf-8");
		HttpSession session=request.getSession();
		SinhVien_bo sv_bo=new SinhVien_bo();
		SinhVien_bean sv=(SinhVien_bean) session.getAttribute("ten");
		GiangVien_bo g=new GiangVien_bo();
		Lop_bo l_bo=new Lop_bo();
		Khoa_bo k_bo=new Khoa_bo();
		Monhoc_bo Mhoc_bo=new Monhoc_bo();
		LopHocPhan_bo l=new LopHocPhan_bo();
		ThamGiaHoc_bo TGhoc_bo=new ThamGiaHoc_bo();
		ChoDangKi_bo ChoDk_bo=new ChoDangKi_bo();
		KetQua_bo Kq_bo=new KetQua_bo();
		//System.out.println(Kq_bo.Getketquadaduyet(sv.getMasv()));
		request.setAttribute("dsduyet", Kq_bo.Getketquadaduyet(sv.getMasv()));
		String Mamon=request.getParameter("MaMon");
		MonHoc_bean m=Mhoc_bo.Getmon(Mamon);
		request.setAttribute("mon", m);
		request.setAttribute("DsMonHoc", Mhoc_bo.Getmonhocs());
			Date date=java.util.Calendar.getInstance().getTime(); 
			Calendar instance = Calendar.getInstance();
	        int year = instance.get(Calendar.YEAR);
	        request.setAttribute("year", year);
			request.setAttribute("lop",l_bo.Getlopbymalop(sv.getMalop()));
			request.setAttribute("date", date);
			request.setAttribute("khoa", k_bo.GetKhoabyMaKhoa(l_bo.Getlopbymalop(sv.getMalop()).getMakhoa()));
			request.setAttribute("solop", ChoDk_bo.Solop()+Kq_bo.Getketquadaduyet(sv.getMasv()).size());
			request.setAttribute("tongtin", Kq_bo.Sotinduocduyet(sv.getMasv()));
			request.getRequestDispatcher("HocPhanDuocDuyet.jsp").forward(request, response);
		response.getWriter().append("Served at: ").append(request.getContextPath());
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
