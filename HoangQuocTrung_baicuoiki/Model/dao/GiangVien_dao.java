package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import bean.GiangVien_bean;
import bean.LopHocPhan_bean;

public class GiangVien_dao {
	public ArrayList<GiangVien_bean> getGiangViens()
	{
		ArrayList<GiangVien_bean> ds_lop=new ArrayList<GiangVien_bean>();
		cosodulieu1 cs=new cosodulieu1();
		cs.ketnoi1();
		String Lenhsql="Select *from GiangVien";
		try {
			PreparedStatement cmd=cs.cn.prepareStatement(Lenhsql);
			ResultSet rs=cmd.executeQuery();
			while(rs.next())
			{
				String maGiangVien=rs.getString(1);
				String makhoaString=rs.getString(2);
				String tenGiangVien=rs.getString(3);
				String queQuan =rs.getString(4);
				String matKhau =rs.getString(5);
				String ngaySinh=rs.getString(6);
				GiangVien_bean g=new GiangVien_bean(maGiangVien, makhoaString, tenGiangVien, queQuan, matKhau, ngaySinh);
				ds_lop.add(g);
			}
			rs.close();
			cs.cn.close();
			return ds_lop;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
		
	}
	public static void main(String[] args) {
		GiangVien_dao g=new GiangVien_dao();
		System.out.println(g.getGiangViens());
	}
}
