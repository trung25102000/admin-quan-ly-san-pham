package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import bean.Lop_bean;

public class Lop_dao {
	public ArrayList<Lop_bean> getlops()
	{
		ArrayList<Lop_bean> ds_lop=new ArrayList<Lop_bean>();
		cosodulieu1 cs=new cosodulieu1();
		cs.ketnoi1();
		String Lenhsql="Select *from lop";
		try {
			PreparedStatement cmd=cs.cn.prepareStatement(Lenhsql);
			ResultSet rs=cmd.executeQuery();
			while(rs.next())
			{
				String malop=rs.getString(1);
				String tenlop=rs.getString(2);
				int khoa=rs.getInt(3);
				String hedaotao=rs.getString(4);
				int namnhaphoc=rs.getInt(5);
				int siso=rs.getInt(6);
				String makhoa=rs.getString(7);
				Lop_bean l=new Lop_bean(malop, tenlop, khoa, hedaotao, namnhaphoc, siso, makhoa);
				ds_lop.add(l);
			}
			rs.close();
			cs.cn.close();
			return ds_lop;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
	public static void main(String[] args) {
		Lop_dao l=new Lop_dao();
		System.out.println(l.getlops());
	}
}
