package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import bean.KetQua_bean;
import bo.ThamGiaHoc_bo;



public class Ketqua_dao {
	public ThamGiaHoc_bo Tghoc_bo=new ThamGiaHoc_bo();
	
	public ArrayList<KetQua_bean> Getketquas()
	{
		 ArrayList<KetQua_bean> ds_ketqua=new ArrayList<KetQua_bean>();
		cosodulieu1 cs=new cosodulieu1();
		cs.ketnoi1();
		String Lenhsql="Select *from ketqua";
		try {
			PreparedStatement cmd=cs.cn.prepareStatement(Lenhsql);
			ResultSet rs=cmd.executeQuery();
			while(rs.next())
			{
				String MaSV=rs.getString(1);
				String Malophocphan=rs.getString(2);
				String MAMONHOC=rs.getString(3);
				String Magiangvien=rs.getString(4);
				int gioihiansoluongsinhvien=rs.getInt(5);
				String Tenhocphan=rs.getString(6);
				int SODVHT=rs.getInt(7);
				String Tenmonhoc=rs.getString(8);
				String TenGiangVien=rs.getString(9);
				int diemchuyencan=rs.getInt(10);
				int Diemkiemtra=rs.getInt(11);
				int diemthi=rs.getInt(12);
				boolean DaDangki=rs.getBoolean(13);
				double DiemTB=(diemchuyencan+diemthi+Diemkiemtra)/3;
				char KetQua=Tghoc_bo.Ketqua(DiemTB);
				int Diemheso=Tghoc_bo.Heso(KetQua);
				KetQua_bean kq=new KetQua_bean(MaSV, Malophocphan, MAMONHOC, Magiangvien, gioihiansoluongsinhvien, Tenhocphan, SODVHT, Tenmonhoc, TenGiangVien, diemchuyencan, Diemkiemtra, diemthi,DiemTB,KetQua,Diemheso,DaDangki);
				ds_ketqua.add(kq);
			}
			rs.close();
			cs.cn.close();
			return ds_ketqua;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
	public static void main(String[] args) {
		Ketqua_dao k=new Ketqua_dao();
		System.out.println(k.Getketquas());
	}

}
