package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import bean.LopHocPhan_bean;

public class LopHocPhan_dao {
	public ArrayList<LopHocPhan_bean> getLophocs()
	{
		ArrayList<LopHocPhan_bean> ds_lop=new ArrayList<LopHocPhan_bean>();
		cosodulieu1 cs=new cosodulieu1();
		cs.ketnoi1();
		String Lenhsql="Select *from LOPHOCPHAN";
		try {
			PreparedStatement cmd=cs.cn.prepareStatement(Lenhsql);
			ResultSet rs=cmd.executeQuery();
			while(rs.next())
			{
				String maLopHocPhan=rs.getString(1); 
				String maMonHoc =rs.getString(2);
				String maGiangVien=rs.getString(3);
				int gioiHanSoluongsinhvien=rs.getInt(4);
				int namHoc=rs.getInt(5);
				String tenlophocphan=rs.getString(6);
				LopHocPhan_bean l=new LopHocPhan_bean(maLopHocPhan, maMonHoc, maGiangVien, gioiHanSoluongsinhvien, namHoc, tenlophocphan);
				ds_lop.add(l);
			}
			rs.close();
			cs.cn.close();
			return ds_lop;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
		
	}
	public static void main(String[] args) {
		LopHocPhan_dao l=new LopHocPhan_dao();
		System.out.println(l.getLophocs());
	}
}
