package dao;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import bean.DangNhap_bean;


public class DangNhap_dao {
	public ArrayList<DangNhap_bean> GetDangNhaps()
	{
		 ArrayList<DangNhap_bean> ds_DangNhap=new ArrayList<DangNhap_bean>();
		cosodulieu1 cs=new cosodulieu1();
		cs.ketnoi1();
		String Lenhsql="Select *from DangNhap";
		try {
			PreparedStatement cmd=cs.cn.prepareStatement(Lenhsql);
			ResultSet rs=cmd.executeQuery();
			while(rs.next())
			{
				String TenDangNhap=rs.getString(1);
				String MatKhau=rs.getString(2);
				int Quyen=rs.getInt(3);
				DangNhap_bean dn=new DangNhap_bean(TenDangNhap,MatKhau,Quyen);
				ds_DangNhap.add(dn);
			}
			rs.close();
			cs.cn.close();
			return ds_DangNhap;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
	public int ThemDangNhap(String TenDangNhap,String MatKhau,int quyen)
	{
		cosodulieu1 cs=new cosodulieu1();
		cs.ketnoi1();
		String Lenhsql="insert into DangNhap values(?,?,?)";
		try {
			PreparedStatement cmd=cs.cn.prepareStatement(Lenhsql);
			cmd.setString(1, TenDangNhap);
			cmd.setString(2, MatKhau);
			cmd.setInt(3, quyen);
			return cmd.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return 0;
		}		
	}
	public int DoiMatKhau(String TenDangNhap,String MatKhau)
	{
		cosodulieu1 cs=new cosodulieu1();
		cs.ketnoi1();
		String Lenhsql="update DangNhap set MatKhau=? where TenDangNhap=?";
		try {
			PreparedStatement cmd=cs.cn.prepareStatement(Lenhsql);
			cmd.setString(1, MatKhau);
			cmd.setString(2, TenDangNhap);
			return cmd.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return 0;
		}		
	}
	public static void main(String[] args) throws NoSuchAlgorithmException, UnsupportedEncodingException {
		DangNhap_dao d=new DangNhap_dao();
		System.out.println(d.GetDangNhaps());
	}
}
