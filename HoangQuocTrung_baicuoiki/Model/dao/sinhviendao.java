package dao;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;
import java.util.TimeZone;

import bean.SinhVien_bean;
import bo.MP5;


public class sinhviendao {
	public MP5 mp5=new MP5();
	public ArrayList<SinhVien_bean> getssinhvien()
	{
		ArrayList<SinhVien_bean> ds_Sinhvien=new ArrayList<SinhVien_bean>();
		cosodulieu1 cs=new cosodulieu1();
		cs.ketnoi1();
		String Lenhsql="Select *from SINHVIEN";
		try {
			PreparedStatement cmd=cs.cn.prepareStatement(Lenhsql);
			ResultSet rs=cmd.executeQuery();
			while(rs.next())
			{
				String masv=rs.getString(1);
				String hodem=rs.getString(2);
				String ten=rs.getString(3);
				java.util.Date ngaysinh = rs.getDate(4);
				Boolean gioitinh=rs.getBoolean(5);
				String noisinh=rs.getString(6);
				String malop=rs.getString(7);
				String Matkhau=rs.getString(8);
				String Anh=rs.getString(9);
				SinhVien_bean sv=new SinhVien_bean(masv, hodem, ten, ngaysinh, gioitinh, noisinh, malop, Matkhau,Anh);
				ds_Sinhvien.add(sv);
			}
			rs.close();
			cs.cn.close();
			return ds_Sinhvien;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
		
	}
	public int them(String masv,String hodem,String ten,String ngaysinh,Boolean gioitinh,String noisinh,String malop,String MatKhau,String Anh)
	{
		try {
			DateFormat format = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
			format.setTimeZone(TimeZone.getTimeZone("Etc/UTC"));
			cosodulieu1 cs=new cosodulieu1();
			cs.ketnoi1();
			String lenhsql="insert into sinhvien(MASV,HODEM,ten,NGAYSINH,GIOITINH,NOISINH,MALOP,MatKhau) values(?,?,?,?,?,?,?,?)";
			PreparedStatement cmd=cs.cn.prepareStatement(lenhsql);
			cmd.setString(1, masv);
			cmd.setNString(2,hodem);
			cmd.setNString(3, ten);
			java.util.Date ngay=format.parse(ngaysinh.toString());
			java.sql.Date ngaysinh1 = new java.sql.Date(ngay.getTime());
			cmd.setDate(4, ngaysinh1);
			if(gioitinh==true) cmd.setInt(5,1);
			if(gioitinh==false) cmd.setInt(5,0);
			cmd.setString(6, noisinh);
			cmd.setString(7, malop);
			cmd.setString(8, MatKhau);
			cmd.setString(9, Anh);
			return cmd.executeUpdate();
		} catch (Exception tt) {
			tt.printStackTrace();
			return 0;
			// TODO: handle exception
		}
	}
	public int CapNhatAnh(String Anh,String Masv)
	{
		try {
			cosodulieu1 cs=new cosodulieu1();
			cs.ketnoi1();
			String lenhsql="Update sinhvien set anh=? where MASV=?";
			PreparedStatement cmd=cs.cn.prepareStatement(lenhsql);
			cmd.setString(1, Anh);
			cmd.setNString(2,Masv);
			return cmd.executeUpdate();
		} catch (Exception tt) {
			tt.printStackTrace();
			return 0;
			// TODO: handle exception
		}
	}
	public int CapNhatThongtinsinhvien(String Masv,String Tensv,boolean gioitinh,String noisinh,String Ngaysinh)
	{
		try {
			DateFormat format = new SimpleDateFormat("yyyy-mm-dd", Locale.US);
			format.setTimeZone(TimeZone.getTimeZone("Etc/UTC"));
			cosodulieu1 cs=new cosodulieu1();
			cs.ketnoi1();
			String lenhsql="update sinhvien set Hodem=?,TEN=?,NGAYSINH=?,GIOITINH=?,NOISINH=? where MASV=?";
			String hodem=Tensv.substring(0,Tensv.lastIndexOf(" "));
			String ten=Tensv.substring(Tensv.lastIndexOf(" ")+1,Tensv.length());
			java.util.Date ngay=format.parse(Ngaysinh.toString());
			java.sql.Date ngaysinh1 = new java.sql.Date(ngay.getTime());
			PreparedStatement cmd=cs.cn.prepareStatement(lenhsql);
			cmd.setString(1, hodem);
			cmd.setString(2, ten);
			cmd.setDate(3, ngaysinh1);
			cmd.setBoolean(4, gioitinh);
			cmd.setString(5, noisinh);
			cmd.setString(6, Masv);
			return cmd.executeUpdate();
		} catch (Exception e) {
			return 0;
			// TODO: handle exception
		}
		
	}
	public int DoiMatKhau(String MaSV,String MatKhau)
	{
		try {
			cosodulieu1 cs=new cosodulieu1();
			cs.ketnoi1();
			String lenhsql="update sinhvien set MATKHAU=? where MASV=?";
			PreparedStatement cmd=cs.cn.prepareStatement(lenhsql);
			cmd.setString(1, mp5.ecrypt(MatKhau));
			cmd.setString(2, MaSV);
			return cmd.executeUpdate();
		} catch (Exception e) {
			return 0;
			// TODO: handle exception
		}
	}
	public static void main(String[] args) {
		sinhviendao s=new sinhviendao();
		System.out.println(s.DoiMatKhau("191021", "trung2510"));
	}
}
