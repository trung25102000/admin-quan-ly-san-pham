package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import bean.LopHocPhan_bean;
import bean.ThamGiaHoc_bean;

public class ThamGiaHoc_dao {
	public ArrayList<ThamGiaHoc_bean> getLophocs()
	{
		ArrayList<ThamGiaHoc_bean> ds_lop=new ArrayList<ThamGiaHoc_bean>();
		cosodulieu1 cs=new cosodulieu1();
		cs.ketnoi1();
		String Lenhsql="Select *from ThamGiahoc";
		try {
			PreparedStatement cmd=cs.cn.prepareStatement(Lenhsql);
			ResultSet rs=cmd.executeQuery();
			while(rs.next())
			{
				String maLophocphan=rs.getString(1); 
				String maSV=rs.getString(2);
				int diemChuyenCan=rs.getInt(3);
				int diemKiemTra=rs.getInt(4);
				int diemThi=rs.getInt(5);
				boolean Dadangky=rs.getBoolean(6);
				ThamGiaHoc_bean t=new ThamGiaHoc_bean(maLophocphan, maSV, diemChuyenCan, diemKiemTra, diemThi,Dadangky);
				ds_lop.add(t);
			}
			rs.close();
			cs.cn.close();
			return ds_lop;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
		
	}
	public int ThemThamGiaHoc(String MaLopHocPhan,String MaSinhvien)
	{
		cosodulieu1 cs=new cosodulieu1();
		cs.ketnoi1();
		String Lenhsql="insert into ThamGiahoc values(?,?,0,0,0,0)";
		try {
			PreparedStatement cmd=cs.cn.prepareStatement(Lenhsql);
			cmd.setString(1, MaLopHocPhan);
			cmd.setString(2, MaSinhvien);
			return cmd.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
			return 0;
			// TODO: handle exception
		}
	}
	public int XoaThamGiaHoc(String MaLopHocPhan,String MaSinhvien)
	{
		cosodulieu1 cs=new cosodulieu1();
		cs.ketnoi1();
		String Lenhsql="delete ThamGiahoc where malophocphan=? and masv=?";
		try {
			PreparedStatement cmd=cs.cn.prepareStatement(Lenhsql);
			cmd.setString(1, MaLopHocPhan);
			cmd.setString(2, MaSinhvien);
			return cmd.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
			return 0;
			// TODO: handle exception
		}
	}
	public int XacNhanDangKi(String MaLopHocPhan,String MaSinhvien)
	{
		cosodulieu1 cs=new cosodulieu1();
		cs.ketnoi1();
		String Lenhsql="update ThamGiahoc set Dadangki=1 where malophocphan=? and masv=?";
		try {
			PreparedStatement cmd=cs.cn.prepareStatement(Lenhsql);
			cmd.setString(1, MaLopHocPhan);
			cmd.setString(2, MaSinhvien);
			return cmd.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
			return 0;
			// TODO: handle exception
		}
	}
	public int XacNhanDangKi()
	{
		cosodulieu1 cs=new cosodulieu1();
		cs.ketnoi1();
		String Lenhsql="update ThamGiahoc set Dadangki=1 where Dadangki=0";
		try {
			PreparedStatement cmd=cs.cn.prepareStatement(Lenhsql);
			return cmd.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
			return 0;
			// TODO: handle exception
		}
	}
	public static void main(String[] args) {
		ThamGiaHoc_dao t=new ThamGiaHoc_dao();
		System.out.println(t.XoaThamGiaHoc("2", "19t1021"));
	}
}
