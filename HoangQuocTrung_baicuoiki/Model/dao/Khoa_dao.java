package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import bean.Khoa_bean;
import bean.Lop_bean;

public class Khoa_dao {
	public ArrayList<Khoa_bean> getkhoas()
	{
		ArrayList<Khoa_bean> ds_khoa=new ArrayList<Khoa_bean>();
		cosodulieu1 cs=new cosodulieu1();
		cs.ketnoi1();
		String Lenhsql="Select *from khoa";
		try {
			PreparedStatement cmd=cs.cn.prepareStatement(Lenhsql);
			ResultSet rs=cmd.executeQuery();
			while(rs.next())
			{
				String makhoa=rs.getString(1);
				String Tenkhoa=rs.getString(2);
				String Sodienthoai=rs.getString(3);
				Khoa_bean k=new Khoa_bean(makhoa, Tenkhoa, Sodienthoai);
				ds_khoa.add(k);
			}
			rs.close();
			cs.cn.close();
			return ds_khoa;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
	public static void main(String[] args) {
		Khoa_dao k=new Khoa_dao();
		System.out.println(k.getkhoas());
	}
}
