package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import bean.MonHoc_bean;
import bean.SinhVien_bean;

public class Monhoc_dao {
	public ArrayList<MonHoc_bean> getMonhocs()
	{
		ArrayList<MonHoc_bean> ds_monhoc=new ArrayList<MonHoc_bean>();
		cosodulieu1 cs=new cosodulieu1();
		cs.ketnoi1();
		String Lenhsql="Select *from Monhoc";
		try {
			PreparedStatement cmd=cs.cn.prepareStatement(Lenhsql);
			ResultSet rs=cmd.executeQuery();
			while(rs.next())
			{
				String MaMonHoc=rs.getString(1);
				String TenMonHoc=rs.getString(2);
				int DVHT=rs.getInt(3);
				MonHoc_bean mh=new MonHoc_bean(MaMonHoc, TenMonHoc, DVHT);
				ds_monhoc.add(mh);
			}
			rs.close();
			cs.cn.close();
			return ds_monhoc;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
		
	}
	public static void main(String[] args) {
		Monhoc_dao m=new Monhoc_dao();
		System.out.println(m.getMonhocs());
	}
}
