package bean;

public class Khoa_bean {
	private String makhoa;
	private String tenkhoa;
	private String Sdt;
	@Override
	public String toString() {
		return "diemthi [makhoa=" + makhoa + ", tenkhoa=" + tenkhoa + ", Sdt=" + Sdt + "]";
	}
	public String getMakhoa() {
		return makhoa;
	}
	public void setMakhoa(String makhoa) {
		this.makhoa = makhoa;
	}
	public String getTenkhoa() {
		return tenkhoa;
	}
	public void setTenkhoa(String tenkhoa) {
		this.tenkhoa = tenkhoa;
	}
	public String getSdt() {
		return Sdt;
	}
	public void setSdt(String sdt) {
		Sdt = sdt;
	}
	public Khoa_bean(String makhoa, String tenkhoa, String sdt) {
		super();
		this.makhoa = makhoa;
		this.tenkhoa = tenkhoa;
		Sdt = sdt;
	}
	public Khoa_bean() {
		super();
		// TODO Auto-generated constructor stub
	}
	
}
