package bean;

public class GiangVien_bean {
	private String MaGiangVien;
	private String Makhoa;
	private String TenGiangVien;
	private String QueQuan;
	private String MatKhau;
	private String NgaySinh;
	public String getMaGiangVien() {
		return MaGiangVien;
	}
	public void setMaGiangVien(String maGiangVien) {
		MaGiangVien = maGiangVien;
	}
	public String getMakhoa() {
		return Makhoa;
	}
	public void setMakhoa(String makhoa) {
		Makhoa = makhoa;
	}
	public String getTenGiangVien() {
		return TenGiangVien;
	}
	public void setTenGiangVien(String tenGiangVien) {
		TenGiangVien = tenGiangVien;
	}
	public String getQueQuan() {
		return QueQuan;
	}
	public void setQueQuan(String queQuan) {
		QueQuan = queQuan;
	}
	public String getMatKhau() {
		return MatKhau;
	}
	public void setMatKhau(String matKhau) {
		MatKhau = matKhau;
	}
	public String getNgaySinh() {
		return NgaySinh;
	}
	public void setNgaySinh(String ngaySinh) {
		NgaySinh = ngaySinh;
	}
	@Override
	public String toString() {
		return "GiangVien_bean [MaGiangVien=" + MaGiangVien + ", Makhoa=" + Makhoa + ", TenGiangVien=" + TenGiangVien
				+ ", QueQuan=" + QueQuan + ", MatKhau=" + MatKhau + ", NgaySinh=" + NgaySinh + "]";
	}
	public GiangVien_bean(String maGiangVien, String makhoa, String tenGiangVien, String queQuan, String matKhau,
			String ngaySinh) {
		super();
		MaGiangVien = maGiangVien;
		Makhoa = makhoa;
		TenGiangVien = tenGiangVien;
		QueQuan = queQuan;
		MatKhau = matKhau;
		NgaySinh = ngaySinh;
	}
	public GiangVien_bean() {
		super();
		// TODO Auto-generated constructor stub
	}
	
}
