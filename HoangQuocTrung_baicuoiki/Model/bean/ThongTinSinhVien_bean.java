package bean;

import java.util.Date;

public class ThongTinSinhVien_bean {
	private String masv;
	private String HoTenSV;
	private java.util.Date ngaysinh;
	private Boolean gioitinh;
	private String Tenlop;
	public double DiemGPA;
	public int khoa;
	public String HocLuc;
	public String getMasv() {
		return masv;
	}
	public void setMasv(String masv) {
		this.masv = masv;
	}
	public String getHoTenSV() {
		return HoTenSV;
	}
	public void setHoTenSV(String hoTenSV) {
		HoTenSV = hoTenSV;
	}
	public java.util.Date getNgaysinh() {
		return ngaysinh;
	}
	public void setNgaysinh(java.util.Date ngaysinh) {
		this.ngaysinh = ngaysinh;
	}
	public Boolean getGioitinh() {
		return gioitinh;
	}
	public void setGioitinh(Boolean gioitinh) {
		this.gioitinh = gioitinh;
	}
	public String getTenlop() {
		return Tenlop;
	}
	public void setTenlop(String tenlop) {
		Tenlop = tenlop;
	}
	public double getDiemGPA() {
		return DiemGPA;
	}
	public void setDiemGPA(double diemGPA) {
		DiemGPA = diemGPA;
	}
	public int getKhoa() {
		return khoa;
	}
	public void setKhoa(int khoa) {
		this.khoa = khoa;
	}
	public String getHocLuc() {
		return HocLuc;
	}
	public void setHocLuc(String hocLuc) {
		HocLuc = hocLuc;
	}
	@Override
	public String toString() {
		return "ThongTinSinhVien_bean [masv=" + masv + ", HoTenSV=" + HoTenSV + ", ngaysinh=" + ngaysinh + ", gioitinh="
				+ gioitinh + ", Tenlop=" + Tenlop + ", DiemGPA=" + DiemGPA + ", khoa=" + khoa + ", HocLuc=" + HocLuc
				+ "]";
	}
	public ThongTinSinhVien_bean(String masv, String hoTenSV, Date ngaysinh, Boolean gioitinh, String tenlop,
			double diemGPA, int khoa, String hocLuc) {
		super();
		this.masv = masv;
		HoTenSV = hoTenSV;
		this.ngaysinh = ngaysinh;
		this.gioitinh = gioitinh;
		Tenlop = tenlop;
		DiemGPA = diemGPA;
		this.khoa = khoa;
		HocLuc = hocLuc;
	}
	public ThongTinSinhVien_bean() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
	
}
