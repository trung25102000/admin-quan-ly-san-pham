package bean;

import java.util.Date;

public class SinhVien_bean {
	private String masv;
	private String hodem;
	private String ten;
	private java.util.Date ngaysinh;
	private Boolean gioitinh;
	private String noisinh;
	private String malop;
	private String Matkhau;
	private String Anh;
	public String getMasv() {
		return masv;
	}
	public void setMasv(String masv) {
		this.masv = masv;
	}
	public String getHodem() {
		return hodem;
	}
	public void setHodem(String hodem) {
		this.hodem = hodem;
	}
	public String getTen() {
		return ten;
	}
	public void setTen(String ten) {
		this.ten = ten;
	}
	public java.util.Date getNgaysinh() {
		return ngaysinh;
	}
	public void setNgaysinh(java.util.Date ngaysinh) {
		this.ngaysinh = ngaysinh;
	}
	public Boolean getGioitinh() {
		return gioitinh;
	}
	public void setGioitinh(Boolean gioitinh) {
		this.gioitinh = gioitinh;
	}
	public String getNoisinh() {
		return noisinh;
	}
	public void setNoisinh(String noisinh) {
		this.noisinh = noisinh;
	}
	public String getMalop() {
		return malop;
	}
	public void setMalop(String malop) {
		this.malop = malop;
	}
	public String getMatkhau() {
		return Matkhau;
	}
	public void setMatkhau(String matkhau) {
		this.Matkhau = matkhau;
	}
	public String getAnh() {
		return Anh;
	}
	public void setAnh(String anh) {
		this.Anh = anh;
	}
	@Override
	public String toString() {
		return "SinhVien_bean [masv=" + masv + ", hodem=" + hodem + ", ten=" + ten + ", ngaysinh=" + ngaysinh
				+ ", gioitinh=" + gioitinh + ", noisinh=" + noisinh + ", malop=" + malop + ", Matkhau=" + Matkhau
				+ ", Anh=" + Anh + "]";
	}
	public SinhVien_bean(String masv, String hodem, String ten, Date ngaysinh, Boolean gioitinh, String noisinh,
			String malop, String matkhau, String anh) {
		super();
		this.masv = masv;
		this.hodem = hodem;
		this.ten = ten;
		this.ngaysinh = ngaysinh;
		this.gioitinh = gioitinh;
		this.noisinh = noisinh;
		this.malop = malop;
		this.Matkhau = matkhau;
		this.Anh = anh;
	}
	public SinhVien_bean() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
}
