package bean;

public class KetQua_bean {
	private String MaSV;
	private String Malophocphan;
	private String MaMonhoc;
	private String MagiangVien;
	private int GioihanSoluongSinhVien;
	private String TenHocPhan;
	private int SoDVHT;
	private String TenMonHoc;
	private String TenGiangVien;
	private int DiemChuyenCan;
	private int DiemKiemTra;
	private int DiemThi;
	private double DiemTB;
	private char KetQua;
	private int Diemheso;
	private boolean Dadangki;
	public KetQua_bean(String maSV, String malophocphan, String maMonhoc, String magiangVien,
			int gioihanSoluongSinhVien, String tenHocPhan, int soDVHT, String tenMonHoc, String tenGiangVien,
			int diemChuyenCan, int diemKiemTra, int diemThi, double diemTB, char ketQua, int diemheso,
			boolean dadangki) {
		super();
		MaSV = maSV;
		Malophocphan = malophocphan;
		MaMonhoc = maMonhoc;
		MagiangVien = magiangVien;
		GioihanSoluongSinhVien = gioihanSoluongSinhVien;
		TenHocPhan = tenHocPhan;
		SoDVHT = soDVHT;
		TenMonHoc = tenMonHoc;
		TenGiangVien = tenGiangVien;
		DiemChuyenCan = diemChuyenCan;
		DiemKiemTra = diemKiemTra;
		DiemThi = diemThi;
		DiemTB = diemTB;
		KetQua = ketQua;
		Diemheso = diemheso;
		Dadangki = dadangki;
	}
	public boolean isDadangki() {
		return Dadangki;
	}
	public void setDadangki(boolean dadangki) {
		Dadangki = dadangki;
	}
	public KetQua_bean(String maSV, String malophocphan, String maMonhoc, String magiangVien,
			int gioihanSoluongSinhVien, String tenHocPhan, int soDVHT, String tenMonHoc, String tenGiangVien,
			int diemChuyenCan, int diemKiemTra, int diemThi, double diemTB, char ketQua, int diemheso) {
		super();
		MaSV = maSV;
		Malophocphan = malophocphan;
		MaMonhoc = maMonhoc;
		MagiangVien = magiangVien;
		GioihanSoluongSinhVien = gioihanSoluongSinhVien;
		TenHocPhan = tenHocPhan;
		SoDVHT = soDVHT;
		TenMonHoc = tenMonHoc;
		TenGiangVien = tenGiangVien;
		DiemChuyenCan = diemChuyenCan;
		DiemKiemTra = diemKiemTra;
		DiemThi = diemThi;
		DiemTB = diemTB;
		KetQua = ketQua;
		Diemheso = diemheso;
	}
	public double getDiemTB() {
		return DiemTB;
	}
	public void setDiemTB(double diemTB) {
		DiemTB = diemTB;
	}
	public char getKetQua() {
		return KetQua;
	}
	public void setKetQua(char ketQua) {
		KetQua = ketQua;
	}
	public int getDiemheso() {
		return Diemheso;
	}
	public void setDiemheso(int diemheso) {
		Diemheso = diemheso;
	}
	public String getMaSV() {
		return MaSV;
	}
	public void setMaSV(String maSV) {
		MaSV = maSV;
	}
	public String getMalophocphan() {
		return Malophocphan;
	}
	public void setMalophocphan(String malophocphan) {
		Malophocphan = malophocphan;
	}
	public String getMaMonhoc() {
		return MaMonhoc;
	}
	public void setMaMonhoc(String maMonhoc) {
		MaMonhoc = maMonhoc;
	}
	public String getMagiangVien() {
		return MagiangVien;
	}
	public void setMagiangVien(String magiangVien) {
		MagiangVien = magiangVien;
	}
	public int getGioihanSoluongSinhVien() {
		return GioihanSoluongSinhVien;
	}
	public void setGioihanSoluongSinhVien(int gioihanSoluongSinhVien) {
		GioihanSoluongSinhVien = gioihanSoluongSinhVien;
	}
	public String getTenHocPhan() {
		return TenHocPhan;
	}
	public void setTenHocPhan(String tenHocPhan) {
		TenHocPhan = tenHocPhan;
	}
	public int getSoDVHT() {
		return SoDVHT;
	}
	public void setSoDVHT(int soDVHT) {
		SoDVHT = soDVHT;
	}
	public String getTenMonHoc() {
		return TenMonHoc;
	}
	public void setTenMonHoc(String tenMonHoc) {
		TenMonHoc = tenMonHoc;
	}
	public String getTenGiangVien() {
		return TenGiangVien;
	}
	public void setTenGiangVien(String tenGiangVien) {
		TenGiangVien = tenGiangVien;
	}
	public int getDiemChuyenCan() {
		return DiemChuyenCan;
	}
	public void setDiemChuyenCan(int diemChuyenCan) {
		DiemChuyenCan = diemChuyenCan;
	}
	public int getDiemKiemTra() {
		return DiemKiemTra;
	}
	public void setDiemKiemTra(int diemKiemTra) {
		DiemKiemTra = diemKiemTra;
	}
	public int getDiemThi() {
		return DiemThi;
	}
	public void setDiemThi(int diemThi) {
		DiemThi = diemThi;
	}
	public KetQua_bean() {
		super();
		// TODO Auto-generated constructor stub
	}
	public KetQua_bean(String maSV, String malophocphan, String maMonhoc, String magiangVien,
			int gioihanSoluongSinhVien, String tenHocPhan, int soDVHT, String tenMonHoc, String tenGiangVien,
			int diemChuyenCan, int diemKiemTra, int diemThi) {
		super();
		MaSV = maSV;
		Malophocphan = malophocphan;
		MaMonhoc = maMonhoc;
		MagiangVien = magiangVien;
		GioihanSoluongSinhVien = gioihanSoluongSinhVien;
		TenHocPhan = tenHocPhan;
		SoDVHT = soDVHT;
		TenMonHoc = tenMonHoc;
		TenGiangVien = tenGiangVien;
		DiemChuyenCan = diemChuyenCan;
		DiemKiemTra = diemKiemTra;
		DiemThi = diemThi;
	}
	@Override
	public String toString() {
		return "KetQua_bean [MaSV=" + MaSV + ", Malophocphan=" + Malophocphan + ", MaMonhoc=" + MaMonhoc
				+ ", MagiangVien=" + MagiangVien + ", GioihanSoluongSinhVien=" + GioihanSoluongSinhVien
				+ ", TenHocPhan=" + TenHocPhan + ", SoDVHT=" + SoDVHT + ", TenMonHoc=" + TenMonHoc + ", TenGiangVien="
				+ TenGiangVien + ", DiemChuyenCan=" + DiemChuyenCan + ", DiemKiemTra=" + DiemKiemTra + ", DiemThi="
				+ DiemThi + ", DiemTB=" + DiemTB + ", KetQua=" + KetQua + ", Diemheso=" + Diemheso + ", Dadangki="
				+ Dadangki + ", getDiemTB()=" + getDiemTB() + ", getKetQua()=" + getKetQua() + ", getDiemheso()="
				+ getDiemheso() + ", getMaSV()=" + getMaSV() + ", getMalophocphan()=" + getMalophocphan()
				+ ", getMaMonhoc()=" + getMaMonhoc() + ", getMagiangVien()=" + getMagiangVien()
				+ ", getGioihanSoluongSinhVien()=" + getGioihanSoluongSinhVien() + ", getTenHocPhan()="
				+ getTenHocPhan() + ", getSoDVHT()=" + getSoDVHT() + ", getTenMonHoc()=" + getTenMonHoc()
				+ ", getTenGiangVien()=" + getTenGiangVien() + ", getDiemChuyenCan()=" + getDiemChuyenCan()
				+ ", getDiemKiemTra()=" + getDiemKiemTra() + ", getDiemThi()=" + getDiemThi() + ", getClass()="
				+ getClass() + ", hashCode()=" + hashCode() + ", toString()=" + super.toString() + "]";
	}
	

}
