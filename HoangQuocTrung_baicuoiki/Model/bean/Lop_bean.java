package bean;

public class Lop_bean {
	private String malop;
	private String tenlop;
	private int khoa;
	private String hedaotao;
	private int namnhaphoc;
	private int siso;
	private String makhoa;
	public String getMalop() {
		return malop;
	}
	public void setMalop(String malop) {
		this.malop = malop;
	}
	public String getTenlop() {
		return tenlop;
	}
	public void setTenlop(String tenlop) {
		this.tenlop = tenlop;
	}
	public int getKhoa() {
		return khoa;
	}
	public void setKhoa(int khoa) {
		this.khoa = khoa;
	}
	public String getHedaotao() {
		return hedaotao;
	}
	public void setHedaotao(String hedaotao) {
		this.hedaotao = hedaotao;
	}
	public int getNamnhaphoc() {
		return namnhaphoc;
	}
	public void setNamnhaphoc(int namnhaphoc) {
		this.namnhaphoc = namnhaphoc;
	}
	public int getSiso() {
		return siso;
	}
	public void setSiso(int siso) {
		this.siso = siso;
	}
	public String getMakhoa() {
		return makhoa;
	}
	public void setMakhoa(String makhoa) {
		this.makhoa = makhoa;
	}
	@Override
	public String toString() {
		return "lop [malop=" + malop + ", tenlop=" + tenlop + ", khoa=" + khoa + ", hedaotao=" + hedaotao
				+ ", namnhaphoc=" + namnhaphoc + ", siso=" + siso + ", makhoa=" + makhoa + "]";
	}
	public Lop_bean(String malop, String tenlop, int khoa, String hedaotao, int namnhaphoc, int siso, String makhoa) {
		super();
		this.malop = malop;
		this.tenlop = tenlop;
		this.khoa = khoa;
		this.hedaotao = hedaotao;
		this.namnhaphoc = namnhaphoc;
		this.siso = siso;
		this.makhoa = makhoa;
	}
	public Lop_bean() {
		super();
		// TODO Auto-generated constructor stub
	}
	
}
