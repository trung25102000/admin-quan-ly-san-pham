package bean;

public class Chodangki_bean {
	private String MaLopHocPhan;
	private String TenHocPhan;
	private int SODVHT;
	private String TenGiangVien;
	private boolean Dadangki;
	private String HotenSv;
	public String getMaLopHocPhan() {
		return MaLopHocPhan;
	}
	public void setMaLopHocPhan(String maLopHocPhan) {
		MaLopHocPhan = maLopHocPhan;
	}
	public String getTenHocPhan() {
		return TenHocPhan;
	}
	public void setTenHocPhan(String tenHocPhan) {
		TenHocPhan = tenHocPhan;
	}
	public int getSODVHT() {
		return SODVHT;
	}
	public void setSODVHT(int sODVHT) {
		SODVHT = sODVHT;
	}
	public String getTenGiangVien() {
		return TenGiangVien;
	}
	public void setTenGiangVien(String tenGiangVien) {
		TenGiangVien = tenGiangVien;
	}
	public boolean isDadangki() {
		return Dadangki;
	}
	public void setDadangki(boolean dadangki) {
		Dadangki = dadangki;
	}
	public String getHotenSv() {
		return HotenSv;
	}
	public void setHotenSv(String hotenSv) {
		HotenSv = hotenSv;
	}
	@Override
	public String toString() {
		return "Chodangki_bean [MaLopHocPhan=" + MaLopHocPhan + ", TenHocPhan=" + TenHocPhan + ", SODVHT=" + SODVHT
				+ ", TenGiangVien=" + TenGiangVien + ", Dadangki=" + Dadangki + ", HotenSv=" + HotenSv + "]";
	}
	public Chodangki_bean(String maLopHocPhan, String tenHocPhan, int sODVHT, String tenGiangVien, boolean dadangki,
			String hotenSv) {
		super();
		MaLopHocPhan = maLopHocPhan;
		TenHocPhan = tenHocPhan;
		SODVHT = sODVHT;
		TenGiangVien = tenGiangVien;
		Dadangki = dadangki;
		HotenSv = hotenSv;
	}
	public Chodangki_bean() {
		super();
		// TODO Auto-generated constructor stub
	}
	
}
