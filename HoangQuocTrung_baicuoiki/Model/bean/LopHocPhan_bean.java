package bean;

public class LopHocPhan_bean {
	private String MaLopHocPhan;
	private String MaMonHoc;
	private String MaGiangVien;
	private int GioiHanSoluongsinhvien;
	private int NamHoc;
	private String Tenlophocphan;
	public String getMaLopHocPhan() {
		return MaLopHocPhan;
	}
	public void setMaLopHocPhan(String maLopHocPhan) {
		MaLopHocPhan = maLopHocPhan;
	}
	public String getMaMonHoc() {
		return MaMonHoc;
	}
	public void setMaMonHoc(String maMonHoc) {
		MaMonHoc = maMonHoc;
	}
	public String getMaGiangVien() {
		return MaGiangVien;
	}
	public void setMaGiangVien(String maGiangVien) {
		MaGiangVien = maGiangVien;
	}
	public int getGioiHanSoluongsinhvien() {
		return GioiHanSoluongsinhvien;
	}
	public void setGioiHanSoluongsinhvien(int gioiHanSoluongsinhvien) {
		GioiHanSoluongsinhvien = gioiHanSoluongsinhvien;
	}
	public int getNamHoc() {
		return NamHoc;
	}
	public void setNamHoc(int namHoc) {
		NamHoc = namHoc;
	}
	public String getTenlophocphan() {
		return Tenlophocphan;
	}
	public void setTenlophocphan(String tenlophocphan) {
		Tenlophocphan = tenlophocphan;
	}
	@Override
	public String toString() {
		return "LopHocPhan_bean [MaLopHocPhan=" + MaLopHocPhan + ", MaMonHoc=" + MaMonHoc + ", MaGiangVien="
				+ MaGiangVien + ", GioiHanSoluongsinhvien=" + GioiHanSoluongsinhvien + ", NamHoc=" + NamHoc
				+ ", Tenlophocphan=" + Tenlophocphan + "]";
	}
	public LopHocPhan_bean(String maLopHocPhan, String maMonHoc, String maGiangVien, int gioiHanSoluongsinhvien,
			int namHoc, String tenlophocphan) {
		super();
		MaLopHocPhan = maLopHocPhan;
		MaMonHoc = maMonHoc;
		MaGiangVien = maGiangVien;
		GioiHanSoluongsinhvien = gioiHanSoluongsinhvien;
		NamHoc = namHoc;
		Tenlophocphan = tenlophocphan;
	}
	public LopHocPhan_bean() {
		super();
		// TODO Auto-generated constructor stub
	}
	
}
