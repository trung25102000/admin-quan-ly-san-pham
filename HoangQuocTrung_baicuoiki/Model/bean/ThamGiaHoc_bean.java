package bean;

public class ThamGiaHoc_bean {
	private String MaLophocphan;
	private String MaSV;
	private int DiemChuyenCan;
	private int DiemKiemTra;
	private int DiemThi;
	private boolean DaDangky;
	private double DiemTB;
	private char KetQua;
	@Override
	public String toString() {
		return "ThamGiaHoc_bean [MaLophocphan=" + MaLophocphan + ", MaSV=" + MaSV + ", DiemChuyenCan=" + DiemChuyenCan
				+ ", DiemKiemTra=" + DiemKiemTra + ", DiemThi=" + DiemThi + ", DaDangky=" + DaDangky + ", DiemTB="
				+ DiemTB + ", KetQua=" + KetQua + "]";
	}
	public ThamGiaHoc_bean() {
		super();
		// TODO Auto-generated constructor stub
	}
	public boolean isDaDangky() {
		return DaDangky;
	}
	public void setDaDangky(boolean daDangky) {
		DaDangky = daDangky;
	}
	public ThamGiaHoc_bean(String maLophocphan, String maSV, int diemChuyenCan, int diemKiemTra, int diemThi) {
		super();
		MaLophocphan = maLophocphan;
		MaSV = maSV;
		DiemChuyenCan = diemChuyenCan;
		DiemKiemTra = diemKiemTra;
		DiemThi = diemThi;
	}
	public ThamGiaHoc_bean(String maLophocphan, String maSV, int diemChuyenCan, int diemKiemTra, int diemThi,
			boolean daDangky) {
		super();
		MaLophocphan = maLophocphan;
		MaSV = maSV;
		DiemChuyenCan = diemChuyenCan;
		DiemKiemTra = diemKiemTra;
		DiemThi = diemThi;
		DaDangky = daDangky;
	}
	public String getMaLophocphan() {
		return MaLophocphan;
	}
	public void setMaLophocphan(String maLophocphan) {
		MaLophocphan = maLophocphan;
	}
	public String getMaSV() {
		return MaSV;
	}
	public void setMaSV(String maSV) {
		MaSV = maSV;
	}
	public int getDiemChuyenCan() {
		return DiemChuyenCan;
	}
	public void setDiemChuyenCan(int diemChuyenCan) {
		DiemChuyenCan = diemChuyenCan;
	}
	public int getDiemKiemTra() {
		return DiemKiemTra;
	}
	public void setDiemKiemTra(int diemKiemTra) {
		DiemKiemTra = diemKiemTra;
	}
	public int getDiemThi() {
		return DiemThi;
	}
	public void setDiemThi(int diemThi) {
		DiemThi = diemThi;
	}
	public double getDiemTB() {
		return DiemTB;
	}
	public void setDiemTB(double diemTB) {
		this.DiemTB = (DiemChuyenCan+DiemKiemTra+DiemThi)/3;
	}
	public char getKetQua() {
		return KetQua;
	}
	public void setKetQua(char ketQua) {
		this.KetQua = 'F';
		if(DiemTB>=5 && DiemTB<=6.5) KetQua = 'D';
		if(DiemTB>6.5 && DiemTB<=7.5) KetQua = 'C';
		if(DiemTB>7.5 && DiemTB<=8.5) KetQua = 'B';
		if(DiemTB>8.5 && DiemTB<=10) KetQua = 'A';
		
	}
	
}
