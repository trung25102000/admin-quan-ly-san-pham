package bean;

public class MonHoc_bean {
	private String mamonhoc;
	private String tenmonhoc;
	private int SODVHT;
	public String getMamonhoc() {
		return mamonhoc;
	}
	public void setMamonhoc(String mamonhoc) {
		this.mamonhoc = mamonhoc;
	}
	public String getTenmonhoc() {
		return tenmonhoc;
	}
	public void setTenmonhoc(String tenmonhoc) {
		this.tenmonhoc = tenmonhoc;
	}
	public int getSODVHT() {
		return SODVHT;
	}
	public void setSODVHT(int sODVHT) {
		SODVHT = sODVHT;
	}
	@Override
	public String toString() {
		return "diemthi [mamonhoc=" + mamonhoc + ", tenmonhoc=" + tenmonhoc + ", SODVHT=" + SODVHT + "]";
	}
	public MonHoc_bean(String mamonhoc, String tenmonhoc, int sODVHT) {
		super();
		this.mamonhoc = mamonhoc;
		this.tenmonhoc = tenmonhoc;
		SODVHT = sODVHT;
	}
	public MonHoc_bean() {
		super();
		// TODO Auto-generated constructor stub
	}
	

}
