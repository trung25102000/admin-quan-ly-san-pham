package bean;

public class DangNhap_bean {
	private String TenDangNhap;
	private String MatKhau;
	private int quyen;
	@Override
	public String toString() {
		return "DangNhap_bean [TenDangNhap=" + TenDangNhap + ", MatKhau=" + MatKhau + ", quyen=" + quyen + "]";
	}
	public DangNhap_bean(String tenDangNhap, String matKhau, int quyen) {
		super();
		TenDangNhap = tenDangNhap;
		MatKhau = matKhau;
		this.quyen = quyen;
	}
	public DangNhap_bean() {
		super();
		// TODO Auto-generated constructor stub
	}
	public String getTenDangNhap() {
		return TenDangNhap;
	}
	public void setTenDangNhap(String tenDangNhap) {
		TenDangNhap = tenDangNhap;
	}
	public String getMatKhau() {
		return MatKhau;
	}
	public void setMatKhau(String matKhau) {
		MatKhau = matKhau;
	}
	public int getQuyen() {
		return quyen;
	}
	public void setQuyen(int quyen) {
		this.quyen = quyen;
	}
	
	

}
