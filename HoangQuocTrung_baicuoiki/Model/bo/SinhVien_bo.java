package bo;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;

import bean.SinhVien_bean;
import dao.sinhviendao;

public class SinhVien_bo {
	public sinhviendao sv_dao=new sinhviendao();
	public MP5 mp5=new MP5();
	public ArrayList<SinhVien_bean> GetsSinhVien()
	{
		return sv_dao.getssinhvien();
	}
	public SinhVien_bean KiemTraDangNhap(String MaSinhVien,String MatKhau) throws NoSuchAlgorithmException, UnsupportedEncodingException
	{
		for(SinhVien_bean sv:GetsSinhVien())
		{
			if(sv.getMasv().equals(MaSinhVien) && sv.getMatkhau().equals(mp5.ecrypt(MatKhau)))
				return sv;
		}
		return null;
	}
	public int them(String masv,String hodem,String ten,String ngaysinh,Boolean gioitinh,String noisinh,String malop,String MatKhau,String Anh)
	{
		return sv_dao.them(masv, hodem, ten, ngaysinh, gioitinh, noisinh, malop, MatKhau,Anh);
	}
	public int CapNhatAnh(String Anh,String MaSinhVien)
	{
		return sv_dao.CapNhatAnh(Anh, MaSinhVien);
	}
	public int CapNhatSinhVien(String Masv,String Tensv,boolean gioitinh,String noisinh,String Ngaysinh)
	{
		return sv_dao.CapNhatThongtinsinhvien(Masv, Tensv, gioitinh, noisinh, Ngaysinh);
	}
	public SinhVien_bean GetSinhvienbyID(String ID)
	{
		for(SinhVien_bean s:GetsSinhVien())
		{
			if(s.getMasv().equals(ID))
			return s;
		}
		return null;
	}
	public int DoiMatKhau(String MaSV,String MatKhau)
	{
		return sv_dao.DoiMatKhau(MaSV, MatKhau);
	}
	public static void main(String[] args) {
		SinhVien_bo sv_bo=new SinhVien_bo();
		System.out.println(sv_bo.DoiMatKhau("191021", "trung2510"));
	}
}
