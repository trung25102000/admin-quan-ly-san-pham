package bo;

import java.util.ArrayList;

import bean.DangNhap_bean;
import dao.DangNhap_dao;

public class DangNhap_bo {
	public DangNhap_dao dn_dao=new DangNhap_dao();
	public ArrayList<DangNhap_bean> GetsDangNhap()
	{
		return dn_dao.GetDangNhaps();
	}
	public DangNhap_bean KiemTraDangNhap(String TenDangNhap,String MatKhau)
	{
		for(DangNhap_bean d:GetsDangNhap())
		{
			if(d.getTenDangNhap().equals(TenDangNhap) && d.getMatKhau().equals(MatKhau))
				return d;
		}
		return null;
	}
	public int DoiMatKhau(String TenDangNhap,String MatKhau)
	{
		return dn_dao.DoiMatKhau(TenDangNhap, MatKhau);
	}
	public static void main(String[] args) {
		DangNhap_bo d=new DangNhap_bo();
		System.out.println(d.DoiMatKhau("HoangHa", "hoangha"));
	}
}
