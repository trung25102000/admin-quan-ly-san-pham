package bo;

import java.util.ArrayList;

import bean.Lop_bean;
import dao.Lop_dao;

public class Lop_bo {
	public Lop_dao l_dao=new Lop_dao();
	public ArrayList<Lop_bean> Getlops()
	{
		return l_dao.getlops();
	}
	public Lop_bean Getlopbymalop(String Malop)
	{
		for(Lop_bean l:Getlops())
		{
			if(l.getMalop().equals(Malop))
				return l;
		}
		return null;
	}
	public static void main(String[] args) {
		Lop_bo l=new Lop_bo();
		System.out.println(l.Getlopbymalop("C24101"));
	}
}
