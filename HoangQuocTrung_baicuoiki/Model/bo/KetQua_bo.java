package bo;

import java.util.ArrayList;

import bean.KetQua_bean;
import dao.Ketqua_dao;

public class KetQua_bo {
	public Ketqua_dao Kq_dao=new Ketqua_dao();
	public ArrayList<KetQua_bean> Getketquas()
	{
		return Kq_dao.Getketquas();
	}
	public ArrayList<KetQua_bean> Getketqua(String Masv)
	{
		ArrayList<KetQua_bean> Dsketqua=new ArrayList<KetQua_bean>();
		for(KetQua_bean k:Getketquas())
		{
			if(k.isDadangki()==true && k.getMaSV().equals(Masv))
				Dsketqua.add(k);
		}
		return Dsketqua;
		
	}
	public ArrayList<KetQua_bean> Getketquadaduyet(String Masv)
	{
		ArrayList<KetQua_bean> Dsketqua=new ArrayList<KetQua_bean>();
		for(KetQua_bean k:Getketquas())
		{
			if(k.isDadangki()==true && k.getMaSV().equals(Masv) && k.getDiemChuyenCan()==0 && k.getDiemKiemTra()==0 && k.getDiemThi()==0)
				Dsketqua.add(k);
		}
		return Dsketqua;
		
	}
	public ArrayList<KetQua_bean> Getketquadaduyet()
	{
		ArrayList<KetQua_bean> Dsketqua=new ArrayList<KetQua_bean>();
		for(KetQua_bean k:Getketquas())
		{
			if(k.isDadangki()==false)
				Dsketqua.add(k);
		}
		return Dsketqua;
		
	}
	public int Sotinduocduyet(String Masv)
	{
		int a=0;
		for(KetQua_bean k:Getketquas())
		{
			if(k.isDadangki()==true && k.getMaSV().equals(Masv) && k.getDiemChuyenCan()==0 && k.getDiemKiemTra()==0 && k.getDiemThi()==0)
				a=a+k.getSoDVHT();
		}
		return a;
		
	}
}
