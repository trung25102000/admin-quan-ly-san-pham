package bo;

import java.util.ArrayList;

import bean.Chodangki_bean;
import dao.Chodangki_dao;

public class ChoDangKi_bo {
	public Chodangki_dao Cho_dao=new Chodangki_dao();
	public ArrayList<Chodangki_bean> Getchodangkis()
	{
		return Cho_dao.Getchodangkis();
	}
	public ArrayList<Chodangki_bean> Getchodangkitheotens(String Tensv)
	{
		ArrayList<Chodangki_bean> dscho=new ArrayList<Chodangki_bean>();
		for(Chodangki_bean c:Getchodangkis())
		{
			if(c.getHotenSv().equals(Tensv) && c.isDadangki()==false)
				dscho.add(c);
		}
		return dscho;
	}
	public ArrayList<Chodangki_bean> Getchodangkitheotendaduyets(String Tensv)
	{
		ArrayList<Chodangki_bean> dscho=new ArrayList<Chodangki_bean>();
		for(Chodangki_bean c:Getchodangkis())
		{
			if(c.getHotenSv().equals(Tensv) && c.isDadangki()==true)
				dscho.add(c);
		}
		return dscho;
	}
	public int Solop()
	{
		int a=0;
		for(Chodangki_bean c:Getchodangkis())
		{
			if(c.isDadangki()==false)
			a++;
		}
		return a;
	}
	public int Tongtinchi()
	{
		int a=0;
		for(Chodangki_bean c:Getchodangkis())
		{
			if(c.isDadangki()==false)
			a=a+c.getSODVHT();
		}
		return a;
	}
	public int tongtinchidadangky(String Tensv)
	{
		int a=0;
		for(Chodangki_bean c:Getchodangkis() )
		{
			if(c.isDadangki()==true && c.getHotenSv().equals(Tensv))
			a=a+c.getSODVHT();
		}
		return a;
	}
}
