package bo;

import java.util.ArrayList;

import bean.GiangVien_bean;
import dao.GiangVien_dao;



public class GiangVien_bo {
	public GiangVien_dao g_dao=new GiangVien_dao();
	public ArrayList<GiangVien_bean> GetGiangViens()
	{
		return g_dao.getGiangViens();
	}
	public String TenGiangVien(String MaGiangVien)
	{
		for(GiangVien_bean gv:GetGiangViens())
		{
			if(gv.getMaGiangVien().equals(MaGiangVien))
			return gv.getTenGiangVien();
		}
		return null;
	}
}
