package bo;

import java.util.ArrayList;

import bean.LopHocPhan_bean;
import bean.ThamGiaHoc_bean;
import dao.ThamGiaHoc_dao;

public class ThamGiaHoc_bo {
	public ThamGiaHoc_dao TGhoc_dao=new ThamGiaHoc_dao();
	public LopHocPhan_bo l_bo=new LopHocPhan_bo();
	public Monhoc_bo mhoc_bo=new Monhoc_bo();
	public String MaMonbymalop(String Malop)
	{
		for(LopHocPhan_bean l:l_bo.GetLophocphan())
		{
			if(l.getMaLopHocPhan().equals(Malop))
				return l.getMaMonHoc();
		}
		return null;
	}
	public ArrayList<ThamGiaHoc_bean> getThamGiahocs()
	{
		return TGhoc_dao.getLophocs();
	}
	public ArrayList<ThamGiaHoc_bean> getThamGiahocchuaduyet()
	{
		ArrayList<ThamGiaHoc_bean> DsThamGia=new ArrayList<ThamGiaHoc_bean>();
		for(ThamGiaHoc_bean t:getThamGiahocs())
		{
			if(t.isDaDangky()==false)
				DsThamGia.add(t);
		}
		return DsThamGia;
	}
	public int Dahoc(String Masv)
	{
		int a=0;
		for(ThamGiaHoc_bean t:getThamGiahocs())
		{
			if(t.getMaSV().equals(Masv) && t.isDaDangky()==true)
				a++;
		}
		return a;
	}
	public int Dadangky(String Masv,String Mamon)
	{
		int a=0;
		for(ThamGiaHoc_bean t:getThamGiahocs())
		{
			if(t.getMaSV().equals(Masv) && MaMonbymalop(t.getMaLophocphan()).equals(Mamon))
				a++;
		}
		return a;
	}
	public ThamGiaHoc_bean KetQua(String Masv,String Mamon)
	{
		for(ThamGiaHoc_bean t:getThamGiahocs())
		{
			if(t.getMaSV().equals(Masv) && MaMonbymalop(t.getMaLophocphan()).equals(Mamon))
				return t;
		}
		return null;
	}
	public char Ketqua(double DiemTB)
	{
		char KetQua;
		KetQua='F';
		if(DiemTB>=5 && DiemTB<=6.5) KetQua = 'D';
		if(DiemTB>6.5 && DiemTB<=7.5) KetQua = 'C';
		if(DiemTB>7.5 && DiemTB<=8.5) KetQua = 'B';
		if(DiemTB>8.5 && DiemTB<=10) KetQua = 'A';
		
		return KetQua;
	}
	public int soluongSvDangKy(String Malophocphan)
	{
		int a=0;
		for(ThamGiaHoc_bean t:getThamGiahocs())
		{
			if(t.isDaDangky()==false && t.getMaLophocphan().equals(Malophocphan))
				a++;
		}
		return a;
	}
	public int ThemThamGiaHoc(String MaLopHocPhan,String MaSinhvien)
	{
		return TGhoc_dao.ThemThamGiaHoc(MaLopHocPhan, MaSinhvien);
	}
	public int XoaThamGiaHoc(String MaLopHocPhan,String MaSinhvien)
	{
		return TGhoc_dao.XoaThamGiaHoc(MaLopHocPhan, MaSinhvien);
	}
	public int tongsotinchi(String Masv)
	{
		int a=0;
		for(ThamGiaHoc_bean t:getThamGiahocs())
		{
			if(t.isDaDangky()==true && t.getMaSV().equals(Masv))
			{
				int SotinChi=mhoc_bo.Sotinchi(l_bo.MaMon(t.getMaLophocphan()));
				a=a+SotinChi;
			}
		}
		return a;
	}
	public int Heso(char A)
	{
		int a=0;
		if(A=='A') a=4;
		if(A=='B') a=3;
		if(A=='C') a=2;
		if(A=='D') a=1;
		return a;
	}
	public double TongDiemTrungbinh(String Masv)
	{
		double a=0,tongsotinchi=0;
		for(ThamGiaHoc_bean t:getThamGiahocs())
		{
			if(t.isDaDangky()==true && t.getMaSV().equals(Masv))
			{
				int DiemTB=(t.getDiemChuyenCan()+t.getDiemKiemTra()+t.getDiemThi())/3;
				int SotinChi=mhoc_bo.Sotinchi(l_bo.MaMon(t.getMaLophocphan()));
				tongsotinchi=tongsotinchi+SotinChi;
				if(Ketqua((DiemTB))=='A') a=a+4*(SotinChi);
				if(Ketqua((DiemTB))=='B') a=a+3*(SotinChi);
				if(Ketqua((DiemTB))=='C') a=a+2*(SotinChi);
				if(Ketqua((DiemTB))=='D') a=a+1*(SotinChi);
				if(Ketqua((DiemTB))=='F') a=a+0*(SotinChi);
			}
		}
		return a/tongsotinchi;
	}
	public int XacNhanDangKi(String MaLopHocPhan,String MaSinhvien)
	{
		return TGhoc_dao.XacNhanDangKi(MaLopHocPhan, MaSinhvien);
	}
	public int XacNhanDangKi()
	{
		return TGhoc_dao.XacNhanDangKi();
	}
	public static void main(String[] args) {
		ThamGiaHoc_bo t=new ThamGiaHoc_bo();
		System.out.println(t.Dahoc("0241010001"));
	}
}
