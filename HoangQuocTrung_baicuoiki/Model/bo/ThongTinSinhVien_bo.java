package bo;

import java.util.ArrayList;
import java.util.Date;

import bean.SinhVien_bean;
import bean.ThongTinSinhVien_bean;

public class ThongTinSinhVien_bo {
	public SinhVien_bo sv_bo=new SinhVien_bo();
	public ThamGiaHoc_bo TG_bo=new ThamGiaHoc_bo();
	public Lop_bo l_bo=new Lop_bo();
	public String hocluc(double DiemGPA)
	{
		 String hocluc="Yếu";
		 if(DiemGPA>=3.5 && DiemGPA<=4)  hocluc="Xuất Xắc";
		 if(DiemGPA>=3 && DiemGPA<=3.5)  hocluc="Giỏi";
		 if(DiemGPA>=2.5 && DiemGPA<=3)  hocluc="Khá";
		 if(DiemGPA>=2 && DiemGPA<=2.5)  hocluc="Trung Bình";
		 return hocluc;
	}
	public ArrayList<ThongTinSinhVien_bean> GetThongtinsv()
	{
		ArrayList<ThongTinSinhVien_bean> dssv=new ArrayList<ThongTinSinhVien_bean>();
		for(SinhVien_bean sv:sv_bo.GetsSinhVien())
		{
			if(TG_bo.Dahoc(sv.getMasv())>0)
			{
				String Masv=sv.getMasv();
				String tensv=sv.getHodem()+sv.getTen();
				Date Ngaysinh=sv.getNgaysinh();
				boolean gioitinh=sv.getGioitinh();
				String Tenlop=l_bo.Getlopbymalop(sv.getMalop()).getTenlop();
				Double DiemGPA=TG_bo.TongDiemTrungbinh(sv.getMasv());
				int khoa=l_bo.Getlopbymalop(sv.getMalop()).getKhoa();
				String hocluc=hocluc(DiemGPA);
				ThongTinSinhVien_bean t=new ThongTinSinhVien_bean(Masv, tensv, Ngaysinh, gioitinh, Tenlop, DiemGPA, khoa,hocluc);
				dssv.add(t);
			}
		}
		return dssv;
	}
	public ArrayList<ThongTinSinhVien_bean> GetThongtinsvall()
	{
		ArrayList<ThongTinSinhVien_bean> dssv=new ArrayList<ThongTinSinhVien_bean>();
		for(SinhVien_bean sv:sv_bo.GetsSinhVien())
		{
			
				String Masv=sv.getMasv();
				String tensv=sv.getHodem()+sv.getTen();
				Date Ngaysinh=sv.getNgaysinh();
				boolean gioitinh=sv.getGioitinh();
				String Tenlop=l_bo.Getlopbymalop(sv.getMalop()).getTenlop();
				Double DiemGPA=TG_bo.TongDiemTrungbinh(sv.getMasv());
				int khoa=l_bo.Getlopbymalop(sv.getMalop()).getKhoa();
				String hocluc=hocluc(DiemGPA);
				ThongTinSinhVien_bean t=new ThongTinSinhVien_bean(Masv, tensv, Ngaysinh, gioitinh, Tenlop, DiemGPA, khoa,hocluc);
				dssv.add(t);
			
		}
		return dssv;
	}
	public  ArrayList<ThongTinSinhVien_bean> GetThongtinsvbyMasv(String Masv)
	{
		ArrayList<ThongTinSinhVien_bean> dssv=new ArrayList<ThongTinSinhVien_bean>();
		for(ThongTinSinhVien_bean t:GetThongtinsv())
			if(t.getMasv().equals(Masv))
			dssv.add(t);
		return dssv;
	}
	public  ArrayList<ThongTinSinhVien_bean> GetThongtinsvbyMasvs(String Masv)
	{
		ArrayList<ThongTinSinhVien_bean> dssv=new ArrayList<ThongTinSinhVien_bean>();
		for(ThongTinSinhVien_bean t:GetThongtinsvall())
			if(t.getMasv().equals(Masv))
			dssv.add(t);
		return dssv;
	}
	public static void main(String[] args) {
		ThongTinSinhVien_bo t=new ThongTinSinhVien_bo();
		for(ThongTinSinhVien_bean ta:t.GetThongtinsv())
		{
			System.out.println(ta);
		}
	}
}
