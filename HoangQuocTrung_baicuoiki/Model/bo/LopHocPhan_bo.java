package bo;

import java.util.ArrayList;

import bean.LopHocPhan_bean;
import dao.LopHocPhan_dao;

public class LopHocPhan_bo {
	public LopHocPhan_dao l_dao=new LopHocPhan_dao();
	public ArrayList<LopHocPhan_bean> GetLophocphan()
	{
		return l_dao.getLophocs();
	}
	public ArrayList<LopHocPhan_bean> GetLophocphanbyMamon(String Mamon)
	{
		ArrayList<LopHocPhan_bean> DsLop=new ArrayList<LopHocPhan_bean>();
		for(LopHocPhan_bean l:GetLophocphan())
		{
			if(l.getMaMonHoc().equals(Mamon))
				DsLop.add(l);
		}
		return DsLop;
	}
	public int Soluonglop(String MaMon)
	{
		int a=0;
		for(LopHocPhan_bean l:GetLophocphan())
		{
			if(l.getMaMonHoc().equals(MaMon))
				a++;
		}
		return a;
	}
	public String TenHocPhan(String MaLopHocPhan)
	{
		for(LopHocPhan_bean l:GetLophocphan())
		{
			if(l.getMaLopHocPhan().equals(MaLopHocPhan))
				return l.getTenlophocphan();
				
		}
		return null;
	}
	public String MaMon(String MaLopHocPhan)
	{
		for(LopHocPhan_bean l:GetLophocphan())
		{
			if(l.getMaLopHocPhan().equals(MaLopHocPhan))
				return l.getMaMonHoc();
		}
		return null;
	}
	public int SoLuongSVToiDa(String MaLopHocPhan)
	{
		for(LopHocPhan_bean l:GetLophocphan())
		{
			if(l.getMaLopHocPhan().equals(MaLopHocPhan))
				return l.getGioiHanSoluongsinhvien();
		}
		return 0;
	}
	public int SoLuongSVToiDaTheoMon(String MaMon)
	{
		for(LopHocPhan_bean l:GetLophocphan())
		{
			if(l.getMaMonHoc().equals(MaMon))
				return l.getGioiHanSoluongsinhvien();
		}
		return 0;
	}
	public static void main(String[] args) {
		LopHocPhan_bo l=new LopHocPhan_bo();
		System.out.println(l.GetLophocphanbyMamon("TI-002"));
	}
}
