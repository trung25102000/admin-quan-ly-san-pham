package bo;

import java.util.ArrayList;

import bean.MonHoc_bean;
import dao.Monhoc_dao;

public class Monhoc_bo {
	public Monhoc_dao mon_dao=new Monhoc_dao();
	public ArrayList<MonHoc_bean> Getmonhocs()
	{
		return mon_dao.getMonhocs();
	}
	public int Sotinchi(String MaMon)
	{
		for(MonHoc_bean m:Getmonhocs())
		{
			if(m.getMamonhoc().equals(MaMon))
				return m.getSODVHT();
		}
		return 0;
	}
	public MonHoc_bean Getmon(String MaMon)
	{
		for(MonHoc_bean m:Getmonhocs())
		{
			if(m.getMamonhoc().equals(MaMon))
				return m;
		}
		return null;
	}
}
