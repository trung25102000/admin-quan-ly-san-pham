package bo;

import java.util.ArrayList;

import bean.Khoa_bean;
import dao.Khoa_dao;

public class Khoa_bo {
	public Khoa_dao k_dao=new Khoa_dao();
	public ArrayList<Khoa_bean> GetKhoas()
	{
		return k_dao.getkhoas();
	}
	public Khoa_bean GetKhoabyMaKhoa(String MaKhoa)
	{
		for(Khoa_bean k:GetKhoas())
		{
			if(k.getMakhoa().equals(MaKhoa))
				return k;
		}
		return null;
	}
}
